<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('title_site')->nullable();
            $table->string('mail_site');
            $table->string('mail_order');
            $table->string('mail_contact');
            $table->string('mail_adhesion')->nullable();
            $table->enum('mode_payement', ['Kkiapay', 'Paypal', 'Visa'])->default('Kkiapay');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
