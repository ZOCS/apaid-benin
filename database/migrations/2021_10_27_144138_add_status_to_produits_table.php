<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produits', function (Blueprint $table) {
            $table->boolean('new_arrival')->default(false)->after('status');
            $table->boolean('featured')->default(false)->after('status');
            $table->boolean('popular')->default(false)->after('status');
            $table->boolean('promo')->default(false)->after('status');
            $table->string('taux_promo')->nullable()->after('status');
            $table->string('prix_promo')->nullable()->after('prix');
            $table->string('slug')->nullable()->after('nom');
            $table->longText('detail')->nullable()->after('description');
            $table->unsignedBigInteger('mesure_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produits', function (Blueprint $table) {
            //
        });
    }
}
