<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeOrdersUserIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commandes', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->string('lname')->after('id');
            $table->string('fname')->after('id');
            $table->string('email')->after('id');
            $table->string('phone')->after('id');
            $table->string('adresse')->after('produits');
            $table->string('pays')->after('produits');
            $table->string('ville')->after('produits');
            $table->string('zip')->nullable()->after('produits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commandes', function (Blueprint $table) {
            //
        });
    }
}
