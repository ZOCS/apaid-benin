<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('nom');
            $table->string('image');
            $table->string('slug')->nullable();
            $table->string('prix');
            $table->integer('stock');
            $table->enum('status', ['Actif', 'Non actif'])->nullable()->default('Actif');           
            $table->longText('detail')->nullable();
            $table->longText('description');
            $table->string('auteur');
            $table->boolean('new_arrival')->default(false);
            $table->boolean('featured')->default(false);
            $table->boolean('popular')->default(false);
            $table->boolean('promo')->default(false);
            $table->string('taux_promo')->nullable();
            $table->string('prix_promo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
