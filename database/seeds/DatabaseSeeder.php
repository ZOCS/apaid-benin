<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10) . '@gmail.com',
            'role' => 'admin',
            'sexe' => 'Masculin',
            'phone' => '62201028',
            'pays' => 'Benin',
            'password' => Hash::make('12345678'),
        ]);
    }
}
