<?php

use App\Http\Livewire\Shop;
use App\Http\Livewire\ShopComponent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('/');

Route::get('/code', function(){
  $date = Carbon\Carbon::now()->format('d-m-Y');
  $uniq = mt_rand(1000, 9999);
  $code = 'Sourou_'.$date.'_'.$uniq;
  dd($code);
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');



//Route pour la page de contact
Route::get('/contact', 'ContactController@contact')->name('contact');
Route::post('/send', 'ContactController@store')->name('send.mail');

//Route pour la page a propos
Route::get('/about', 'User\AboutController@about')->name('about');

//Route pour la page adhesion
Route::get('/adhesion', 'User\AboutController@adhesion')->name('adhesion');

Route::get('/conditions-generales-dutilisation', 'ContactController@cgu')->name('cgu');


Route::post('/adhesionsend', 'User\AboutController@preStore')->name('adhesion.store');

Route::post('/adhesion/payment', 'User\AboutController@store')->name('adhesion.payment');




Route::resource('blog/user', 'User\BlogController');



Route::resource('commentaire', 'CommentaireController')->middleware('auth');


/* <-- Routes resources --------------------------------------------------------->*/






//Route pour la page Blog
Route::resource('blog', 'User\BlogController');

Route::resource('projet', 'ProjetController');



//<-- Routes resources fin  --------------------------------------------------------->

Route::get('services', 'Service\ServiceController@services')->name('services');

Route::get('services/single/{id}', 'Service\ServiceController@single')->name('services.single');
Route::post('/subscribe', 'Service\ServiceController@subscribeService')->name('subscribe.service');






Route::post('/newsletter_email','NewsletterController@store')->name('newsletter_email');



Route::get('/projet-vue', 'ProjetController@index')->name('projet-vue');
Route::get('/projet-search', 'ProjetController@search')->name('projet-search');
Route::resource('/projet', 'ProjetController');
Route::get('/partenaire', 'ShopController@partenaire')->name('partenaire');
Route::get('/ong', 'ShopController@ong')->name('ong');
Route::get('/organisme', 'ShopController@organisme')->name('organisme');
//Route::get('/accueil', 'ShopController@accueil')->name('accueil-shop');

Route::get('/details', 'User\AboutController@details')->name('details');


//**====================================== Boutique routes =================================**/

// Cart routes
Route::resource('cart', 'CartController');
Route::get('/cart.store', 'CartController@store')->name('store.cart');
Route::patch('/panier/{rowId}', 'CartController@update')->name('cart.update');

Route::get('/AddWhislist', 'CartController@whislistAdd')->name('whislist.store');

Route::get('/filter_by_price', 'BoutiqueController@filter_by_price')->name('filter_by_price');

Route::get('/sorting', 'ProduitController@sortingBy')->name('sorting');

Route::get('/perpage', 'ProduitController@paginatePage')->name('perpage');

Route::resource('checkout', 'CheckoutController');

Route::get('payement', 'CheckoutController@checkout')->name('payement');

Route::get('/info', 'CheckoutController@dataUser')->name('info');

Route::post('/coupon', 'CouponController@apply')->name('coupon.apply');

//  Route::livewire('/boutique/premiere', Shop::class);
Route::get('/whislist', 'BoutiqueController@whislist')->name('whislist');

//Route::get('/checkout', 'User\AboutController@checkout')->name('checkout');

Route::get('/search', 'ProduitController@search')->name('produit.search');

Route::get('/thank-You', 'CheckoutController@thankyou')->name('thank-You');

Route::resource('boutique', 'BoutiqueController');

Route::post('/payment', 'ShopController@payment')->name('payment');

//Route pour la boutique
Route::get('/accueil-shop', 'ShopController@accueil')->name('accueil-shop');

Route::resource('produit', 'ProduitController');


Route::get('/mail-order', function () {
  $order = App\Models\Commande::find(21);

  return new App\Mail\OrderUser($order);
});

Route::get('/mail-orderadmin', function () {
  $data = App\Models\Adhesion::find(1);

  return new App\Mail\AdhesionMail($data);
});
// Route::get('produi')
/** Fin routes boutique */
Route::resource('carrefour', 'User\CarrefourController')->middleware('auth');
Route::post('/candidature/', 'User\CarrefourController@store')->name('candidat');

Route::get('/profile', 'User\UserController@index')->name('profile')->middleware('auth');
Route::put('/profile/update/{id}', 'User\UserController@update')->name('user.update')->middleware('auth');




/** ============================================ Grouoe routes User ============================*/
Route::group(['prefix' => 'user', 'as' => 'user.', 'middleware' => ['auth', 'user']],function () {

  Route::resource('carrefour', 'CarrefourController');

  //**====================== Route profile =================*/
  Route::get('/profile', 'UserController@profile')->name('profile');
});




/** ============================================ Grouoe routes admin blog ============================*/
Route::group(['prefix' => 'blogger', 'as' => 'blogger.', 'middleware' => ['auth', 'blogger']],function () {
  Route::get('/dashboard', 'Admin\AdminController@index')->name('dashboard');
  Route::resource('carrefour', 'CarrefourController');

  //**====================== Routes Blog ==================================================*/
  Route::get('/blog.index', 'Admin\BlogController@index')->name('blog.index');

  Route::get('/blog', 'Admin\BlogController@create')->name('blog.create');

  Route::post('/blog', 'Admin\BlogController@store')->name('blog.store');

  Route::get('/blog/{blog}', 'Admin\BlogController@edit')->name('blog.edit');

  Route::get('/blog/{blog}', 'Admin\BlogController@show')->name('blog.show');

  Route::put('/blog/{blog}', 'Admin\BlogController@update')->name('blog.update');

  Route::put('/blog/{blog}', 'Admin\BlogController@destroy')->name('blog.destroy');

});




/** ============================================ Grouoe routes admin Manager ============================*/
Route::group(['prefix' => 'manage', 'as' => 'manage.', 'middleware' => ['auth', 'manager']],function () {

  Route::get('/dashboard', 'Admin\AdminController@index')->name('dashboard');
  
  //**============= Routes produits ==========================*/
    
  Route::get('/insert-product', 'Admin\AdminController@insertProduct')->name('insert-product');

  Route::post('/store-product', 'ProduitController@store')->name('store-product');

  Route::get('/edit-product/{produit}', 'ProduitController@edit')->name('edit-product');

  Route::get('/show-product/{produit}', 'ProduitController@show')->name('show-product');

  Route::put('/update-product/{produit}', 'ProduitController@update')->name('update-product');

  Route::delete('/destroy-product/{produit}', 'ProduitController@destroy')->name('destroy-product');
  
  Route::get('/list-product', 'ProduitController@index')->name('list-product');


    //**======================= Route mesure =========================*/  
  Route::resource('mesure', 'MesureController');
    
    // Ressource de Categorie
  Route::resource('categorie', 'CategorieController');

    //**====================== Routes Offre =========================*/
  Route::get('/list-offre', 'Admin\OffreController@listBlog')->name('list-offre'); 

  Route::post('/insert-offre', 'Admin\OffreController@store')->name('insert-offre');

  Route::get('/edit-offre/{offre}', 'Admin\OffreController@edit')->name('edit-offre');

  Route::get('/show-offre/{offre}', 'Admin\OffreController@show')->name('show-offre');

  Route::put('/update-offre/{offre}', 'Admin\OffreController@update')->name('update-offre');

  Route::put('/destroy-offre/{offre}', 'Admin\OffreController@destroy')->name('destroy-offre');
});





//**================================= Groupe route administration ===================================**// 
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'isadmin']],function () {
    Route::get('/dashboard', 'Admin\AdminController@index')->name('dashboard');
  Route::get('/essai', 'Admin\AdminController@essai')->name('essai');

  //**============================ Routes produits ====================*/
    
    Route::get('/insert-product', 'Admin\AdminController@insertProduct')->name('insert-product');

    Route::post('/store-product', 'ProduitController@store')->name('store-product');

    Route::get('/edit-product/{produit}', 'ProduitController@edit')->name('edit-product');

    Route::get('/show-product/{produit}', 'ProduitController@show')->name('show-product');

    Route::put('/update-product/{produit}', 'ProduitController@update')->name('update-product');

    Route::delete('/destroy-product/{produit}', 'ProduitController@destroy')->name('destroy-product');
    
    Route::get('/list-product', 'ProduitController@index')->name('list-product');

    //**====================== Routes Blog ===========================*/
    Route::get('/blog/index', 'Admin\BlogController@index')->name('blog.index'); 

    Route::get('/blog', 'Admin\BlogController@create')->name('blog.create');

    Route::post('/blog', 'Admin\BlogController@store')->name('blog.store');

    Route::get('/blog/edit/{blog}', 'Admin\BlogController@edit')->name('blog.edit');

    Route::get('/blog/show/{blog}', 'Admin\BlogController@show')->name('blog.show');

    Route::put('/blog/update/{blog}', 'Admin\BlogController@update')->name('blog.update');

    Route::put('/blog/delete/{blog}', 'Admin\BlogController@destroy')->name('blog.destroy');

    //**====================== Route Projet ===================== */

    Route::get('/insert-projet', 'Admin\AdminController@insertProjet')->name('insert-projet');

    Route::post('/store-projet', 'ProjetController@store')->name('store-projet');

    Route::get('/edit-projet/{projet}', 'ProjetController@edit')->name('edit-projet'); 

    Route::put('/update-projet/{projet}', 'ProjetController@update')->name('update-projet');

    Route::put('/destroy-projet/{projet}', 'ProjetController@destroy')->name('destroy-projet'); 

    Route::get('/list-projet', 'Admin\AdminController@listProjet')->name('list-projet');


    //**====================== Routes organisme =======================*/

    Route::get('/ajout-organisme', 'Admin\AdminController@createOrganisme')->name('ajout.organisme');

    Route::get('/listOrganisme', 'Admin\AdminController@indexOrganisme')->name('listOrganisme');

    Route::post('/storeOrganisme', 'Admin\AdminController@storeOrganisme')->name('storeOrganisme');

    Route::get('/editOrganisme/{organisme}', 'Admin\AdminController@editOrganisme')->name('editOrganisme');

    Route::put('/updateOrganisme/{organisme}', 'Admin\AdminController@updateOrganisme')->name('updateOrganisme');

    Route::delete('/destroyOrganisme/{organisme}', 'Admin\AdminController@destroyOrganisme')->name('destroyOrganisme');

  //**======================= Route mesures =========================*/  
    Route::resource('mesure', 'MesureController');
    
  // Ressource de Categorie
    Route::resource('categorie', 'CategorieController');

   //**====================== Routes Offres =========================*/
    Route::get('/list-offre', 'Admin\OffreController@listBlog')->name('list-offre');

    Route::get('/offre.index', 'Admin\OffreController@index')->name('offre.index');

  Route::get('/offre.create', 'Admin\OffreController@create')->name('offre.create');

    Route::post('/offre.store', 'Admin\OffreController@store')->name('offre.store');

    Route::get('/offre.edit/{offre}', 'Admin\OffreController@edit')->name('offre.edit');

    Route::get('/offre.show/{offre}', 'Admin\OffreController@show')->name('offre.show');

    Route::put('/offre.update/{offre}', 'Admin\OffreController@update')->name('offre.update');

    Route::put('/offre.destroy/{offre}', 'Admin\OffreController@destroy')->name('offre.destroy');

  //**========================== Routes Services ========================================== */
    Route::get('services', 'Service\ServiceController@index')->name('services.index');

    Route::get('services.create', 'Service\ServiceController@create')->name('services.create');

    Route::post('services.store', 'Service\ServiceController@store')->name('services.store');

    Route::get('services.edit/{Id:id}', 'Service\ServiceController@edit')->name('services.edit');

    Route::patch('services.update', 'Service\ServiceController@upate')->name('services.update');


    Route::delete('services.destroy', 'Service\ServiceController@destroy')->name('services.destroy');

    //**====================== Routes managment Users =========================*/
    Route::resource('user', 'UserController');
});




