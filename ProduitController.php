<?php

namespace App\Http\Controllers;


use App\Models\Produit;
use App\Models\Categorie;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
            $produits = Produit::with('categories')->paginate(4);
            $produit_promos = Produit::with('categories')->where('promo', '1')->paginate(4);

        return view('admin.produit.liste-produit', compact('produits', 'produit_promos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
            $request->validate([
                'nom' => 'required',
                'prix' => 'required',
                'image' => 'required|mimes:png,jpg,jpeg|max:8192',
                'stock' => 'required',
                'auteur' => 'required',
                'slug' => 'required',
                'description' => 'required|min:50',
                'detail' => 'required|min:50',
                'status' => 'required',
            ]);
            $data = array(
                'code' => random_int(100000000000, 100000000000000000),
                'nom'=> $request->nom,
                'prix'=> $request->prix,
                'image'=> $request->file('image')->store('produits', 'public'),
                'images'=> json_encode($request->file('images')),
                'status' => $request->statut,
                'stock' => $request->stock,
                'auteur'=> $request->auteur,
                'slug' => $request->slug,
                'detail'=> $request->detail,
                'description'=> $request->description,
                'featured' => $request->has('featured'),
                'new_arrival' => $request->has('new_arrival'),
                'promo'=> $request->has('promo'),
                'popular'=> $request->has('popular'),
                'taux_promo'=> $request->promo,
                'prix_promo'=> $request->prix_promo,
                'mesure_id' => $request->mesure_id,
            );
            $prod = Produit::create($data);
            $prod->categories()->attach($request->categorie);
            return redirect()->back()->with('message', 'L\'enregistrement a reussi.!!!');
       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  Produit  $prod
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produit = Produit::where('slug',$id)->firstOrFail();
        //dd($produit);
        return view('boutique.details', compact('produit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
        $categories = Categorie::all();
        $categ= Produit::find($produit->id)->categories()->get();
       // dd($categ);
        return view('admin\produit\edit-product', compact('produit', 'categories', 'categ'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       if(Auth::user()->is_admin == 'admin'){
            $produit = Produit::find($id);
        
            $produit->nom = $request->nom;
            $produit->prix = $request->prix;
            $produit->image = $request->file('image')->store('produits', 'public');
            $produit->images = $request->file('images')->store('produits', 'public');
            $produit->stock = $request->stock;
            $produit->auteur = $request->auteur;
            $produit->description = $request->description;
            $produit->detail = $request->detail;
            $produit->featured = $request->has('featured');
            $produit->popular = $request->has('popular');
            $produit->new_arrival = $request->has('new_arrival');
            $produit->promo = $request->has('promo');
            $produit->prix_promo = $request->prix_promo;
            $produit->taux_promo = $request->description;
            $produit->slug = $request->slug;

            $produit->update();
            $produit->categories()->sync($request->categorie);
            $produits = Produit::with('categories')->paginate(6);
            return view('admin.produit.liste-produit', compact('produits'))->with('success', 'Le produit a ete mise a jour');
       }else{
           return redirect('login');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produit = Produit::destroy($id);
        //$produit->delete();

        return redirect()->back();
       // return $id;
    }

    public function search()
    {
        request()->validate([
            'search' => 'required|min:3'
        ]);
        $q = request()->input('search');
        //dd($q);
       $produits = Produit::where('nom', 'like', "%$q%")->orWhere('description', 'like', "%$q%")->paginate(4);
        return view('Boutique.shop')->with('produits', $produits);
    }
}
