@extends('layouts.nav')

@section('content')

<section class="hero-wrap hero-wrap-2" style="background-image: url('account/images/bg_2.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-end">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>À propos de nous <i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-0 bread">À propos de nous</h1>
        </div>
      </div>
    </div>
  </section>
     
  <section class="ftco-section ftco-no-pt bg-light">
      <div class="container">
          <div class="row d-flex no-gutters">
              <div class="col-md-6 d-flex">
                  <div class="img img-video d-flex align-self-stretch align-items-center justify-content-center justify-content-md-center mb-4 mb-sm-0" style="background-image:url(images/about.jpg);">
                  </div>
              </div>
              <div class="col-md-6 pl-md-5 py-md-5">
                  <div class="heading-section pl-md-4 pt-md-5">
                      <span class="subheading">Bienvenue a APAID</span>
              <h2 class="mb-4">{{ ('Organisation d\'Appui a la Promotion de Pratiques Agro écologiques Innovantes Durables') }}</h2>
              <p>APAID est une plateforme entrepreneuriale, un espace de vulgarisation porteur de solutions techniques, organisationnelles et morales des innovations vertes initiées par les organismes membres ou partenaires (Centres de Recherche, ONG, OPA, Centres de Production et de Transformation, Entreprises Agroalimentaires, Particuliers, structures financières, etc...)</p>
              {{-- <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p> --}}
                  </div>
          </div>
      </div>
      </div>
  </section>

  <section class="ftco-counter bg-light ftco-no-pt" id="section-counter">
    <div class="container">
      @php
      $inno = App\Models\Produit::count();
      $org = App\Models\Organisme::count();
      $ca = App\Models\Categorie::count();
      @endphp
      <div class="row">
        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
          <div class="block-18 text-center">
            <div class="text">
              <strong class="number" data-number="1">1</strong>
            </div>
            <div class="text">
              <span>{{ __("Années d'expérience") }}</span>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
          <div class="block-18 text-center">
            <div class="text">
              <strong class="number" data-number="{{ $inno }}">{{ $inno }}</strong>
            </div>
            <div class="text">
              <span>{{ ('Nombres d\'innovations') }}</span>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
          <div class="block-18 text-center">
            <div class="text">
              <strong class="number" data-number="{{ $ca }}">{{ $ca }}</strong>
            </div>
            <div class="text">
              <span>{{ ('Nombres de domaine d\'intervention') }}</span>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
          <div class="block-18 text-center">
            <div class="text">
              <strong class="number" data-number="{{ $org }}">{{ $org }}</strong>
            </div>
            <div class="text">
              <span>{{ ('Nombres d\'organismes') }}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>



  

  

@include('layouts.newsletter')

@endsection