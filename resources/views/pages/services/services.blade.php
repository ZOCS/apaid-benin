@extends('layouts.nav')

@push('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<style>
    .css {
        margin-top: 9rem;
        align-content: center;

    }
</style>
@endpush

@section('content')

<section class="ftco-section">
    <div class="container">
        <div class="row d-flex">
            @foreach ($services as $service)
                <div class="col-md-4 d-flex ftco-animate">
                    <div class="blog-entry align-self-stretch">
                        {{-- <a href="blog-single.html" class="block-20 rounded" style="background-image: url('images/image_1.jpg');">
                        </a> --}}
                        <div class="text p-4">
                            <div class="meta mb-2">
                                {{-- <div><a href="#">March 31, 2020</a></div> --}}
                                {{-- <div><a href="#">Admin</a></div> --}}
                                {{-- <div><a href="#" class="meta-chat"><span class="fa fa-comment"></span> 3</a></div> --}}
                            </div>
                            <h2>{{ $service->titre }}</h2>
                            <h3 class="heading"><a href="#" class="pb-15">{!! substr(strip_tags($service->description), 0, 240) !!}</a></h3>
                            <a href="{{ route('services.single', $service) }}" class="btn btn-primary css">En Savoir +</a>
                        </div>
                    </div>
                </div>
            @endforeach
            
        </div>    
        
    </div>
</section>

@include('layouts.newsletter')

@endsection