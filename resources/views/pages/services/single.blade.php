@extends('layouts.nav')


@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('account/images/bg_2.jpg');"
        data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-end">
                <div class="col-md-9 ftco-animate pb-5">
                    <p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Accueil <i
                                    class="ion-ios-arrow-forward"></i></a></span> <span class="mr-2"><a
                                href="service.html">service <i class="ion-ios-arrow-forward"></i></a></span> <span>{{$service->titre}}<i
                                class="ion-ios-arrow-forward"></i></span></p>
                    <h1 class="mb-0 bread">{{$service->titre}}</h1>
                </div>
            </div>
        </div>
    </section>
    
    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <p>
                        <img src="{{ asset('storage/' . $service->image) }}" alt="" class="img-fluid">
                    </p>
                    <h1 class="mb-3">{{$service->titre}}</h1>
                    {{-- <p>Temporibus ad error suscipit exercitationem hic molestiae totam obcaecati rerum, eius aut, in.
                        Exercitationem atque quidem tempora maiores ex architecto voluptatum aut officia doloremque. Error
                        dolore voluptas, omnis molestias odio dignissimos culpa ex earum nisi consequatur quos odit quasi
                        repellat qui officiis reiciendis incidunt hic non? Debitis commodi aut, adipisci.</p> --}}
                    {{-- <p>
                        <img src="{{asset('account/images/image_2.jpg')}}" alt="" class="img-fluid">
                    </p> --}}
                    <p>{!! $service->description !!}</p>
    
                    <div class="tag-widget post-tag-container mb-5 mt-5">
                        <div class="tagcloud">
                            <a href="#" class="tag-cloud-link">Agriculture</a>
                            <a href="#" class="tag-cloud-link">Aviculture</a>
                            <a href="#" class="tag-cloud-link">Halieutique</a>
                            {{-- <a href="#" class="tag-cloud-link">Travel</a> --}}
                        </div>
                    </div>
    
                    <div class="about-author d-flex p-4 bg-light">
                        <div class="bio mr-5">
                            <img src="{{ asset('account/images/person_1.jpg') }}" alt="Image placeholder"
                                class="img-fluid mb-4">
                        </div>
                        <div class="desc">
                            <h3></h3>
                            <p></p>
                        </div>
                    </div>
    
    
                </div> <!-- .col-md-8 -->
                <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
                    <div class="sidebar-box">
                        <form action="#" class="search-form">
                            <div class="form-group">
                                <span class="fa fa-search"></span>
                                <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
                            </div>
                        </form>
                    </div>
                    <div class="sidebar-box ftco-animate">
                        <div class="categories">
                            <h3>Services</h3>
                            @foreach ($services as $item)
                             <li><a href="{{ route('services.single', $item) }}">{{ $item->titre }}<span class="fa fa-chevron-right"></span></a></li>   
                            @endforeach
                            
                            
                        </div>
                    </div>
    
                   
    
                    <div class="sidebar-box ftco-animate">
                        <h3>Souscrire a ce services</h3>
                        <div class="tagcloud">
                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample"
                                aria-expanded="false" aria-controls="collapseExample">
                                Souscrire
                            </button>
                            <div class="collapse " id="collapseExample">
                                <div class="card card-body">
                                    <form method="POST" id="contactForm" name="contactForm" class="contactForm" action="{{route('subscribe.service')}}">
                                        @csrf
                                        <input type="hidden" name="service" value="{{ $service->id }}"/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label" for="name">Nom et prénom</label>
                                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label" for="email">Adresse e-mail</label>
                                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="label" for="#">Message</label>
                                                    <textarea name="message" class="form-control" id="message" cols="30" rows="4"
                                                        placeholder="Message"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="submit" value="Envoyer le message" class="btn btn-primary">
                                                    <div class="submitting"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="sidebar-box ftco-animate">
                        <h3>APAID Benin</h3>
                        <p>APAID est une plateforme entrepreneuriale, un espace de vulgarisation porteur de solutions techniques,
                        organisationnelles et morales des innovations vertes initiées par les organismes membres ou partenaires.</p>
                    </div>
                </div>
    
            </div>
        </div>
    </section> <!-- .section -->
    
@include('layouts.newsletter')
@endsection