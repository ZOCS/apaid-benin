@extends('layouts.nav')

@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-end">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html"> <i class="ion-ios-arrow-forward"></i></a></span> <span><i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-0 bread">Passez au paiement</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 ftco-animate">
                      <div class="cases-wrap d-md-flex align-items-center">
                          
                          <div class="text pl-md-5">
                              <span class="cat">Paiement</span>
                              <h2>Derniere etape pour finaliser votre souscription a notre service</h2>
                              <p>Lors du paiement, s'il s'agit d'une entreprise ou ONG, ou Organisme; nous mettre le nom de l'entreprise ou ONG, ou Organisme dans a la place du <strong>nom et prenom</strong></p>
                              <p>Apres le paiement une facture est delivree, veuillez donc la telecharger et nous envoyer un exemplaire par mail afin d'examiner votre dossier</p>
                              <p><button  class="btn btn-primary kkiapay-button">Payez maintenant</button></p>
                              
                          </div>
                      </div>

                     
              </div>
            </div>
    </div>
  </section> <!-- .section -->

 
@endsection