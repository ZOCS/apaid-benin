@extends('layouts.nav')

@section('content')

<section class="hero-wrap hero-wrap-2" style="background-image: url('account/images/bg_2.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-end">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{route('/')}}">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Blog <i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-0 bread">Blog</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section">
    <div class="container">
      <div class="row d-flex">
        @foreach($blogs as $blog)
          @php
           $created = Carbon\Carbon::parse($blog->created_at)->locale('fr_FR')->isoFormat('LLLL')
          @endphp
        <div class="col-md-4 d-flex ftco-animate">
          <div class="blog-entry align-self-stretch">
            <a href="{{ route('blog.show', $blog) }}" class="block-20 rounded" style="background-image: url('{{ asset('storage/' . $blog->image) }}');">
            </a>
            <div class="text p-4">
                <div class="meta mb-2">
                <div><a href="#">{{ Carbon\Carbon::parse($blog->created_at)->locale('fr_FR')->isoFormat('Do MMMM YYYY') }}</a></div>
                <div><a href="{{ route('blog.show', $blog) }}">{{ $blog->auteur}}</a></div>
                <div><a href="#" class="meta-chat"><span class="fa fa-comment"></span> 3</a></div>
              </div>
              <h1 class="heading"><a href="{{ route('blog.show', Crypt::encrypt($blog)) }}"><strong>{{$blog->title}}</strong></a></h1>
              <h3 class="heading"><a href="{{ route('blog.show', Crypt::encrypt($blog)) }}">{!! substr(strip_tags($blog->description, '(...)'), 0, 100) !!}</a></h3>
              {{-- <h3 class="heading"><a href="#">{{ $blog->text }}</a></h3> --}}
            </div>
          </div>
        </div>
        @endforeach
        {{-- <div class="col-md-4 d-flex ftco-animate">
          <div class="blog-entry align-self-stretch">
            <a href="blog-single.html" class="block-20 rounded" style="background-image: url('account/images/image_2.jpg');">
            </a>
            <div class="text p-4">
                <div class="meta mb-2">
                <div><a href="#">March 31, 2020</a></div>
                <div><a href="#">Admin</a></div>
                <div><a href="#" class="meta-chat"><span class="fa fa-comment"></span> 3</a></div>
              </div>
              <h1 class="heading"><a href="#"><strong>Nom</strong></a></h1>
              <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
            </div>
          </div>
        </div>
        <div class="col-md-4 d-flex ftco-animate">
          <div class="blog-entry align-self-stretch">
            <a href="blog-single.html" class="block-20 rounded" style="background-image: url('account/images/image_3.jpg');">
            </a>
            <div class="text p-4">
                <div class="meta mb-2">
                <div><a href="#">March 31, 2020</a></div>
                <div><a href="#">Admin</a></div>
                <div><a href="#" class="meta-chat"><span class="fa fa-comment"></span> 3</a></div>
              </div>
              <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
            </div>
          </div>
        </div>

        <div class="col-md-4 d-flex ftco-animate">
          <div class="blog-entry align-self-stretch">
            <a href="blog-single.html" class="block-20 rounded" style="background-image: url('account/images/image_3.jpg');">
            </a>
            <div class="text p-4">
                <div class="meta mb-2">
                <div><a href="#">March 31, 2020</a></div>
                <div><a href="#">Admin</a></div>
                <div><a href="#" class="meta-chat"><span class="fa fa-comment"></span> 3</a></div>
              </div>
              <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
            </div>
          </div>
        </div>
        <div class="col-md-4 d-flex ftco-animate">
          <div class="blog-entry align-self-stretch">
            <a href="blog-single.html" class="block-20 rounded" style="background-image: url('account/images/image_5.jpg');">
            </a>
            <div class="text p-4">
                <div class="meta mb-2">
                <div><a href="#">March 31, 2020</a></div>
                <div><a href="#">Admin</a></div>
                <div><a href="#" class="meta-chat"><span class="fa fa-comment"></span> 3</a></div>
              </div>
              <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
            </div>
          </div>
        </div>
        <div class="col-md-4 d-flex ftco-animate">
          <div class="blog-entry align-self-stretch">
            <a href="blog-single.html" class="block-20 rounded" style="background-image: url('account/images/image_6.jpg');">
            </a>
            <div class="text p-4">
                <div class="meta mb-2">
                <div><a href="#">March 31, 2020</a></div>
                <div><a href="#">Admin</a></div>
                <div><a href="#" class="meta-chat"><span class="fa fa-comment"></span> 3</a></div>
              </div>
              <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
            </div>
          </div>
        </div> --}}
      </div>
      
      {{$blogs->links('pagination::blog-paginate')}}
            
    </div>
  </section>

  @include('layouts.newsletter')

@endsection