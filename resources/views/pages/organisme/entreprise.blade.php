@extends('layouts.nav')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/vendors/styles/style.css') }}">
<style>
  .portflio-item .portfolio-item-content {
    position: absolute;
    content: "";
    right: 0px;
    bottom: 0px;
    opacity: 0;
    transition: all .35s ease;
  }

  .portflio-item:before {
    position: absolute;
    content: "";
    left: 0px;
    top: 0px;
    width: 50%;
    height: 50%;
    background: rgba(0, 0, 0, 0.8);
    opacity: 0;
    transition: all .35s ease;
    overflow: hidden;
  }

  .portflio-item:hover:before {
    opacity: 1;
  }

  .portflio-item:hover .portfolio-item-content {
    opacity: 1;
    bottom: 20px;
    right: 30px;
  }

  .portflio-item .overlay-item {
    position: absolute;
    content: "";
    left: 0px;
    top: 0px;
    bottom: 0px;
    right: 0px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 80px;
    color: #f75757;
    opacity: 0;
    transition: all .35s ease;
  }

  .portflio-item:hover .overlay-item {
    opacity: 1;
  }
  .social-link {
  width: 30px;
  height: 30px;
  border: 1px solid #ddd;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #666;
  border-radius: 50%;
  transition: all 0.3s;
  font-size: 0.9rem;
  }
  
  .social-link:hover, .social-link:focus {
  background: #ddd;
  text-decoration: none;
  color: #555;
  }
</style>
@endpush

@section('content')

<section class="hero-wrap hero-wrap-2">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text align-items-end">
      <div class="col-md-9 ftco-animate pb-5">
        <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{route('/')}}">Accueil <i
                class="ion-ios-arrow-forward"></i></a></span> <span>entreprises <i class="ion-ios-arrow-forward"></i></span>
        </p>
        <h1 class="mb-0 bread">Les entreprises</h1>>
      </div>
    </div>
  </div>
</section>

<section class="">
  <div class="container">
    <div class="row text-center">
  
      @foreach ($entreprises as $entreprise)
      <!-- Team item -->
      <div class="col-xl-3 col-sm-6 mb-5">
        <div class="bg-white rounded shadow-sm py-5 px-4"><img
            src="{{ asset('storage/'.$entreprise->logo)}}" alt="" width="100"
            class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm">
          <h5 class="mb-0">{{ $entreprise->nom }}</h5><span class="small text-uppercase text-muted">{{ $entreprise->adresse }}</span>
          <ul class="social mb-0 list-inline mt-3">
            <li class="list-inline-item"><a href="{{$entreprise->lien}}" class="social-link"><i class="fa fa-facebook-f"></i></a></li>
            <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-twitter"></i></a></li>
            <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div><!-- End -->
      @endforeach
    </div>
  </div> 
  {{-- <div class="container">


  <div class="container-fluid">
    <div class="row portfolio-gallery">
      @foreach ($entreprises as $entreprise)
      <div class="col-lg-4 col-md-6">
        <div class="portflio-item position-relative mb-4">
          <a href="{{$entreprise->lien}}" class="popup-gallery">
            <img src="{{ asset('storage/'.$entreprise->logo)}}" alt="" class="img-fluid w-100"
              style="width: 10rem; height: 10rem;">

            <i class="ti-plus overlay-item"></i>
            <div class="portfolio-item-content">
              <h3 class="mb-0 text-white">{{ $entreprise->nom }}</h3>
              <p class="text-white-50">{{ $entreprise->adresse }}</p>
            </div>
          </a>
        </div>
      </div>
      @endforeach --}}


</section>

@include('layouts.newsletter')

@endsection