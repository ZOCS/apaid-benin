@extends('layouts.nav')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/vendors/styles/style.css') }}">
<style>
  .portflio-item .portfolio-item-content {
    position: absolute;
    content: "";
    right: 0px;
    bottom: 0px;
    opacity: 0;
    transition: all .35s ease;
  }

  .portflio-item:before {
    position: absolute;
    content: "";
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.8);
    opacity: 0;
    transition: all .35s ease;
    overflow: hidden;
  }

  .portflio-item:hover:before {
    opacity: 1;
  }

  .portflio-item:hover .portfolio-item-content {
    opacity: 1;
    bottom: 20px;
    right: 30px;
  }

  .portflio-item .overlay-item {
    position: absolute;
    content: "";
    left: 0px;
    top: 0px;
    bottom: 0px;
    right: 0px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 80px;
    color: #f75757;
    opacity: 0;
    transition: all .35s ease;
  }

  .portflio-item:hover .overlay-item {
    opacity: 1;
  }
</style>
@endpush

@section('content')

<section class="hero-wrap hero-wrap-2">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text align-items-end">
      <div class="col-md-9 ftco-animate pb-5">
        <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{route('/')}}">Accueil <i
                class="ion-ios-arrow-forward"></i></a></span> <span>Structures <i class="ion-ios-arrow-forward"></i></span>
        </p>
        <h1 class="mb-0 bread">Les Structures</h1>>
      </div>
    </div>
  </div>
</section>

<section class="">
  <div class="container">
    {{-- <div class="row d-flex">
      <div class="col-md-6 d-flex ftco-animate">
        <p>Even the all-powerful Pointing has no control about the blind texts.<br /> Even the all-powerful Pointing has
          no control about the blind texts.<br /> Even the all-powerful Pointing has no control about the blind texts
        </p>
      </div>
      <div class="col-md-6 d-flex ftco-animate">
        <p>Even the all-powerful Pointing has no control about the blind texts.<br /> Even the all-powerful Pointing has
          no control about the blind texts.<br /> Even the all-powerful Pointing has no control about the blind texts
        </p>
      </div>
    </div><br /> --}}

    <div class="container">
      <div class="row text-center">
    
        @foreach ($structures as $structure)
        <!-- Team item -->
        <div class="col-xl-3 col-sm-6 mb-5">
          <div class="bg-white rounded shadow-sm py-5 px-4"><img src="{{ asset('storage/'.$structure->logo)}}" alt="" width="100"
              class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm">
            <h5 class="mb-0">{{ $structure->nom }}</h5><span class="small text-uppercase text-muted">{{
              $structure->adresse }}</span>
            <ul class="social mb-0 list-inline mt-3">
              <li class="list-inline-item"><a href="{{$structure->lien}}" class="social-link"><i class="fa fa-facebook-f"></i></a>
              </li>
              <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-twitter"></i></a></li>
              <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-instagram"></i></a></li>
              <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div><!-- End -->
        @endforeach
      </div>
    </div>
    

  {{-- <div class="container-fluid">
    <div class="row portfolio-gallery">
      @foreach ($structures as $structure)
      <div class="col-lg-4 col-md-6">
        <div class="portflio-item position-relative mb-4">
          <a href="{{$structure->lien}}" class="popup-gallery">
            <img src="{{ asset('storage/'.$structure->logo)}}" alt="" class="img-fluid w-100"
              style="width: 20rem; height: 20rem;">

            <i class="ti-plus overlay-item"></i>
            <div class="portfolio-item-content">
              <h3 class="mb-0 text-white">{{ $structure->nom }}</h3>
              <p class="text-white-50">{{ $structure->adresse }}</p>
            </div>
          </a>
        </div>
      </div>
      @endforeach

      
    </div>
  </div> --}}

</section>

@include('layouts.newsletter')

@endsection