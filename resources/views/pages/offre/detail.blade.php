@extends('layouts.nav')

@push('css')
    <style>
        .text {
            font-size: 1.5rem;
        }
    </style>
@endpush

@section('content')

<section class="hero-wrap hero-wrap-2" style="background-image: url('account/images/bg_2.jpg');"
    data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs mb-2"><span class="mr-2"><a href="/">Accueil <i
                                class="ion-ios-arrow-forward"></i></a></span> <span class="mr-2"><a
                            href="offre.html">offre <i class="ion-ios-arrow-forward"></i></a></span> <span>Detail offree <i
                            class="ion-ios-arrow-forward"></i></span></p>
                <h1 class="mb-0 bread">Detail offre</h1>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-degree-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ftco-animate">
                <p>
                    <img src="{{ asset('storage/' . $offre->image) }}" alt="" class="img-fluid">
                </p>
                <h1 class="mb-3">{{$offre->entrprise}}</h1>

                <p class="text"><strong class="mb-3 mt-5">Poste :</strong> {{$offre->poste}}</p>

                <p class="text"><strong class="mb-3 mt-5">Type de contrat :</strong> {{ $offre->title }}</p>

                <h2 class="mb-3 mt-5">Description</h2>
                <p>{!! $offre->description !!}</p>


            </div> <!-- .col-md-8 -->
            <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
                <div class="sidebar-box">
                    <form action="#" class="search-form">
                        <div class="form-group">
                            <span class="fa fa-search"></span>
                            <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
                        </div>
                    </form>
                </div>
                <div class="sidebar-box ftco-animate">
                    <div class="categories">
                        <h3>Services</h3>
                        <li><a href="#">Market Analysis <span class="fa fa-chevron-right"></span></a></li>
                        <li><a href="#">Accounting Advisor <span class="fa fa-chevron-right"></span></a></li>
                        <li><a href="#">General Consultancy <span class="fa fa-chevron-right"></span></a></li>
                        <li><a href="#">Structured Assesment <span class="fa fa-chevron-right"></span></a></li>
                    </div>
                </div>

                <div class="sidebar-box ftco-animate">
                    <h3>Recent offre</h3>
                    <div class="block-21 mb-4 d-flex">
                        <a class="offre-img mr-4" style="background-image: url(images/image_1.jpg);"></a>
                        <div class="text">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the
                                    blind texts</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span> Mar. 31, 2020</a></div>
                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="block-21 mb-4 d-flex">
                        <a class="offre-img mr-4" style="background-image: url(images/image_2.jpg);"></a>
                        <div class="text">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the
                                    blind texts</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span> Mar. 31, 2020</a></div>
                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="block-21 mb-4 d-flex">
                        <a class="offre-img mr-4" style="background-image: url(images/image_3.jpg);"></a>
                        <div class="text">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the
                                    blind texts</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span> Mar. 31, 2020</a></div>
                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="sidebar-box ftco-animate">
                    <h3>Tag Cloud</h3>
                    <div class="tagcloud">
                        <a href="#" class="tag-cloud-link">home</a>
                        <a href="#" class="tag-cloud-link">builder</a>
                        <a href="#" class="tag-cloud-link">build</a>
                        <a href="#" class="tag-cloud-link">create</a>
                        <a href="#" class="tag-cloud-link">make</a>
                        <a href="#" class="tag-cloud-link">construction</a>
                        <a href="#" class="tag-cloud-link">house</a>
                        <a href="#" class="tag-cloud-link">architect</a>
                    </div>
                </div>

                <div class="sidebar-box ftco-animate">
                    <h3>Paragraph</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus
                        voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur
                        similique, inventore eos fugit cupiditate numquam!</p>
                </div>
            </div>

        </div>
    </div>
</section> <!-- .section -->

@include('layouts.newsletter')

@endsection