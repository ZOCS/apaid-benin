@extends('layouts.nav')

@push('css')
    <style>
      .btn-secondary {
        margin-left: 1rem;
      }
      .card {
        box-shadow: 0.2rem 0.2rem 0.2rem 0.2rem rgb(236, 236, 236);
      }
    </style>
@endpush

@section('content')
    
<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-end">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{ route('/') }}">Accueil <i class="ion-ios-arrow-forward"></i></a></span> <span>Offre <i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-0 bread">Offre</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section">
    <div class="container">
      <div class="row ">
        @foreach($offres as $offre)
            <div class="card card-box d-flex ftco-animate p-0 ml-10 col-md-4">
              <img class="card-img-top" src="{{ ('storage/' . $offre->image) }}" alt="{{ $offre->entreprise }}">
              <div class="card-body">
                <h1 class="card-title" style="font-size: 1rem; font-style: italic bold;"><strong>{{ $offre->poste }} - {{ $offre->entreprise }}</strong></h1>
                <h4>{{ $offre->title }}</h4>
                <p class="card-text">{!! substr(strip_tags($offre->description, '(...)'), 0, 100) !!}</p>
                <p class="card-text"><small class="text-muted">{{ $offre->created_at->diffForHumans() }} </small></p>
                <div class="card-footer">
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <form action="{{ route('candidat') }}" method="POST" >
                      @csrf
                      <input type="hidden" value="{{ $offre->reference }}" name="ref">
                      <input  class="btn btn-info" type="submit" value="Postuler"/>
                    </form>
                    <a href="{{ route('carrefour.show', $offre) }}" class="btn btn-secondary" style="margin-left: 2rem">En savoir +</a>
                  </div>
                </div>
              </div>
          </div>
        @endforeach
      </div>

         
  </section>

  @include('layouts.newsletter')


@endsection