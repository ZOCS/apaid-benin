@extends('layouts.nav')

@section('content')

<section class="hero-wrap hero-wrap-2" style="background-image: url('account/images/bg_2.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-end">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Accueil <i class="ion-ios-arrow-forward"></i></a></span> <span>Projets <i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-0 bread">Nos Projets</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 ftco-animate">
          @foreach($projets as $projet)
              <div class="cases-wrap d-md-flex align-items-center">
                <div class="img" style="background-image: url({{ asset('storage/' . $projet->image) }});"></div>
                <div class="text pl-md-5">
                    <span class="cat">Projet </span>
                    <h2>{{ $projet->title }}</h2>
                    <p>{!! substr(strip_tags($projet->description, '(...)'), 0, 180) !!}</p>
                    <p><a href="{{ route('projet.show', Crypt::encrypt($projet)) }}" class="btn btn-primary">En savoir +</a></p>
                </div>
              </div>
          @endforeach
          

          {{-- {{$projets->links('pagination::blog-paginate')}} --}}

        </div> <!-- .col-md-8 -->
        <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
          <div class="sidebar-box">
            <form action="{{ route('projet-search') }}" class="search-form">
              <div class="form-group d-flex">
                <input name="search" class="form-control" placeholder="Entrez un mot" type="search" value="{{ request()->search ?? ''}}">
                <button class="btn btn-primary p-4" style="margin-left: 0.5rem; height: 3.5rem;"><i class="ti-search"></i></button>
              </div>
            </form>
          </div>
          <div class="sidebar-box ftco-animate">
            <div class="categories">
              <h3>Categories</h3>
              @foreach($categ as $categorie)
                <li><a href="#">{{$categorie->nom}} <span class="ion-ios-arrow-forward"></span></a></li>
              @endforeach
              
              {{-- <li><a href="#">Mental &amp; Physical Care <span class="ion-ios-arrow-forward"></span></a></li>
              <li><a href="#">People &amp; Relationships <span class="ion-ios-arrow-forward"></span></a></li>
              <li><a href="#">Life Coaching <span class="ion-ios-arrow-forward"></span></a></li> --}}
            </div>
          </div>

          <div class="sidebar-box ftco-animate">
            <h3>Blog récent</h3>
            @foreach($blogs_recent as $blog)
            <div class="block-21 mb-4 d-flex">
              <a class="blog-img mr-4" style="background-image: url({{ asset('storage/' . $blog->image) }});"></a>
              <div class="text">
                <h3 class="heading"><a href="#">{!! substr(strip_tags($blog->description, '(...)'), 0, 80) !!}</a></h3>
                <div class="meta">
                  <div><a href="#"><span class="icon-calendar"></span>{{ Carbon\Carbon::parse($blog->created_at)->locale('fr_FR')->isoFormat('Do MMMM YYYY') }}</a></div>
                  <div><a href="#"><span class="icon-person"></span> {{ $blog->auteur }}</a></div>
                  <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                </div>
              </div>
            </div>
            @endforeach
            {{-- <div class="block-21 mb-4 d-flex">
              <a class="blog-img mr-4" style="background-image: url(account/images/image_2.jpg);"></a>
              <div class="text">
                <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                <div class="meta">
                  <div><a href="#"><span class="icon-calendar"></span> Jan. 30, 2020</a></div>
                  <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                  <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                </div>
              </div>
            </div>
            <div class="block-21 mb-4 d-flex">
              <a class="blog-img mr-4" style="background-image: url(account/images/image_3.jpg);"></a>
              <div class="text">
                <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                <div class="meta">
                  <div><a href="#"><span class="icon-calendar"></span> Jan. 30, 2020</a></div>
                  <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                  <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                </div>
              </div>
            </div>
          </div> --}}

          <div class="sidebar-box ftco-animate">
            <h3>Nuage de tags</h3>
            <div class="tagcloud">
              <a href="#" class="tag-cloud-link">agriculture</a>
              <a href="#" class="tag-cloud-link">pisciculture</a>
              <a href="#" class="tag-cloud-link">elevage</a>
              <a href="#" class="tag-cloud-link">aquaculture</a>
              <a href="#" class="tag-cloud-link">horticulture</a>
              <a href="#" class="tag-cloud-link">jardinage</a>
              <a href="#" class="tag-cloud-link">formation</a>
              <a href="#" class="tag-cloud-link">viticulture</a>
            </div>
          </div>

          <div class="sidebar-box ftco-animate">
            <h3>Informations</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
          </div>
        </div>

      </div>
    </div>
  </section> <!-- .section -->

  @include('layouts.newsletter')


@endsection