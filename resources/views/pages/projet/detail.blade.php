@extends('layouts.nav')

@push('css')
    <style>
      .cus {
        background-color: #fffff;
        border-color: #ffffff;
      }
    </style>
@endpush

@section('content')

<section class="hero-wrap hero-wrap-2" style="background-image: url('account/images/bg_2.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-end">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span class="mr-2"><a href="blog.html">Blog <i class="ion-ios-arrow-forward"></i></a></span> <span>Detail Projet <i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-0 bread">Detail Projet</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section ftco-degree-bg">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 ftco-animate">
            <p>
            <img src="{{ asset('storage/' . $projet->image) }}" alt="" class="img-fluid">
          </p>
          <h1 class="mb-3">{{$projet->title}}</h1>
          
          <p>{!! $projet->description !!}</p>
          
        </div> <!-- .col-md-8 -->
        <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
          <div class="sidebar-box">
            <form action="{{ route('projet-search') }}" class="search-form">
              <div class="form-group">
                <button class="fa fa-search cus" type="submit"></button>
                <input type="text" name="search" class="form-control" placeholder="Entrez un mot" type="search" value="{{ request()->search ?? ''}}">
              </div>
            </form>
          </div>
          <div class="sidebar-box ftco-animate">
            <div class="categories">
              <h3>Categories</h3>
              @foreach($categ as $categorie)
              <li><a href="#">{{$categorie->nom}} <span class="ion-ios-arrow-forward"></span></a></li>
              @endforeach
            </div>
          </div>

          <div class="sidebar-box ftco-animate">
            <h3>Recent Blog</h3>
            @foreach($blogs as $blog)
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url({{ asset('storage/' . $blog->image) }});"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">{!! substr(strip_tags($blog->description, '(...)'), 0, 80) !!}</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>{{ Carbon\Carbon::parse($blog->created_at)->locale('fr_FR')->isoFormat('Do MMMM YYYY') }}</a></div>
                    <div><a href="#"><span class="icon-person"></span> {{ $blog->auteur }}</a></div>
                    {{-- <div><a href="#"><span class="icon-chat"></span> 19</a></div> --}}
                  </div>
                </div>
              </div>
            @endforeach
          <div class="sidebar-box ftco-animate">
            <h3>Tag Cloud</h3>
            <div class="tagcloud">
              <a href="#" class="tag-cloud-link">home</a>
              <a href="#" class="tag-cloud-link">builder</a>
              <a href="#" class="tag-cloud-link">build</a>
              <a href="#" class="tag-cloud-link">create</a>
              <a href="#" class="tag-cloud-link">make</a>
              <a href="#" class="tag-cloud-link">construction</a>
              <a href="#" class="tag-cloud-link">house</a>
              <a href="#" class="tag-cloud-link">architect</a>
            </div>
          </div>

          {{-- <div class="sidebar-box ftco-animate">
            <h3>Paragraph</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
          </div>
        </div> --}}

      </div>
    </div>
  </section> <!-- .section -->

  @include('layouts.newsletter')

@endsection