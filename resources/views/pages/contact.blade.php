@extends('layouts.nav')

@section('content')
    
<section class="hero-wrap hero-wrap-2" style="background-image: url('account/images/bg_2.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-end">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{ route('contact')}}">Accueil <i class="ion-ios-arrow-forward"></i></a></span> <span>Nous contacter <i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-0 bread">Nous contacter</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section bg-light">
      <div class="container">
          <div class="row justify-content-center">
                  <div class="col-md-12">
                      <div class="wrapper">
                          <div class="row no-gutters">
                              <div class="col-lg-8 col-md-7 order-md-last d-flex align-items-stretch">
                                  <div class="contact-wrap w-100 p-md-5 p-4">
                                      <h3 class="mb-4">Entrer en contact</h3>
                                      <div id="form-message-warning" class="mb-4"></div> 
                                <div id="form-message-success" class="mb-4">
                                    Votre message a été envoyé, merci!
                                </div>
                                      <form method="POST" id="contactForm" name="contactForm" class="contactForm" action="{{route('send.mail')}}">
                                          @csrf
                                          <div class="row">
                                              <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label class="label" for="name">Nom et prénom</label>
                                                      <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                                                  </div>
                                              </div>
                                              <div class="col-md-6"> 
                                                  <div class="form-group">
                                                      <label class="label" for="email">Adresse e-mail</label>
                                                      <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                                  </div>
                                              </div>
                                              <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label class="label" for="subject">Sujet</label>
                                                      <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
                                                  </div>
                                              </div>
                                              <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label class="label" for="#">Message</label>
                                                      <textarea name="message" class="form-control" id="message" cols="30" rows="4" placeholder="Message"></textarea>
                                                  </div>
                                              </div>
                                              <div class="col-md-12">
                                                  <div class="form-group">
                                                      <input type="submit" value="Envoyer le message" class="btn btn-primary">
                                                      <div class="submitting"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                              <div class="col-lg-4 col-md-5 d-flex align-items-stretch">
                                  <div class="info-wrap bg-primary w-100 p-md-5 p-4">
                                      <h3>Rentrons en contact</h3>
                                      <p class="mb-4">{{ ('Nous sommes ouverts à toute suggestion ou simplement pour discuter') }}</p>
                              <div class="dbox w-100 d-flex align-items-start">
                                  <div class="icon d-flex align-items-center justify-content-center">
                                      <span class="fa fa-map-marker"></span>
                                  </div>
                                  <div class="text pl-3">
                                  <p><span>Address:</span> 198 West 21th Street, Suite 721 New York NY 10016</p>
                                </div>
                            </div>
                              <div class="dbox w-100 d-flex align-items-center">
                                  <div class="icon d-flex align-items-center justify-content-center">
                                      <span class="fa fa-phone"></span>
                                  </div>
                                  <div class="text pl-3">
                                  <p><span>Phone:</span> <a href="tel://22962201028">+229 6220 1028</a></p>
                                </div>
                            </div>
                              <div class="dbox w-100 d-flex align-items-center">
                                  <div class="icon d-flex align-items-center justify-content-center">
                                      <span class="fa fa-paper-plane"></span>
                                  </div>
                                  <div class="text pl-3">
                                  <p><span>Email:</span> <a href="mailto:contact@apaid-benin.org">contact@apaid-benin.org</a></p>
                                </div>
                            </div>
                              <div class="dbox w-100 d-flex align-items-center">
                                  <div class="icon d-flex align-items-center justify-content-center">
                                      <span class="fa fa-globe"></span>
                                  </div>
                                  <div class="text pl-3">
                                  <p><span>Website</span> <a href="/">apaid.com</a></p>
                                </div>
                            </div>
                        </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
      </div>
  </section>

     <div id="map" class="map"></div>

  @include('layouts.newsletter')

@endsection