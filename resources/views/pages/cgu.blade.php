@extends('layouts.nav')

@section('content')

<section class="hero-wrap hero-wrap-2" style="background-image: url('account/images/bg_2.jpg');"
    data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i
                                class="ion-ios-arrow-forward"></i></a></span> <span>Conditions Generales d'Utilisations (CGU)  <i
                            class="ion-ios-arrow-forward"></i></span></p>
                <h1 class="mb-0 bread">Conditions Generales d'Utilisations (CGU)</h1>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-no-pt bg-light">
    <div class="container">
        <div class="row d-flex no-gutters">
            {{-- <div class="col-md-6 d-flex">
                <div class="img img-video d-flex align-self-stretch align-items-center justify-content-center justify-content-md-center mb-4 mb-sm-0"
                    style="background-image:url(images/about.jpg);">
                </div>
            </div> --}}
            <div class="col-md-12 pl-md-10 py-md-5">
                <div class="heading-section pl-md-4 pt-md-5">
                    <span class="subheading">Bienvenue a APAID</span>
                    <h2 class="mb-4">{{ ('Conditions générales d’utilisation du site apaid-benin.org') }}</h2>

                    <h4>Article 1:Objet</h4> 
                    <p>Les présentes CGU ou Conditions Générales d’Utilisation encadrent juridiquement l’utilisation des
                    services du site apaid-benin.org (ci-après dénommé « le site»).&nbsp;
                    Constituant le contrat entre l’organisation APAID, l’Utilisateur, l’accès au site doit être précédé de
                    l’acceptation de ces CGU. L’accès à cette plateforme signifie l’acceptation des présentes CGU.</p>

                    <h4>Article 2:<span>Mentions légales</span></h4>
                    <p>L’édition du site apaid-benin.org est assurée par l’organisation APAID inscrite au RCS sous le numéro 451432228, dont le siège social est localisé au 3 Rue Docteur Joubert, 59110, La Madeleine, France Métropolitaine.<br><br>
                    L’hébergeur du site apaid-benin.org est la société LWS, sise au 10 Rue Penthièvre, 75008 PARIS,
                    France.</p>


                    <h4>Article 3: <span>Accès au site</span></h4>
                    <p>Le site apaid-benin.org permet d’accéder gratuitement aux services suivants:<br>
                        <ul>
                            <li>Vente de produits issus des innovations et recherche (matière premières, etc…);</li>
                            <li>Réalisations des fiches techniques;</li>
                            <li>Gestion des ressources humaines;</ol>
                            <li>Formations et elearning;</li>
                            <li>Reseautage des organisations et associations intervenants dans le développement
                            durable.</li>
                        </ul><br>
                        Le site est accessible gratuitement depuis n’importe où par tout utilisateur disposant d’un accès à
                        Internet. Tous les frais nécessaires pour l’accès aux services (matériel informatique, connexion
                        Internet…) sont à la charge de l’utilisateur.<br><br>
                        L’accès aux services dédiés aux membres s’effectue à l’aide d’un identifiant(email) et d’un mot de
                        passe.<br><br>
                        Pour des raisons de maintenance ou autres, l’accès au site peut être interrompu ou suspendu par
                        l’éditeur sans préavis ni justification.
                    </p>


                    <h4>Article 4: Collecte des données</h4>
                    <p>Pour la création du compte de l’Utilisateur, la collecte des informations au moment de l’inscription
                    sur le site est nécessaire et obligatoire. Conformément à la loi n°78-17 du 6 janvier relative à
                    l’informatique, aux fichiers et aux libertés, la collecte et le traitement d’informations personnelles
                    s’effectuent dans le respect de la vie privée.<br><br>
                    Suivant la loi Informatique et Libertés en date du 6 janvier 1978, articles 39 et 40, l’Utilisateur
                    dispose du droit d’accéder, de rectifier, de supprimer et d’opposer ses données personnelles.
                    L’exercice de ce droit s’effectue par:<br>
                        <ul>
                            <li>Le formulaire de contact;</li>
                            <li>Son espace client.</li>
                        </ul>
                    </p>


                    <h4>Article 5: Propriété intellectuelle</h4>
                    <p>Les marques, logos ainsi que les contenus du site apaid-benin.org (illustrations graphiques, textes…)
                    sont protégés par le Code de la propriété intellectuelle et par le droit d’auteur.<br><br>
                    La reproduction et la copie des contenus par l’Utilisateur requièrent une autorisation préalable du
                    site. Dans ce cas, toute utilisation à des usages commerciaux ou à des fins publicitaires est proscrite.</p>


                    <h4>Article 6: Responsabilité</h4>
                    <p>Bien que les informations publiées sur le site soient réputées fiables, le site se réserve la faculté
                    d’une non-garantie de la fiabilité des sources.<br><br>
                    Les informations diffusées sur le site apaid-benin.org sont présentées à titre purement informatif et
                    sont sans valeur contractuelle. En dépit des mises à jour régulières, la responsabilité du site ne peut
                    être engagée en cas de modification des dispositions administratives et juridiques apparaissant après
                    la publication. Il en est de même pour l’utilisation et l’interprétation des informations communiquées
                    sur la plateforme.<br><br>
                    Le site décline toute responsabilité concernant les éventuels virus pouvant infecter le matériel
                    informatique de l’Utilisateur après l’utilisation ou l’accès à ce site.
                    Le site ne peut être tenu pour responsable en cas de force majeure ou du fait imprévisible et
                    insurmontable d’un tiers.<br><br>
                    La garantie totale de la sécurité et la confidentialité des données n’est pas assurée par le site.
                    Cependant, le site s’engage à mettre en œuvre toutes les méthodes requises pour le faire au mieux.</p>


                    <h4>Article 7: Liens hypertextes</h4>
                    <p>Le site peut être constitué de liens hypertextes. En cliquant sur ces derniers, l’Utilisateur sortira de la
                    plateforme. Cette dernière n’a pas de contrôle et ne peut pas être tenue responsable du contenu des
                    pages web relatives à ces liens.</p>


                    <h4>Article 8: Cookies</h4>
                    <p>Lors des visites sur le site, l’installation automatique d’un cookie sur le logiciel de navigation de
                    l’Utilisateur peut survenir.<br><br>
                    Les cookies correspondent à de petits fichiers déposés temporairement sur le disque dur de
                    l’ordinateur de l’Utilisateur.<br><br> Ces cookies sont nécessaires pour assurer l’accessibilité et la navigation
                    sur le site. Ces fichiers ne comportent pas d’informations personnelles et ne peuvent pas être utilisés
                    pour l’identification d’une personne.<br><br>
                    L’information présente dans les cookies est utilisée pour améliorer les performances de navigation
                    sur le site apaid-benin.org.<br><br>
                    En naviguant sur le site, l’Utilisateur accepte les cookies. Leur désactivation peut s’effectuer via les
                    paramètres du logiciel de navigation.</p>



                    <h4>Article 9: Publication par l’Utilisateur</h4>
                    <p>Le site apaid-benin.org permet aux membres de publier des commentaires.<br><br>
                    Dans ses publications, le membre est tenu de respecter les règles de la Netiquette ainsi que les règles
                    de droit en vigueur.<br><br>
                    Le site dispose du droit d’exercer une modération à priori sur les publications et peut refuser leur
                    mise en ligne sans avoir à fournir de justification.<br><br>
                    Le membre garde l’intégralité de ses droits de propriété intellectuelle. Toutefois, toute publication
                    sur le site implique la délégation du droit non exclusif et gratuit à la société éditrice de représenter,
                    reproduire, modifier, adapter, distribuer et diffuser la publication n’importe où et sur n’importe quel
                    support pour la durée de la propriété intellectuelle. Cela peut se faire directement ou par
                    l’intermédiaire d’un tiers autorisé. Cela concerne notamment le droit d’utilisation de la publication
                    sur le web et sur les réseaux de téléphonie mobile.<br><br>
                    À chaque utilisation, l’éditeur s’engage à mentionner le nom du membre à proximité de la
                    publication.<br><br>
                    L’Utilisateur est tenu responsable de tout contenu qu’il met en ligne. L’Utilisateur s’engage à ne pas
                    publier de contenus susceptibles de porter atteinte aux intérêts de tierces personnes. Toutes
                    procédures engagées en justice par un tiers lésé à l’encontre du site devront être prises en charge
                    par l’Utilisateur.<br><br>
                    La suppression ou la modification par le site du contenu de l’Utilisateur peut s’effectuer à tout
                    moment, pour n’importe quelle raison et sans préavis.</p>

                    <h4>Article 11: Durée du contrat</h4>
                    <p>Le présent contrat est valable pour une durée indéterminée. Le début de l’utilisation des services du
                    site marque l’application du contrat à l’égard de l’Utilisateur.</p>


                    <h4>Article 12: Droit applicable et juridiction compétente</h4>
                    <p>Le présent contrat est soumis à la législation Béninoise. L’absence de résolution à l’amiable des cas
                    de litige entre les parties implique le recours aux tribunaux Béninois compétents pour régler le
                    contentieux.</p>

                    
                    {{-- <p>A small river named Duden flows by their place and supplies it with the necessary
                        regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your
                        mouth.</p> --}}
                </div>
            </div>
        </div>
    </div>
</section>







@include('layouts.newsletter')

@endsection