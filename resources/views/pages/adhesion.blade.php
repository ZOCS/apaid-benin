@extends('layouts.nav')

@section('content')
    
<section class="hero-wrap hero-wrap-2" style="background-image: url('account/images/bg_2.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-end">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{ route('contact')}}">Accueil <i class="ion-ios-arrow-forward"></i></a></span> <span>Nous contacter <i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-0 bread">Devenir membre de notre organisation</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section bg-light" >
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12">
            <div class="wrapper">
              <div class="row no-gutters">
                <div class="col-lg-8 col-md-7 order-md-last d-flex align-items-stretch">
                  <div class="contact-wrap w-100 p-md-5 p-4">
                    <h3 class="mb-4">Devenir membre</h3>
                    <div id="form-message-warning" class="mb-4"></div>
                    <form method="POST" id="contactForm" name="contactForm" class="contactForm" action="{{ route('adhesion.store') }}" enctype="multipart/form-data">
                      @csrf
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="label" for="name">{{__('Nom de la structure')}}</label>
                            <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="label" for="name">Nom du responsable</label>
                            <input type="text" class="form-control" name="responsable" id="name"
                              placeholder="Le responsable">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="label">Preuve d'existence</label>
                            <div class="custom-file">
                              <label class="custom-file-label" for="file-demande">Document de demande
                                d'adhesion</label>
                              <input type="file"  class="custom-file-input" name="demande"
                                id="file-demande">
                            </div>
                          </div>
                          <p><strong>NB:</strong><span style="color: red"> Dans le document il faut specifier l'IFU, le RCCM, Siege etc..</span></p>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="label" for="domaine">{{ __('Domaine d\'intervention') }}</label>
                            <input name="domaine"  type="text" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="label" for="domaine">{{ __('Adresse Mail') }}</label>
                            <input name="email" type="email" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="label" for="domaine">{{ __('Contact structure') }}</label>
                            <input name="phone" type="tel" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="label" for="pays">Pays</label>
                            <select name="pays"  class="form-control" id="pays">
                              <option value="Benin">Benin</option>
                              <option value="Nigeria">Nigeria</option>
                              <option value="Togo">Togo</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="label" for="subject">Ville</label>
                            <input type="text" class="form-control"  name="ville" id="subject"
                              placeholder="Cotonou">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="label" for="message">Objectifs et Vision</label>
                            <textarea name="objectif" class="form-control" id="message" cols="30"
                              rows="4" placeholder="Objectifs et Vision"></textarea>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="label" for="message">Innovation realisees</label>
                            <textarea name="realisation"  class="form-control" id="message"
                              cols="30" rows="4" placeholder="Vos realisation"></textarea>
                          </div>
                        </div>
                         <div class="col-md-12">
                            <div class="form-group">
                              <input type="submit" @click="send" value="Devenir partenaire" class="btn btn-primary">
                              <div class="submitting"></div>
                            </div>
                          </div>
                      </div>
                    </form>
                    
                  </div>
                </div>
                <div class="col-lg-4 col-md-5 d-flex align-items-stretch">
                  <div class="info-wrap bg-primary w-100 p-md-5 p-4">
                    <h3>Condition pour devenir membre</h3>
                    <p class="mb-4">{{ ('Nous sommes ouverts à toute suggestion ou simplement pour discuter') }}</p>
                    <div class="dbox w-100 d-flex align-items-start">
                      <div class="text pl-3">
                        <p>Vous devez etre une entrepise ou une ONG enregistrer.<br /></p>
                      </div>
                    </div>
      
                    <div class="dbox w-100 d-flex align-items-center">
                      <div class="icon d-flex align-items-center justify-content-center">
                        <span class="fa fa-globe"></span>
                      </div>
                      <div class="text pl-3">
                        <p><span>Website</span> <a href="/">apaid-benin.org</a></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>

     <div id="map" class="map"></div>

  @include('layouts.newsletter')

@endsection