@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<div class="comment-form-wrap pt-5">
    <h3 class="mb-5">Laissez un commentaire</h3>
    {{-- <form action="{{ url("{$blog->path()}/comments") }}" class="p-5 bg-light">
      <div class="form-group">
        <label for="message">Commentaire</label>
        <textarea name="" id="message" cols="25" rows="10" class="form-control" placeholder="Votre commentaire"></textarea>
      </div>
      <div class="form-group">
        <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary">
      </div>

    </form> --}}
  </div>

{{-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
 --}}
 <ul>
    @foreach($categories as $categorie)
        <li>
            <a href="#">{{$categorie->nom}}</a>

            @php
                $children = App\Models\Categorie::where('parent_id', $categorie->id)->get();
            @endphp

            @if($children->isNotEmpty())
                <div class="dropdown">
                    @foreach($children as $child)
                        <div class="dropdown">
                            <h4>{{$child->nom}}</h4>
                            @php
                                $grandChild = App\Models\Categorie::where('parent_id', $child->id)->get();
                            @endphp
                            @if($grandChild->isNotEmpty())
                                <ul>
                                    @foreach($grandChild as $c)
                                    <li><a href="#">{{$c->nom}}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    @endforeach
                </div>
            @endif
        </li>
    @endforeach
</ul>


// slider.nouislider.on('update', function(value){
    //     @this.set('min_price', value[0]);
    //     @this.set('max_price', value[1]);
    // });


    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5">
                    <div class="sidebar">
                        <div class="sidebar__item widget mercado-widget categories-widget">
                            <h4 class="widget-title">All Categories</h4>
                            <div class="widget-content">
                                @foreach ($categ as $categorie)
                                <ul class="list-category list-style vertical-list main-category" data-show="6">
                                    <li class="category-item has-child-cate">
                                        <a href="{{ route('boutique.index', ['categorie' => $categorie->slug ]) }}"
                                            class="cate-link">{{
                                            $categorie->nom }}</a>
                                        <span class="toggle-control">+</span>
                                        @php
                                        $children = App\Models\Categorie::where('parent_id', $categorie->id)->get();
                                        @endphp
                                        @if($children->isNotEmpty())
                                        <ul class="sub-cate">
                                            @foreach ($children as $child)
                                            <li class="category-item"><a
                                                    href="{{ route('boutique.index', ['categorie' => $child->slug ]) }}"
                                                    class="cate-link">{{$child->nom}}</a>
                                                @php
                                                $grandChild = App\Models\Categorie::where('parent_id', $child->id)->get();
                                                @endphp
                                                @if($grandChild->isNotEmpty())
                                                <ul>
                                                    @foreach($grandChild as $c)
                                                    <li class="category-item"><a
                                                            href="{{ route('boutique.index', ['categorie' => $c->slug ]) }}"
                                                            class="cate-link">{{$c->nom}}</a></li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </li>
                                            @endforeach
                                        </ul>
                                        @endif
    
                                    </li>
    
                                </ul>
    
                                @endforeach
                            </div>
                        </div>
                        <div class="single-widget range">
                            <h3 class="title">Shop by Price</h3>
                            <div class="price-filter">
                                <div class="price-filter-inner">
                                    <div id="slider-range"></div>
                                    <div class="price_slider_amount">
                                        <div class="label-input">
                                            <span>Range:</span><input type="text" id="amount" name="price"
                                                placeholder="Add Your Price" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <h4>Price <span class="text-info"></span></h4>
                            <div class="price-range-wrap">
                                <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                    id="amount" data-min="0" data-max="20000">
                                    <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                </div>
                                <div class="range-slider">
                                    <div class="price-input">
                                        <input type="text" id="minprice" name="minprice">
                                        <input type="text" id="maxprice" name="maxprice">
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="sidebar__item sidebar__item__color--option">
    
                            <ul class="list-style vertical-list main-category" data-show="6">
    
                                @foreach($categ as $categorie)
                                <li class="list-item"><a
                                        href="{{ route('boutique.index', ['categorie' => $categorie->slug ]) }}">{{$categorie->nom}}
                                        <span class="toggle-control">+</span></a>
                                    @php
                                    $children = App\Models\Categorie::where('parent_id', $categorie->id)->get();
                                    @endphp
                                    @if($children->isNotEmpty())
                                    <ul class="sub-cate">
                                        @foreach($children as $child)
                                        <li class="category-item"><a
                                                href="{{ route('boutique.index', ['categorie' => $child->slug ]) }}">{{$child->nom}}</a>
                                            @php
                                            $grandChild = App\Models\Categorie::where('parent_id', $child->id)->get();
                                            @endphp
                                            @if($grandChild->isNotEmpty())
                                            <ul>
                                                @foreach($grandChild as $c)
                                                <li><a
                                                        href="{{ route('boutique.index', ['categorie' => $c->slug ]) }}">{{$c->nom}}</a>
                                                </li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                                @endforeach
    
                        </div>
                        <div class="sidebar__item">
                            <div class="latest-product__text">
                                <h4>Latest Products</h4>
                                <div class="latest-product__slider owl-carousel">
                                    <div class="latest-prdouct__slider__item">
                                        @foreach($produit_latests as $produit_latest)
                                        <a href="#" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="{{ asset('storage/' . $produit_latest->image) }}" alt=""
                                                    style="width: 110px; height: 110px;">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>{{ $produit_latest->nom }}</h6>
                                                <span>{{ $produit_latest->prix }} XOF</span>
                                            </div>
                                        </a>
                                        @endforeach
                                    </div>
                                    <div class="latest-prdouct__slider__item">
                                        {{-- <a href="#" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="shop/img/latest-product/lp-1.jpg" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>Crab Pool Security</h6>
                                                <span>$30.00</span>
                                            </div>
                                        </a>
                                        <a href="#" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="shop/img/latest-product/lp-2.jpg" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>Crab Pool Security</h6>
                                                <span>$30.00</span>
                                            </div>
                                        </a>
                                        <a href="#" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="shop/img/latest-product/lp-3.jpg" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>Crab Pool Security</h6>
                                                <span>$30.00</span>
                                            </div>
                                        </a> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
    
                    </div>
                </div>
                <div class="col-lg-9 col-md-7">
                    <div class="product__discount">
                        <div class="section-title product__discount__title text-align-left">
                            <h2>Innovations promo</h2>
                        </div>
                        <div class="row">
                            <div class="product__discount__slider owl-carousel">
                                @foreach($produit_promos as $produit_promo)
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="{{ asset('storage/' . $produit_promo->image) }}"
                                            style="width: 200px; height: 200px;">
                                            <div class="product__discount__percent">-20%</div>
                                            <ul class="product__item__pic__hover">
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                <li><a href="{{ route('produit.show', $produit_promo->slug) }}"><i
                                                            class="fa fa-eye"></i></a></li>
                                                <li>
                                                    <form action="{{route('cart.store')}}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $produit_promo->id }}">
                                                        <button type="submit"><i class="fa fa-shopping-cart"></i></button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>{{ $produit_promo->nom }}</span>
                                            <h5><a href="#"></a></h5>
                                            <div class="product__item__price">{{ $produit_promo->prix }} xof<span>{{
                                                    $produit_promo->prix_promo
                                                    }} xof</span></div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
    
                        <div class="filter__item">
                            <div class="row">
                                <filtercomponent></filtercomponent>
                                <div class="col-lg-4 col-md-4">
                                    @if(request()->input())
                                    <div class="filter__found">
                                        <h6><span>{{ $produits->total() }}</span> Produits trouvé(s) pour la recherche
                                            <strong>{{
                                                request()->search }}</strong></h6>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="filter__option">
                                        <span class="icon_grid-2x2"></span>
                                        <span class="icon_ul"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php
                        $items = Cart::instance('wishlist')->content()->pluck('id');
                        @endphp
                        <div class="row productCats">
                            @foreach($produits as $produit)
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg"
                                        data-setbg="{{ asset('storage/' . $produit->image) }}"
                                        style="width: 200px; height: 200px;">
                                        @if($produit->promo)
                                        <div class="product__discount__percent">-20%</div>
                                        @endif
                                        <ul class="product__item__pic__hover">
                                            @if ($items->contains($produit->id))
                                            <li>
                                                <a href="submit" class="heart"><i class="fa fa-heart fill-heart"></i></a>
                                            </li>
                                            @else
                                            <li>
                                                <form action="{{route('whislist.store')}}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{ $produit->id }}">
                                                    <input type="hidden" name="name" value="{{ $produit->nom }}">
                                                    <input type="hidden" name="qty" value="1">
                                                    <input type="hidden" name="prix" value="{{ $produit->prix }}">
                                                    <button type="submit"><i class="fa fa-heart"></i></button>
                                                </form>
                                            </li>
                                            @endif
                                            <li><a href="{{ route('produit.show', $produit->slug) }}"><i
                                                        class="fa fa-eye"></i></a></li>
                                            <li>
                                                <form action="{{route('cart.store')}}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{ $produit->id }}">
                                                    <button type="submit"><i class="fa fa-shopping-cart"></i></button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="product__discount__item__text">
                                        {{-- <span>@foreach($produit->categories as $category) {{ $category->nom }}
                                            @endforeach</span> --}}
                                        <h5><a href="#">{{$produit->nom}}</a></h5>
                                        @if($produit->prix_promo)
                                        <div class="product__item__price">{{ $produit->prix_promo }} xof<span>{{
                                                $produit->prix }} xof</span>
                                        </div>
                                        @else
                                        <div class="product__item__price">{{ $produit->prix }} xof<span></span></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
    
                            @endforeach
    
                        </div>
                        {{-- <div class="product__pagination">
                            <a href="#">1</a>
                            <a href="#">2</a>
                            <a href="#">3</a>
                            <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                        </div> --}}
                        {{-- <div class=" justify-content-center">
                            {{ $produits->appends(request()->input())->links() }}
                        </div> --}}
    
                    </div>
    
                </div>
                <div class="product__pagination d-flex justify-content-center">
                    {{ $produits->appends(request()->input())->links() }}
                </div>
            </div>
    </section>
    <!-- Product Section End -->
    
    
    #F7941D