<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>APAID - Tableau de bord</title>

	<!-- Site favicon -->
	{{-- <link rel="apple-touch-icon" sizes="180x180" href="account/images/logo 2.png"> --}}
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('account/images/logo 2.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('account/images/logo 2.png') }}">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap"
		rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/vendors/styles/core.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/vendors/styles/icon-font.min.css')}}">
	<link rel="stylesheet" type="text/css"
		href="{{ asset('deskapp/src/plugins/datatables/css/dataTables.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css"
		href="{{ asset('deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/vendors/styles/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp/src/plugins/switchery/switchery.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp/src/plugins/jquery-steps/jquery.steps.css')}}">
	<!-- bootstrap-tagsinput css -->
	<link rel="stylesheet" type="text/css"
		href="{{ asset('deskapp/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
	<!-- bootstrap-touchspin css -->
	<link rel="stylesheet" type="text/css"
		href="{{ asset('deskapp/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}">

	<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
	<script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.js"></script>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>


	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>
</head>

<body>
	<div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="{{ asset('deskapp/vendors/images/admin-logo.svg') }}" alt="" style="width: 14rem; height: 7rem;"></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Chargement...
			</div>
		</div>
	</div>
	{!! Toastr::message() !!}
	<div class="header">
		<div class="header-left">
			<div class="menu-icon dw dw-menu"></div>
			<div class="search-toggle-icon dw dw-search2" data-toggle="header_search"></div>
			<div class="header-search">
				<form>
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon"></i>
						<input type="text" class="form-control search-input" placeholder="Search Here">
						<div class="dropdown">
							<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
								<i class="ion-arrow-down-c"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">From</label>
									<div class="col-sm-12 col-md-10">
										<input class="form-control form-control-sm form-control-line" type="text">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">To</label>
									<div class="col-sm-12 col-md-10">
										<input class="form-control form-control-sm form-control-line" type="text">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Subject</label>
									<div class="col-sm-12 col-md-10">
										<input class="form-control form-control-sm form-control-line" type="text">
									</div>
								</div>
								<div class="text-right">
									<button class="btn btn-primary">Search</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="dashboard-setting user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="javascript:;" data-toggle="right-sidebar">
						<i class="dw dw-settings2"></i>
					</a>
				</div>
			</div>
			<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy dw dw-notification"></i>
						<span class="badge notification-active"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
								@php
								$order = App\Models\User::find(1);
								@endphp
								@foreach ($order->unreadNotifications as $notification)
								<li>
									<a href="#">
										<img src="{{ asset('deskapp/vendors/images/img.jpg') }}" alt="">
										<h3>{{ $notification->data['order_lname'] }}</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
									</a>
								</li>
								@endforeach


							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon">
							<img src="{{ asset('deskapp/vendors/images/photo1.jpg') }}" alt="">
						</span>
						<span class="user-name"> @if(auth()->check()) {{ Auth::user()->name }} @else redirect('login')
							@endif </span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<a class="dropdown-item" href="profile.html"><i class="dw dw-user1"></i> Profile</a>
						<a class="dropdown-item" href="profile.html"><i class="dw dw-settings2"></i> Parametres</a>
						<a class="dropdown-item" href="{{ route('logout') }}"
							onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
								class="dw dw-logout"></i> Se deconnecter</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none"> @csrf</form>
					</div>
				</div>
			</div>
			<div class="github-link">
				<a href="/" target="_blank"><img
						src="{{ asset('deskapp/vendors/images/github.svg') }}" alt=""></a>
			</div>
		</div>
	</div>

	<div class="right-sidebar">
		<div class="sidebar-title">
			<h3 class="weight-600 font-16 text-blue">
				Layout Settings
				<span class="btn-block font-weight-400 font-12">User Interface Settings</span>
			</h3>
			<div class="close-sidebar" data-toggle="right-sidebar-close">
				<i class="icon-copy ion-close-round"></i>
			</div>
		</div>
		<div class="right-sidebar-body customscroll">
			<div class="right-sidebar-body-content">
				<h4 class="weight-600 font-18 pb-10">Header Background</h4>
				<div class="sidebar-btn-group pb-30 mb-10">
					<a href="javascript:void(0);" class="btn btn-outline-primary header-white active">White</a>
					<a href="javascript:void(0);" class="btn btn-outline-primary header-dark">Dark</a>
				</div>

				<h4 class="weight-600 font-18 pb-10">Sidebar Background</h4>
				<div class="sidebar-btn-group pb-30 mb-10">
					<a href="javascript:void(0);" class="btn btn-outline-primary sidebar-light ">White</a>
					<a href="javascript:void(0);" class="btn btn-outline-primary sidebar-dark active">Dark</a>
				</div>

				<h4 class="weight-600 font-18 pb-10">Menu Dropdown Icon</h4>
				<div class="sidebar-radio-group pb-10 mb-10">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebaricon-1" name="menu-dropdown-icon" class="custom-control-input"
							value="icon-style-1" checked="">
						<label class="custom-control-label" for="sidebaricon-1"><i class="fa fa-angle-down"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebaricon-2" name="menu-dropdown-icon" class="custom-control-input"
							value="icon-style-2">
						<label class="custom-control-label" for="sidebaricon-2"><i class="ion-plus-round"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebaricon-3" name="menu-dropdown-icon" class="custom-control-input"
							value="icon-style-3">
						<label class="custom-control-label" for="sidebaricon-3"><i
								class="fa fa-angle-double-right"></i></label>
					</div>
				</div>

				<h4 class="weight-600 font-18 pb-10">Menu List Icon</h4>
				<div class="sidebar-radio-group pb-30 mb-10">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-1" name="menu-list-icon" class="custom-control-input"
							value="icon-list-style-1" checked="">
						<label class="custom-control-label" for="sidebariconlist-1"><i
								class="ion-minus-round"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-2" name="menu-list-icon" class="custom-control-input"
							value="icon-list-style-2">
						<label class="custom-control-label" for="sidebariconlist-2"><i class="fa fa-circle-o"
								aria-hidden="true"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-3" name="menu-list-icon" class="custom-control-input"
							value="icon-list-style-3">
						<label class="custom-control-label" for="sidebariconlist-3"><i class="dw dw-check"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-4" name="menu-list-icon" class="custom-control-input"
							value="icon-list-style-4" checked="">
						<label class="custom-control-label" for="sidebariconlist-4"><i
								class="icon-copy dw dw-next-2"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-5" name="menu-list-icon" class="custom-control-input"
							value="icon-list-style-5">
						<label class="custom-control-label" for="sidebariconlist-5"><i
								class="dw dw-fast-forward-1"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-6" name="menu-list-icon" class="custom-control-input"
							value="icon-list-style-6">
						<label class="custom-control-label" for="sidebariconlist-6"><i class="dw dw-next"></i></label>
					</div>
				</div>

				<div class="reset-options pt-30 text-center">
					<button class="btn btn-danger" id="reset-settings">Reset Settings</button>
				</div>
			</div>
		</div>
	</div>

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="index.html">
				<img src="{{ asset('deskapp/vendors/images/admin-logo.svg') }}" alt="" class="dark-logo">
				<img src="{{ asset('deskapp/vendors/images/admin-logo.svg') }}" alt="" class="light-logo">
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li class="{{ Request::is('admin.dashboard') ? 'active' : '' }}">
						<a href="{{ route('admin.dashboard') }}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-house-1"></span><span class="mtext">Accueil</span>
						</a>
						{{-- <ul class="submenu">
							<li><a href="index.html">Dashboard style 1</a></li>
							<li><a href="index2.html">Dashboard style 2</a></li>
						</ul> --}}
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-shopping-cart2"></span><span class="mtext">Boutique</span>
						</a>
						<ul class="submenu"><i class="icon-copy dw dw-shopping-cart2"></i>
							<li class="{{ Request::is('produit.index') ? 'active' : '' }}"><a href="{{ route('produit.index') }}">Liste des produits</a></li>
							<li class="{{ Request::is('admin.insert-product') ? 'active' : '' }}"><a href="{{ route('admin.insert-product')}}">Ajout Produit</a></li>
							<li class="{{ Request::is('admin.categorie.index') ? 'active' : '' }}"><a href="{{route('admin.categorie.index')}}">Categorie</a></li>
							<li class="{{ Request::is('admin.mesure.index') ? 'active' : '' }}"><a href="{{route('admin.mesure.index')}}">Mesures</a></li>
							<li><a href="form-wizard.html">Commande de produits</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-library"></span><span class="mtext">Blog</span>
						</a>
						<ul class="submenu">
							<li class="{{ Request::is('admin.blog.index') ? 'active' : '' }}"><a href="{{ route('admin.blog.index') }}">Liste BLog</a></li>
							<li class="{{ Request::is('admin.blog.create') ? 'active' : '' }}"><a href="{{ route('admin.blog.create') }}">Ajout Blog</a></li>
						</ul>
					</li>
					<li>
						<a href="calendar.html" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-calendar1"></span><span class="mtext">Newsletter</span>
						</a>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-email"></span><span class="mtext"> MailBox </span>
						</a>
						<ul class="submenu">
							<li><a href="ui-buttons.html">InBox</a></li>
							<li><a href="ui-cards.html">Composer Mail</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-paint-brush"></span><span class="mtext">Gestion Projet</span>
						</a>
						<ul class="submenu">
							<li class="{{ Request::is('admin.list-projet') ? 'active' : '' }}"><a href="{{ route('admin.list-projet') }}">Liste projet</a></li>
							<li class="{{ Request::is('admin.insert-projet') ? 'active' : '' }}"><a href="{{ route('admin.insert-projet') }}">Ajout projet</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-right-arrow1"></span><span class="mtext">Autres</span>
						</a>
						<ul class="submenu">
							<li class="{{ Request::is('admin.services.index') ? 'active' : '' }}"><a href="{{ route('admin.services.index') }}">Services</a></li>
							<li class="{{ Request::is('admin.services.create') ? 'active' : '' }}"><a href="{{route('admin.services.create')}}">Ajout Services</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-right-arrow1"></span><span class="mtext">Gestions des Offres</span>
						</a>
						<ul class="submenu">
							<li class="{{ Request::is('admin.offre.index') ? 'active' : '' }}"><a href="{{ route('admin.offre.index') }}">Liste des Offres</a></li>
							<li class="{{ Request::is('admin.offre.create') ? 'active' : '' }}"><a href="{{route('admin.offre.create')}}">Ajout Offres</a></li>
							<li><a href="forgot-password.html">Postulant</a></li>
							<li><a href="reset-password.html">Reset Password</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-browser2"></span><span class="mtext">Partenaires</span>
						</a>
						<ul class="submenu">
							<li><a href="400.html">Liste Partenaire</a></li>
							<li><a href="403.html">Ajout Partenaire</a></li>
						</ul>
					</li>

					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-copy"></span><span class="mtext">Organisme</span>
						</a>
						<ul class="submenu">
							<li class="{{ Request::is('admin.listOrganisme') ? 'active' : '' }}"><a href="{{ route('admin.listOrganisme') }}">Liste </a></li>
							<li class="{{ Request::is('admin.ajout.organisme') ? 'active' : '' }}"><a href="{{ route('admin.ajout.organisme') }}">Ajout </a></li>

						</ul>
					</li>

					<li>
						<a href="{{ route('admin.user.index') }}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-diagram"></span><span class="mtext">Gestion Utilisateur</span>
						</a>
					</li>
					{{-- <li>
						<a href="chat.html" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-chat3"></span><span class="mtext">Chat</span>
						</a>
					</li> --}}
					{{-- <li>
						<a href="invoice.html" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-invoice"></span><span class="mtext">Invoice</span>
						</a>
					</li> --}}
					{{-- <li>
						<div class="dropdown-divider"></div>
					</li> --}}
					{{-- <li>
						<div class="sidebar-small-cap">Extra</div>
					</li> --}}
					{{-- <li>
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-edit-2"></span><span class="mtext">Documentation</span>
						</a>
						<ul class="submenu">
							<li><a href="introduction.html">Introduction</a></li>
							<li><a href="getting-started.html">Getting Started</a></li>
							<li><a href="color-settings.html">Color Settings</a></li>
							<li><a href="third-party-plugins.html">Third Party Plugins</a></li>
						</ul>
					</li> --}}
					<li>
						<a href="https://dropways.github.io/deskapp-free-single-page-website-template/" target="_blank"
							class="dropdown-toggle no-arrow">
							<span class="micon dw dw-settings2"></span>
							<span class="mtext"> Paramètres<img src="vendors/images/coming-soon.png" alt=""
									width="25"></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			@yield('content')



			<div class="footer-wrap pd-20 mb-20 card-box">
				APAID - Tablaeu de bord <a href="https://apaid-benin.org" target="_blank">APAID</a>
			</div>
		</div>
	</div>
	<!-- js -->
	<script src="{{ asset('deskapp/vendors/scripts/core.js')}}"></script>
	<script src="{{ asset('deskapp/vendors/scripts/script.min.js')}}"></script>
	<script src="{{ asset('deskapp/vendors/scripts/process.js')}}"></script>
	<script src="{{ asset('deskapp/vendors/scripts/layout-settings.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/apexcharts/apexcharts.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/responsive.bootstrap4.min.js')}}"></script>
	<script src="{{ asset('deskapp/vendors/scripts/dashboard.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
	<!-- switchery js -->
	<script src="{{ asset('deskapp/src/plugins/switchery/switchery.min.js')}}"></script>
	<!-- bootstrap-tagsinput js -->
	<script src="{{ asset('deskapp/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
	<!-- bootstrap-touchspin js -->
	<script src="{{ asset('deskapp/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js')}}"></script>
	<script src="{{ asset('deskapp/vendors/scripts/advanced-components.js')}}"></script>
	<!-- add sweet alert js & css in footer -->
	<script src="{{ asset('deskapp/src/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
	<script src="{{ asset('deskapp/vendors/scripts/steps-setting.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/jquery-steps/jquery.steps.js')}}"></script>

	<!-- buttons for Export datatable -->
	<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.buttons.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/buttons.bootstrap4.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/buttons.print.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/buttons.html5.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/buttons.flash.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/pdfmake.min.js')}}"></script>
	<script src="{{ asset('deskapp/src/plugins/datatables/js/vfs_fonts.js')}}"></script>
	<!-- Datatable Setting js -->
	<script src="{{ asset('deskapp/vendors/scripts/datatable-setting.js')}}"></script>
	<script>
		$(document).ready(function() {
		$('.summernote').summernote();
		});

	</script>
	<script>
		$(() => {
            $('input[type="file"]').on('change', (e) => {
                let that = e.currentTarget
                if (that.files && that.files[0]) {
                    $(that).next('.custom-file-label').html(that.files[0].name)
                    let reader = new FileReader()
                    reader.onload = (e) => {
                        $('#preview').attr('src', e.target.result)
                    }
                    reader.readAsDataURL(that.files[0])
                }
            })
        })
	</script>
</body>

</html>