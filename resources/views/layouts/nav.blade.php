<!DOCTYPE html>
<html lang="en">
  <head>
    <title>APAID- LA SOLUTION ADEQUAT</title>
        <!-- Start of Async Drift Code -->
    <script>
    "use strict";

    !function() {
      var t = window.driftt = window.drift = window.driftt || [];
      if (!t.init) {
        if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
        t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
        t.factory = function(e) {
          return function() {
            var n = Array.prototype.slice.call(arguments);
            return n.unshift(e), t.push(n), t;
          };
        }, t.methods.forEach(function(e) {
          t[e] = t.factory(e);
        }), t.load = function(t) {
          var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
          o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
          var i = document.getElementsByTagName("script")[0];
          i.parentNode.insertBefore(o, i);
        };
      }
    }();
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('2dd5exfu2v46');
    </script>
    <!-- End of Async Drift Code -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{-- Logo  --}}
    <link rel="icon" type="image/png" href="account/images/logo 2.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    <script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset("vendor/cookie-consent/css/cookie-consent.css")}}">

    <link rel="stylesheet" href="{{asset('account/css/animate.css')}}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('deskapp/vendors/styles/icon-font.min.css')}}">

    
    <link rel="stylesheet" href="{{ asset('account/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{asset('account/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('account/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{ asset('account/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('account/css/style.css')}}">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('deskapp/vendors/styles/style.css')}}"> --}}
    @stack('css')
    {{-- <style>
      
      .user-info-dropdown{padding:5px 10px 5px 0}
      .user-info-dropdown .dropdown-toggle{
        display:block;
        padding:5px 0;
        font-size:14px}
      .user-info-dropdown .dropdown-toggle .user-icon{
        width:42px;height:42px;font-size:22px;background:#ebeff3;
        color:#1b00ff;line-height:52px;
        text-align:center;display:inline-block;vertical-align:middle;
        border-radius:100%;-webkit-box-shadow:0 0 10px rgba(0,0,0,.18);
        box-shadow:0 0 10px rgba(0,0,0,.18)}
      .user-info-dropdown .dropdown-toggle .user-name{font-weight:500;display:inline-block;vertical-align:middle;
        margin-left:5px;color:#131e22;font-family:'Inter',sans-serif}
      .user-info-dropdown .dropdown-menu{min-width:185px}
      .user-icon{width:52px;height:52px;font-size:20px;
        background:#ebeff3;
        color:#1b00ff;
        line-height:52px;
        text-align:center;
        display:inline-block;
        vertical-align:middle;
        border-radius:100%;
        -webkit-box-shadow:0 0 10px rgba(0,0,0,.18);
        box-shadow:0 0 10px rgba(0,0,0,.18)
      }
      .user-info-dropdown .dropdown-toggle .user-icon img{border-radius:100%}
      img{border:0;vertical-align:top;max-width:100%;height:auto}
      /* .left-section.wrap-logo-top{
      width: 30%;
      order: 1;
      text-align: center;
      margin-top: 25px;
      margin-bottom: 25px;
      } */
  
    </style> --}}
  </head>
  <body>

    <div class="wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="bg-wrap">
							<div class="row">
								<div class="col-md-6 d-flex align-items-center">
									<p class="mb-0 phone pl-md-2">
										<a href="tel://22962201028" class="mr-2"><span class="fa fa-phone mr-1"></span> +229 546 527 32</a> 
										<a href="mailto:contact@apaid-benin.org"><span class="fa fa-paper-plane mr-1"></span> contact@apaid-benin.org</a>
									</p>
								</div>
								<div class="col-md-6 d-flex justify-content-md-end">
									<div class="social-media">
						    		<p class="mb-0 d-flex">
						    			<a href="https://www.facebook.com/Lancement-APAID-Benin-102433472277968" class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"><i class="sr-only">Facebook</i></span></a>
						    			<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"><i class="sr-only">Twitter</i></span></a>
						    			<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"><i class="sr-only">Instagram</i></span></a>
						    			<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-dribbble"><i class="sr-only">Dribbble</i></span></a>
						    		</p>
					        </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
        <div class="wrap-logo-top left-section">
          <a href="{{ route('/') }}" class="link-to-home"><img src="{{ asset('account/images/logo 2.png') }}" alt="mercado"
              style="width: 3rem; height: 3rem; margin-right: 1rem; margin-left: 0rem;"></a>
        </div>
        {{-- <img src="account/images/logo 2.jpg"> --}}
	    	<a class="navbar-brand" href="{{ route('/') }}">APAID</a>
          <div class="form-group d-flex order-sm-start order-lg-last">
            @guest
              @if (Route::has('login'))
                <a href="{{ route('login') }}" class="btn btn-dark">Connexion</a>
              @endif
              @else
              <div class="user-info-dropdown">
                <div class=" dropdown">
                  <a href="#" class="dropdown-toggle"  role="button" data-toggle="dropdown" aria-expanded="false">
                    <span class="user-icon">
                      <img src="https://ui-avatars.com/api/?size=50&background=random&name={{ Auth::User()->name }}" alt="">
                    </span>
                    {{-- <span>{{Auth::user()->name}}</span> --}}
                </a>
                  <div class="dropdown-menu">
                      <a href="{{ route('profile') }}" class="dropdown-item"><i class="dw dw-user1"></i> Profile</a>
                      <a href="{{ route('services') }}" class="dropdown-item"><i class="dw dw-settings2"></i> Paramètres</a>
                      <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="dw dw-logout"></i>    {{ __('Logout') }}
                        </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none"> @csrf</form>
                  </div>
                </div>
              </div>
            @endguest  
          </div>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="fa fa-bars"></span> Menu
	      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav m-auto">
	        	<li class="{{ Request::is('/') ? 'active' : '' }}  nav-item "><a href="{{ route('/') }}" class="nav-link">Accueil</a></li>
	        	<li class=" {{ Request::is('blog.index') ? 'active' : '' }} nav-item"><a href="{{ route('blog.index') }}" class="nav-link">Blog</a></li>
            <li class="nav-item dropdown">
              <ul class="navbar-nav m-auto">
                <li class="nav-item"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Innovation</a></li>
                <div class="dropdown-menu">
                  <li class="nav-item"><a href="#"
                      class="dropdown-item">Presentation</a></li>
                  <li class="nav-item "><a href="{{ route('accueil-shop') }}"
                      class="dropdown-item dropdown">Boutique</a></li>
                </div>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <ul class="navbar-nav m-auto">
                <li class="nav-item"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Partenaires</a></li>
                <div class="dropdown-menu"> 
                  <li class="{{ Request::is('ong') ? 'active' : '' }} nav-item"><a href="{{ route('ong') }}" class="dropdown-item">ONG/OPA</a></li>
                  <li class="{{ Request::is('organisme') ? 'active' : '' }} nav-item "><a href="{{ route('organisme') }}" class="dropdown-item dropdown">Autres structures</a></li>
                  <li class="{{ Request::is('adhesion') ? 'active' : '' }} nav-item"><a href="{{ route('adhesion') }}"
                      class="dropdown-item">Devenir membre</a></li>
                </div>
              </ul>
            </li>    
	          <div class="nav-item dropdown">
              <a href="#" class="{{ Request::is('projet-vue'|| 'services' || 'carrefour.index') ? 'active' : '' }} nav-link dropdown-toggle" data-toggle="dropdown">Offres</a>
              <div class="dropdown-menu">
                  <a href="{{ route('projet-vue') }}" class="dropdown-item">Projets </a>
                  <a href="{{ route('services') }}" class="dropdown-item">Services </a>
                  <a href="{{ route('carrefour.index') }}" class="dropdown-item">Carrefour </a>
              </div>
            </div>       
	          <li class="{{ Request::is('contact') ? 'active' : '' }} nav-item"><a href="{{ route('contact') }}" class="nav-link">Contact</a></li>
	          <li class="{{ Request::is('about') ? 'active' : '' }} nav-item"><a href="{{ route('about') }}" class="nav-link">A propos</a></li>
            
	        </ul>
	      </div>
	    </div>
	  </nav>
    {{-- Message de notification --}}
{!! Toastr::message() !!}


      <main class="py-4">
        @yield('content')
    </main>
    {{-- {!! EuCookieConsent::getUpdatePopup() !!} --}}
    {{-- {!! EuCookieConsent::getPopup() !!} --}}
  <a href="javascript:void(0)" class="js-lcc-settings-toggle">@lang('cookie-consent::texts.alert_settings')</a>
    
      <footer class="footer">
        <div class="container-fluid px-lg-5">
            <div class="row">
                <div class="col-md-9 py-5">
                    <div class="row">
                        <div class="col-md-4 mb-md-0 mb-4">
                            <h2 class="footer-heading">A propos de nous</h2>
                            <p style="color: rgb(136, 131, 131);">APAID  est une plateforme entrepreneuriale, un espace de vulgarisation porteur de solutions techniques, organisationnelles et morales des innovations vertes initiées par les organismes membres ou partenaires.</p>
                            <ul class="ftco-footer-social p-0">
                  <li class="ftco-animate"><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><span class="fa fa-twitter"></span></a></li>
                  <li class="ftco-animate"><a href="https://www.facebook.com/Lancement-APAID-Benin-102433472277968" data-toggle="tooltip" data-placement="top" title="Facebook"><span class="fa fa-facebook"></span></a></li>
                  <li class="ftco-animate"><a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><span class="fa fa-instagram"></span></a></li>
                </ul>
                        </div>
                        <div class="col-md-8">
                            <div class="row justify-content-center">
                                <div class="col-md-12 col-lg-10">
                                    <div class="row">
                                        <div class="col-md-4 mb-md-0 mb-4">
                                            <h2 class="footer-heading">Activites</h2>
                                            <ul class="list-unstyled">
                                  <li><a href="#" class="py-1 d-block">Services</a></li>
                                  <li><a href="#" class="py-1 d-block">Carrefour</a></li>
                                  <li><a href="#" class="py-1 d-block">Projets</a></li>
                                  <li><a href="#" class="py-1 d-block">Seminaires & Formation</a></li>
                                </ul>
                                        </div>
                                        <div class="col-md-4 mb-md-0 mb-4">
                                            <h2 class="footer-heading">Pages</h2>
                                            <ul class="list-unstyled">
                                  <li><a href="{{ route('about') }}" class="py-1 d-block">A propos de nous</a></li>
                                  <li><a href="{{ route('contact') }}" class="py-1 d-block">Contactez-nous</a></li>
                                  <li><a href="{{ route('cgu') }}" class="py-1 d-block">Termes et Conditions</a></li>
                                  <li><a href="#" class="py-1 d-block">Stratégies</a></li>
                                </ul>
                                        </div>
                                        {{-- <div class="col-md-4 mb-md-0 mb-4">
                                            <h2 class="footer-heading">Resources</h2>
                                            <ul class="list-unstyled">
                                  <li><a href="#" class="py-1 d-block">Security</a></li>
                                  <li><a href="#" class="py-1 d-block">Global</a></li>
                                  <li><a href="#" class="py-1 d-block">Charts</a></li>
                                  <li><a href="#" class="py-1 d-block">Privacy</a></li>
                                </ul>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-md-5">
                        <div class="col-md-12">
                            <center><p class="copyright" style="color: rgb(136, 131, 131);"><!-- Link back to Colorlib can\'t be removed. Template is licensed under CC BY 3.0. -->
                  Copyright &copy;<script>document.write(new Date().getFullYear());</script> Tous droits réservés <i class="fa fa-heart" aria-hidden="true"></i>
                  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></center>
                  </div>
                </div>
              </div>
              {{-- <div class="col-md-3 py-md-5 py-4 aside-stretch-right pl-lg-5">
                <h2 class="footer-heading">Free consultation</h2>
                <form action="#" class="form-consultation">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Your Name">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Your Email">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Subject">
                  </div>
                  <div class="form-group">
                    <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Message"></textarea>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="form-control submit px-3">Send A Message</button>
                  </div>
                </form>
              </div> --}}
              {{-- <div class="col-md-3 py-md-5 py-4 aside-stretch-right pl-lg-5">
                <h2 class="footer-heading">Contactez-nous</h2>
                <form action="#" class="form-consultation" method="POST" enctype="multipart/form-data">
                  <div class="form-group">
                    <input type="text" name="nom" class="form-control" placeholder="Votre nom">
                  </div>
                  <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="Votre Email">
                  </div>
                  <div class="form-group">
                    <input type="text" name="subject" class="form-control" placeholder="Sujet">
                  </div>
                  <div class="form-group">
                    <textarea name="message" id=""  cols="30" rows="3" class="form-control" placeholder="Message"></textarea>
                  </div>
                  <div class="form-group">
                      <button type="submit" class="form-control submit px-3">Envoyer un message</button>
                  </div>
                </form>
              </div> --}}
            </div>
          </div>
        </div>
      </footer>



<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

<script src="{{asset('account/js/jquery.min.js')}}"></script>
<script src="{{ asset('account/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{asset('account/js/popper.min.js')}}"></script>
<script src="{{asset('account/js/bootstrap.min.js')}}"></script>
<script src="{{asset('account/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('account/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('account/js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('account/js/jquery.animateNumber.min.js')}}"></script>
<script src="{{asset('account/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('account/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('account/js/scrollax.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{asset('account/js/google-map.js')}}"></script>
<script src="{{asset('account/js/main.js')}}"></script>
<script src="https://cdn.kkiapay.me/k.js"></script>
{{-- <script amount="6000" 
  callback="httpss://apaid-benin.org"
  data=""
  url="<url-vers-votre-logo>"
  position="right" 
  theme="#0095ff"
  sandbox="true"
  key="d6691750e5a811eb96cbffe4cc632e8e"
  src="https://cdn.kkiapay.me/k.js"></script> --}}
  @stack('js')
  <script>
    $(function(){
      if(v.mode=="viewer"){
        $("#cookie").hide()
      tb_show_viewer(null, "#TB_inline?
      inlineId=cookie")
      }
    })
  </script>
</body>
</html>