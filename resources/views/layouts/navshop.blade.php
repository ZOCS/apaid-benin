<!doctype html>
<html lang="fr">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Title Tag  -->
    <title>EAgro - La boutique en agronomie</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="eshop/images/favicon.png">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<!-- StyleSheet -->
	<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

	<script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.js"></script>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{ asset('eshop/css/bootstrap.css') }}">
	{{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> --}}
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('eshop/css/magnific-popup.min.css') }}">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('eshop/css/font-awesome.css') }}">
	<!-- Fancybox -->
	<link rel="stylesheet" href="{{ asset('eshop/css/jquery.fancybox.min.css') }}">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="{{ asset('eshop/css/themify-icons.css') }}">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{ asset('eshop/css/niceselect.css') }}">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('eshop/css/animate.css') }}">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="{{ asset('eshop/css/flex-slider.min.css') }}">
	<!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('eshop/css/owl-carousel.css') }}">
	<!-- Slicknav -->
    <link rel="stylesheet" href="{{ asset('eshop/css/slicknav.min.css') }}">
	
	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="{{ asset('eshop/css/reset.css') }}">
	<link rel="stylesheet" href="{{ asset('eshop/css/style.css') }}">
    <link rel="stylesheet" href="{{asset('eshop/css/responsive.css')}}">
	<script src="{{ asset('deskapp/vendors/scripts/layout-settings.js')}}"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/15.5.1/nouislider.min.css" integrity="sha512-qveKnGrvOChbSzAdtSs8p69eoLegyh+1hwOMbmpCViIwj7rn4oJjdmMvWOuyQlTOZgTlZA0N2PXA7iA8/2TUYA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	@stack('css')
	@livewireStyles
	
	
</head>
<body class="js" id="app">
	
	<!-- Preloader -->
	{{-- <div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div> --}}
	<!-- End Preloader -->
	
	
	<!-- Header -->
	<header class="header shop">
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-12 col-12">
						<!-- Top Left -->
						<div class="top-left">
							<ul class="list-main">
								<li><i class="ti-headphone-alt"></i> +229 546 527 32</li>
								<li><i class="ti-email"></i> contact@apaid-benin.org</li>
							</ul>
						</div>
						<!--/ End Top Left -->
					</div>
					<div class="col-lg-7 col-md-12 col-12">
						<!-- Top Right -->
						<div class="right-content">
							<ul class="list-main">
								<li><i class="ti-location-pin"></i> Store location</li>
								<li><a href="#"><img src="{{ asset('shop/img/language.png') }}" alt="">English</a>
                                    <ul class="sub-category">
                                        <li><a href="#">Spanis</a></li>
                                        <li><a href="#">English</a></li>
                                    </ul>
                                </li>
								@guest
								<li><a class="ti-power-off"></a><a href="{{ route('login') }}" rel="content-y" data-toggle="modal" data-target="#bd-example-modal-lg" type="button" role="button">Login</a></li>
								@else
								<li><i class="ti-user"></i> <a href="#">Mon Compte</a></li>
								<li><i class="ti-logout"></i> <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Se deconnecter</a><form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none"> @csrf</form></li>
								@endguest
							</ul>
						</div>
						<!-- End Top Right -->
					</div>
				</div>
			</div>
		</div>
		{!! Toastr::message() !!}
		
		@if(session('error'))
			<div class="alert alert-danger">
				{{session('error')}}
			</div>
		@endif

		@if(count($errors) > 0)
			<div class="row">
				<div class="col-lg-3 alert alert-danger pull-right">
					<ul class="mb-0 mt-0">
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			</div>
		@endif
		<!-- End Topbar -->
		<div class="middle-inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-12">
						<!-- Logo -->
						<div class="logo">
							<a href="index.html"><img src="{{ asset('eshop/images/photo.png') }}" alt="logo" style="width: 5rem; height: 5rem"></a>
						</div>
						<!--/ End Logo -->
						<!-- Search Form -->
						<div class="search-top">
							<div class="top-search"><a href="#0"><i class="ti-search"></i></a></div>
							<!-- Search Form -->
							<div class="search-top">
								<form class="search-form" action="{{ route('produit.search')}}">
									<input type="text" placeholder="Search here..." name="q">
									<button  type="submit"><i class="ti-search"></i></button>
								</form>
							</div>
							<!--/ End Search Form -->
						</div>
						<!--/ End Search Form -->
						<div class="mobile-nav"></div>
					</div>
					<div class="col-lg-8 col-md-7 col-12">
						<div class="search-bar-top">
							<div class="search-bar">
								<select>
									<option selected="selected">Toutes Categories</option>
									<option>watch</option>
									<option>mobile</option>
									<option>kid’s item</option>
								</select>
								<form action="{{ route('produit.search')}}">
									<input name="search" placeholder="Rechercher vos produits ici....." type="search" value="{{ request()->search ?? ''}}">
									<button class="btnn" type="submit"><i class="ti-search"></i></button>
								</form>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-12">
						<div class="right-bar">
							<!-- Search Form -->
							<div class="sinlge-bar">
								<a href="{{ route('whislist') }}" class="single-icon"><i class="fa fa-heart-o" aria-hidden="true"></i><span class="total-countwish">{{ Cart::instance('wishlist')->count() }}</span></a>
							</div>
							<div class="sinlge-bar">
								<a href="#" class="single-icon"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
							</div>
							<div class="sinlge-bar shopping">
								<a href="#" class="single-icon"><i class="ti-bag"></i> <span class="total-count">{{ Cart::instance('cart')->count() }}</span></a>
								<!-- Shopping Item -->
								<div class="shopping-item">
									<div class="dropdown-cart-header">
										<span>{{ Cart::instance('cart')->count() }} Items</span>
										<a href="{{ route('cart.index') }}">Voir Panier</a>
									</div>
									<ul class="shopping-list">
										@foreach(Cart::instance('cart')->content() as $produit)
										<li>
											<form action="{{route('cart.destroy', $produit->rowId)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                {{-- <button class="btn btn-danger" type="submit"><i class="ti-trash remove-icon"></i></button> --}}
												<button type="submit" class="remove" title="Remove this item"><i class="fa fa-remove"></i></button>
                                            </form>
											{{-- <a href="#" class="remove" title="Remove this item"><i class="fa fa-remove"></i></a> --}}
											<a class="cart-img" href="#"><img src="{{ asset('storage/' . $produit->model->image) }}" alt="#" style="width: 70px; height: 70px;"></a>
											<h4><a href="#">{{  $produit->model->nom }}</a></h4>
											<p class="quantity">{{ $produit->qty }}x - <span class="amount">{{  $produit->model->prix }} Fcfa</span></p>
										</li>
										@endforeach
									</ul>
									<div class="bottom">
										<div class="total">
											<span>Total</span>
											<span class="total-amount">{{Cart::subtotal()}}</span>
										</div>
										<a href="{{ route('checkout.index') }}" class="btn animate">Caisse</a>
									</div>
								</div>
								<!--/ End Shopping Item -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header Inner -->
		<div class="header-inner">
			<div class="container">
				<div class="cat-nav-head">
					<div class="row">
						<div class="col-lg-3">
							
							<div class="all-category">
								<h3 class="cat-heading"><i class="fa fa-bars" aria-hidden="false"></i>CATEGORIES</h3>
								<ul class="{{ Request::is('checkout.index') ? '' : 'active' }} main-category">
                                    @foreach($categ as $categorie)
									<li><a href="{{ route('boutique.index', ['categorie' => $categorie->slug ]) }}">{{$categorie->nom}} <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        @php
                                            $children = App\Models\Categorie::where('parent_id', $categorie->id)->get();
                                        @endphp
                                        @if($children->isNotEmpty())
											<ul class="sub-category">
												@foreach($children as $child)
													<li><a href="{{ route('boutique.index', ['categorie' => $child->slug ]) }}">{{$child->nom}}</a>
														@php
															$grandChild = App\Models\Categorie::where('parent_id', $child->id)->get();
														@endphp
														@if($grandChild->isNotEmpty())
															<ul>
																@foreach($grandChild as $c)
																<li><a href="{{ route('boutique.index', ['categorie' => $c->slug ]) }}">{{$c->nom}}</a></li>
																@endforeach
															</ul>
														@endif
													</li>
												@endforeach
											</ul>
                                        @endif
									</li>
                                    @endforeach
								</ul>
							</div>
						</div>
						<div class="col-lg-9 col-12">
							<div class="menu-area">
								<!-- Main Menu -->
								<nav class="navbar navbar-expand-lg">
									<div class="navbar-collapse">	
										<div class="nav-inner">	
											<ul class="nav main-menu menu navbar-nav">
													<li class="{{ Request::is('accueil-shop') ? 'active' : '' }}"><a href="{{route('accueil-shop')}}">Accueil</a></li>
													<li class="{{ Request::is('boutique') ? 'active' : '' }}"><a href="{{route('boutique.index')}}">Boutique</a></li>
													{{-- <li class="active: isActive('accueil')"><a href="#">Document<i class="ti-angle-down"></i><span class="new"></span></a>
														<ul class="dropdown">
															<li><a href="cart.html">Cart</a></li>
															<li><a href="checkout.html">Checkout</a></li>
														</ul>
													</li>									 --}}
													{{-- <li class="{{ Request::is('blog.index') ? 'active' : '' }}"><a href="{{ route('blog.index') }}">Blog</a></li>
													<li class="{{ Request::is('contact') ? 'active' : '' }}"><a href="{{ route('contact') }}">Contact </a></li> --}}
												</ul>
										</div>
									</div>
								</nav>
								<!--/ End Main Menu -->	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ End Header Inner -->
	</header>
	<!--/ End Header -->

    <main class="py-4" id="app">
        @yield('content')
    </main>

    <!-- Start Footer Area -->
	<footer class="footer">
		<!-- Footer Top -->
		<div class="footer-top section">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
							<div class="logo">
								<a href="index.html"><img src="{{ asset('eshop/images/logo2.png') }}" alt="#"></a>
							</div>
							<p class="text">Praesent dapibus, neque id cursus ucibus, tortor neque egestas augue,  magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
							<p class="call">Got Question? Call us 24/7<span><a href="tel:229 546 527 32">+229 546 527 32</a></span></p>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-2 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Information</h4>
							<ul>
								<li><a href="#">Apropos</a></li>
								<li><a href="#">Faq</a></li>
								<li><a href="#">Termes & Conditions</a></li>
								<li><a href="#">Contatez-nous</a></li>
								<li><a href="#">Aide</a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-2 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Nos Services</h4>
							<ul>
								<li><a href="#">Livraison</a></li>
								<li><a href="#">Methodes de paiement</a></li>
								<li><a href="#">Returns</a></li>
								<li><a href="#">Shipping</a></li>
								<li><a href="#">Privacy Policy</a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer social">
							<h4>Get In Tuch</h4>
							<!-- Single Widget -->
							<div class="contact">
								<ul>
									<li>NO. 342 - London Oxford Street.</li>
									<li>012 Republique du Benin.</li>
									<li>contact@apaid-benin.org</li>
									<li>+229 546 527 32</li>
								</ul>
							</div>
							<!-- End Single Widget -->
							<ul>
								<li><a href="#"><i class="ti-facebook"></i></a></li>
								<li><a href="#"><i class="ti-twitter"></i></a></li>
								<li><a href="#"><i class="ti-flickr"></i></a></li>
								<li><a href="#"><i class="ti-instagram"></i></a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Top -->
		<div class="copyright">
			<div class="container">
				<div class="inner">
					<div class="row">
						<div class="col-lg-6 col-12">
							<div class="left">
								<p>Copyright © 2020 <a href="http://www.apaid-benin.org" target="_blank">APAID BENIN</a>  -  Tous les droits sont réservés.</p>
							</div>
						</div>
						<div class="col-lg-6 col-12">
							<div class="right">
								<img src="{{ asset('eshop/images/payments.png') }}" alt="#">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>


	

	{{-- //**======================================= Toggle   login================================================*/ --}}
	<div class="modal fade bs-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" 			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog">
					{{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button> --}}
			</div>
				<div class="modal-body h-200">
					<div class="container py-5 h-100">
				<div class="row d-flex justify-content-center align-items-center h-100">
				<div class="col col-xl-10">
					<div class="card" style="border-radius: 1rem;">
					<div class="row g-0">
						<div class="col-md-6 col-lg-5 d-none d-md-block">
							<img
								src="https://mdbootstrap.com/img/Photos/new-templates/bootstrap-login-form/img1.jpg"
								alt="login form"
								class="img-fluid" style="border-radius: 1rem 0 0 1rem;"
							/>
						</div>
							<div class="col-md-6 col-lg-7 d-flex align-items-center">
								<div class="card-body p-4 p-lg-5 text-black">

									<form method="POST" action="{{ route('login') }}">
											@csrf
										<div class="d-flex align-items-center mb-3 pb-1">
											<i class="fas fa-cubes fa-2x me-3" style="color: #ff6219;"></i>
											<span class="h1 fw-bold mb-0">Logo</span>
										</div>

										<h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;">Sign into your account</h5>

										<div class="form-outline mb-4">
											<input type="email" name="email" id="form2Example17" class="form-control form-control-lg @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
											<label class="form-label" for="form2Example17">Email address</label>
											@error('email')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
										</div>

										<div class="form-outline mb-4">
											<input type="password" name="password" id="form2Example" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="**********" name="password" required autocomplete="current-password" />
											<label class="form-label" for="form2Example">Password</label>
											@error('password')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
										</div>

										<div class="pt-1 mb-4">
											<button class="btn btn-dark btn-lg btn-block" type="submit">Se connecter</button>
										</div>
									</form>
									<a class="small text-muted" href="{{ route('password.request') }}">Forgot password?</a>
									<p class="mb-5 pb-lg-2" style="color: #393f81;">Don\'t have an account? <a href="#!" style="color: #393f81;">Register here</a></p>
									<a href="#!" class="small text-muted">Terms of use.</a>
									<a href="#!" class="small text-muted">Privacy policy</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- //**======================================= Fin Toggle Login ============================*/ --}}



	<!-- /End Footer Area -->
	<script  src="{{ asset('js/cart.js') }}"></script>
	<script  src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('js/checkout.js') }}"></script>
	<script  src="{{ asset('js/filter.js') }}"></script>
	
	<!-- Jquery -->
    {{-- <script src="{{ asset('eshop/js/jquery.min.js') }}"></script>
    <script src="{{ asset('eshop/js/jquery-migrate-3.0.0.js') }}"></script>
	<script src="{{ asset('eshop/js/jquery-ui.min.js') }}"></script> --}}
	
	<!-- Popper JS -->
	<script src="{{ asset('eshop/js/popper.min.js') }}"></script>
	<!-- Bootstrap JS -->
	<script src="{{ asset('eshop/js/bootstrap.min.js') }}"></script>
	<!-- Color JS -->
	<script src="{{ asset('eshop/js/colors.js') }}"></script>
	<!-- Slicknav JS -->
	<script src="{{ asset('eshop/js/slicknav.min.js') }}"></script>
	<!-- Owl Carousel JS -->
	<script src="{{ asset('eshop/js/owl-carousel.js') }}"></script>
	<!-- Magnific Popup JS -->
	<script src="{{ asset('eshop/js/magnific-popup.js') }}"></script>
	<!-- Waypoints JS -->
	<script src="{{ asset('eshop/js/waypoints.min.js') }}"></script>
	<!-- Countdown JS -->
	<script src="{{ asset('eshop/js/finalcountdown.min.js') }}"></script>
	<!-- Nice Select JS -->
	{{-- <script src="{{ asset('eshop/js/nicesellect.js') }}"></script> --}}
	<!-- Flex Slider JS -->
	<script src="{{ asset('eshop/js/flex-slider.js') }}"></script>
	<!-- ScrollUp JS -->
	<script src="{{ asset('eshop/js/scrollup.js') }}"></script>
	<!-- Onepage Nav JS -->
	<script src="{{ asset('eshop/js/onepage-nav.min.js') }}"></script>
	<!-- Easing JS -->
	<script src="{{ asset('eshop/js/easing.js') }}"></script>
	<!-- Active JS -->
	<script src="{{ asset('eshop/js/active.js') }}"></script>
	
	<script src="https://cdn.kkiapay.me/k.js"></script>
	<script amount={{ strval(Cart::total()) }}
        callback="{{ route('payement') }}"
        data=""
        url="{{ asset('eshop/images/logo2.png') }}"
        position="right" 
        theme="#016B10"
        sandbox="true"
        key="ea65eca024de11ebb45c4706c718fb6d"
        src="https://cdn.kkiapay.me/k.js"></script>
	@stack('js')
</body>
</html>