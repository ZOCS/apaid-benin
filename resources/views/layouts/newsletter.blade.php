<section class="ftco-section ftco-no-pb ftco-no-pt bg-secondary">
    <div class="container py-5">
        <div class="row">
            <div class="col-md-7 d-flex align-items-center">
                <h2 class="mb-3 mb-sm-0" style="color:black; font-size: 22px;">Inscrivez-vous pour suivre les infos</h2>
            </div>
            <div class="col-md-5 d-flex align-items-center">
                <form action="{{ route('newsletter_email') }}" method="POST" class="subscribe-form">
                    <div class="form-group d-flex">
                        <input type="email" class="form-control" name="user_email"
                            placeholder="Entrer l'adresse e-mail">
                        {{ csrf_field() }}
                        <input type="submit" value="S'abonner" class="submit px-3">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>