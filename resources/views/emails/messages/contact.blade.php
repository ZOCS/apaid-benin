@component('mail::message')


@component('mail::panel')
**Email:** {{$details['email']}}

**Sujet:** {{$details['subject']}}

Bonjour  APAID Benin

 
{{$details['message']}}


Cordialement,
{{$details['name']}}

@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
