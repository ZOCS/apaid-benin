@component('mail::message')
# ADHESION

{{ $data['responsable'] }}
{{ $data['ville']   }}-{{ $data['pays'] }}

{{ $data['nom'] }}<br>
À l'attention de [Madame/Monsieur] [nom du dirigeant]
{{ $data['email'] }} {{ $data['phone'] }}

{{ $data['ville'] }}, le {{ Carbon\Carbon::now()->format('d-m-Y') }}

Objet : Demande d'adhésion à l'association [nom de l'association]

Madame Directrice,
Par la présente, je vous signifie ma volonté d'adhérer à votre association APAID BENIN.
En effet, {{ $data['objectifs'] }} - {{ $data['realisation'] }}.
Je vous prie donc de bien vouloir considérer ma demande.

Si l'adhésion à l'association est réservée à des personnes qui remplissent certaines conditions :
Vous trouverez joint la copie de document qui prouve notre existence de facon physique.

Dans l'attente de votre réponse, veuillez agréer, Madame la Directrice, l'expression de mes sentiments
respectueux.

{{ $data['responsable'] }}

[Signature]


Thanks,<br>
{{ config('app.name') }}
@endcomponent
