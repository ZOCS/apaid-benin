@component('mail::message')


@component('mail::panel')
**Email:** {{$details['email']}}


**Nom et Prénom(s):** {{$details['name']}}

**Service:**  {{ $details['service'] }}

Bonjour APAID Benin


{{$details['message']}}


Cordialement,
{{$details['name']}}

@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent