@component('mail::message')
<img src="{{ asset('account/images/logo 2.png') }}"
    style="text-align: left; margin-right:20%; width:18%; height:18%;" align="left">
<table style="margin-bottom: 15%;">
    <thead style="background-color: #0abe13; color: white;">
        <tr>
            <th colspan="12"
                style="color:#ffffff;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left; width:100%; background-color:#0abe13"
                align="left">Nouvelle commande n°8888</th>
        </tr>
    </thead>
</table>

Vous avez reçu la commande suivante de {{ $order->lname }} {{ $order->fname }} :

<h2>[ Commande n°8888] ({{ Carbon\Carbon::now()->format('d-m-Y') }})</h2>

<div style="margin-bottom:40px">
    <table>
        <thead style="background-color: #0abe13; color: white;">
            <tr>
                <th scope="col"
                    style="color:#ffffff;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"
                    align="left">Produits</th>
                <th scope="col"
                    style="color:#ffffff;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"
                    align="left">Quantite</th>
                <th scope="col"
                    style="color:#ffffff;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"
                    align="left">Prix</th>
            </tr>
        </thead>
        <tbody>
            @foreach(unserialize($order->produits) as $product)
            <tr>
                <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word"
                    align="left">{{ $product['name'] }}</td>
                <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word"
                    align="left">{{ $product['quantity'] }}</td>
                <td style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word"
                    align="left">{{ $product['price'] }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th scope="row" colspan="2"
                    style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"
                    align="left">Sous-total</th>
                <td style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left;border-top-width:4px"
                    align="left"><span>{{ $order->prix }} <span>CFA</span></span></td>
            </tr>
            <tr>
                <th scope="row" colspan="2"
                    style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"
                    align="left">Moyen de paiement</th>
                <td style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left;border-top-width:4px"
                    align="left">Par Momo/ Carte bancaire</td>
            </tr>
            <tr>
                <th scope="row" colspan="2"
                    style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"
                    align="left">Total</th>
                <td style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left;border-top-width:4px"
                    align="left"><span>{{ $order->prix }}<span>CFA</span></span></td>
            </tr>
        </tfoot>
    </table>
</div>


<table cellspacing="0" cellpadding="0" style="border:0; width:100%;vertical-align:top;margin-bottom:10px;padding:0"
    width="100%">
    <tbody>
        <tr>
            <td valign="top" width="50%"
                style="text-align:left;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;border:0;padding:0"
                align="left">
                <h2
                    style="color:#367ee9;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">
                    Adresse de facturation</h2>
            </td>
        </tr>
    </tbody>
</table>
<table cellspacing="0" cellpadding="0" style="border:0; width:100%;vertical-align:top;margin-bottom:40px;padding:0"
    width="100%">
    <tbody>
        <tr>
            <td style="padding:12px;color:#636363;border:1px solid #e5e5e5">
                {{ $order->fname }} {{ $order->lname }}<br>
                {{ $order->email }}<br>
                {{ $order->adresse }}<br>
                {{ $order->ville }}<br>
                {{ $order->pays }}<br>
                {{ $order->zip }}<br>
                <a href="tel:{{ $order->phone }}" style="color:#0abe13;font-weight:normal;text-decoration:underline"
                    target="_blank">{{ $order->phone }}</a>
                <br><a href="mailto:{{ $order->email }}" target="_blank">{{ $order->email }}</a>
            </td>
        </tr>
    </tbody>
</table>



Merci pour votre confiance,<br>
{{ config('app.name') }}
<img src="{{ asset('account/images/logo 2.png') }}"
    style="text-align: right; margin-right:0%; margin-bottom:0%; width:9%; height:9%;" align="right">
@endcomponent