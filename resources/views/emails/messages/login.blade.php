@component('mail::message')
<img src="{{ asset('account/images/logo 2.png') }}" style="text-align: left; margin-right:20%; width:10%; height:10%;"
    align="left">
<table style="margin-bottom: 15%;">
    <thead style="background-color: #0abe13; color: white;">
        <tr>
            <th colspan="12"
                style="color:#ffffff;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left; width:100%; background-color:#0abe13"
                align="left"> APAID BENIN</th>
        </tr>
    </thead>
</table>


<div class="container">
    <h2>Salut {{ Auth::check() ? Auth::user()->name : "" }}, c'était toi ?</h2>
    <p>Nous avons remarqué une connexion récente à votre compte APAID BENIN  à partir d'un emplacement ou d'un appareil inconnu.</p>

    ********************************************
    <h4>Horodatage</h4>
    <span>{{ Carbon\Carbon::now()->locale('fr_FR')->isoFormat('Do MMMM YYYY HH:mm') }}</span>

    ********************************************
    S'il s'agissait de vous, aucune autre action n'est requise. Si vous ne reconnaissez pas cette activité, réinitialisez
    immédiatement votre mot de passe.
    La sécurité est importante pour nous chez APAID BENIN, et nous vous informerons si nous remarquons une activité inhabituelle
    sur votre compte. Nous pouvons ne pas être en mesure de reconnaître une tentative de connexion lorsque vous avez effacé
    vos cookies ou que vous êtes connecté en mode de navigation privée.
    Pour obtenir des conseils sur la façon de vous protéger en ligne, visitez le Centre de sécurité APAID BENIN. Pour ajouter une
    couche de sécurité supplémentaire, vous pouvez choisir de recevoir un SMS si nous remarquons une activité inhabituelle
    comme celle-ci à l'avenir. Accédez maintenant à vos paramètres de mot de passe et de sécurité pour configurer la
    vérification en deux étapes.

</div>



Merci,<br>
L'équipe APAID BENIN<br>
{{ config('app.name') }}
<img src="{{ asset('account/images/logo 2.png') }}"
    style="text-align: right; margin-right:0%; margin-bottom:0%; width:9%; height:9%;" align="right">
@endcomponent