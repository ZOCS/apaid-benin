@extends('layouts.nav')


@section('content')

<div class="hero-wrap">
    <div class="home-slider owl-carousel">
      <div class="slider-item" style="background-image:url(account/images/accueil_2.jpg);">
          <div class="overlay"></div>
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-center">
              <div class="col-md-8 ftco-animate">
                  <div class="text w-100 text-center">
                      <h2>Innovations vertes</h2>
                    <h1 class="mb-4">Les innovations économiquement viables et écologiquement durables</h1>
                    <p><a href="{{ ('about') }}" class="btn btn-white">En savoir +</a></p>
                </div>
              </div>
            </div>
        </div>
      </div>

      <div class="slider-item" style="background-image:url(account/images/img_12.jpg);">
          <div class="overlay"></div>
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-center">
              <div class="col-md-8 ftco-animate">
                  <div class="text w-100 text-center">
                      <h2>Nos Organismes Nationaux et Internationaux</h2>
                    <h1 class="mb-4">{{('Un creuset d\'échanges et de partage d\'innovations vertes et de réseautage.')}}</h1>
                    <p><a href="{{ ('about') }}" class="btn btn-white">Nous rejoindre</a></p>
                </div>
              </div>
            </div>
        </div>
      </div>
      
      <div class="slider-item" style="background-image:url(account/images/accueil_1.jpg);">
          <div class="overlay"></div>
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-center">
              <div class="col-md-8 ftco-animate">
                  <div class="text w-100 text-center">
                      <h2>Boutique</h2>
                    <h1 class="mb-4">L'espace de vente de nos produits issus de nos recherches.</h1>
                    <p><a href="{{ ('accueil-shop') }}" class="btn btn-white">En savoir +</a></p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
   
<section class="ftco-section ftco-no-pt bg-light">
    <div class="container">
        <div class="row d-flex no-gutters">
            <div class="col-md-6 d-flex">
                <div class="img img-video d-flex align-self-stretch align-items-center justify-content-center justify-content-md-center mb-4 mb-sm-0" style="background-image:url(account/images/about_1.jpg);" style="margin-top: 9rem;">
                </div>
            </div>
            <div class="col-md-6 pl-md-5 py-md-5">
                <div class="heading-section pl-md-4 pt-md-5">
                    <span class="subheading">Bienvenue a APAID</span>
            <h2 class="mb-4">Nos objectifs à atteindre</h2>
                </div>
                <div class="services-2 w-100 d-flex">
                    <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-wealth"></span></div>
                    <div class="text pl-4">
                        <h4>Le répertoire des organismes</h4>
                        <p>{{ __("Constituer le répertoire des organismes nationaux et internationaux intervenant dans l’agriculture durable au Bénin") }}</p>
                    </div>
                </div>
                <div class="services-2 w-100 d-flex">
                    <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-accountant"></span></div>
                    <div class="text pl-4">
                        <h4>Organismes membres</h4>
                        <p>{{ __("Rédiger, soumettre et coordonner entre organismes membres des projets et programmes
                            sur les innovations vertes") }}</p>
                    </div>
                </div>
                <div class="services-2 w-100 d-flex">
                    <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-teamwork"></span></div>
                    <div class="text pl-4">
                        <h4>Gisement de capital humain</h4>
                        <p>{{ __("Constituer un gisement de capital humain pour les promoteurs agricoles") }}</p>
                    </div>
                </div>
                <div class="services-2 w-100 d-flex">
                    <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-accounting"></span></div>
                    <div class="text pl-4">
                        <h4>innovations vertes</h4>
                        <p>Diffuser et vulgariser les innovations économiquement viables et écologiquement durables</p>
                    </div>
                </div>
        </div>
    </div>
    </div>
</section>


<section class="ftco-counter bg-light ftco-no-pt" id="section-counter">
    <div class="container">
      @php
          $inno = App\Models\Produit::count();
          $org = App\Models\Organisme::count();
          $ca = App\Models\Categorie::count();
      @endphp
            <div class="row">
      <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 text-center">
          <div class="text">
            <strong class="number" data-number="1">1</strong>
          </div>
          <div class="text">
              <span>{{ __("Années d'expérience") }}</span>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 text-center">
          <div class="text">
            <strong class="number" data-number="{{ $inno }}">{{ $inno }}</strong>
          </div>
          <div class="text">
              <span>{{ ('Nombres d\'innovations') }}</span>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 text-center">
          <div class="text">
            <strong class="number" data-number="{{ $ca }}">{{ $ca }}</strong>
          </div>
          <div class="text">
              <span>{{ ('Nombres de domaine d\'intervention') }}</span>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 text-center">
          <div class="text">
            <strong class="number" data-number="{{ $org }}">{{ $org }}</strong>
          </div>
          <div class="text">
              <span>{{ ('Nombres d\'organismes') }}</span>
          </div>
        </div>
      </div>
    </div>
    </div>
</section>

<section class="ftco-section testimony-section bg-light">
    <div class="overlay"></div>
  <div class="container">
    <div class="row justify-content-center pb-5 mb-3">
      <div class="col-md-7 heading-section heading-section-white text-center ftco-animate">
          <span class="subheading">Membres</span>
        <h2>ONG, OPA &amp; Autres Structures</h2>
      </div>
    </div>
    <div class="row ftco-animate">
      <div class="col-md-12">
        <div class="carousel-testimony owl-carousel ftco-owl">
          @foreach ($organismes as $organisme)
              <div class="item">
                <div class="testimony-wrap py-4">
                  <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></span></div>
                  <div class="text">
                    <p class="mb-4">{!! substr(strip_tags($organisme->description, '(...)'), 0, 100) !!}</p>
                    <div class="d-flex align-items-center">
                      <div class="user-img" style="background-image: url({{ asset('storage/' . $organisme->logo) }})"></div>
                      <div class="pl-3">
                        <p class="name">{{ $organisme->nom }}</p>
                        {{-- <span class="position">{{ $organisme->nom_responsable }}</span> --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>



<section class="ftco-section">
  <div class="container">
    <div class="row justify-content-center pb-5 mb-3">
      <div class="col-md-7 heading-section text-center ftco-animate">
          <span class="subheading">Actualités &amp; Blog</span>
        <h2>Dernières nouvelles de notre blog</h2>
      </div>
    </div>
    <div class="row d-flex">
      @foreach ($lastest_blogs as $latest_blog)
          <div class="col-md-4 d-flex ftco-animate">
            <div class="blog-entry align-self-stretch">
              <a href="{{ route('blog.show', $latest_blog) }}" class="block-20 rounded" style="background-image: url('{{ asset('storage/' . $latest_blog->image) }}');">
              </a>
              <div class="text p-4">
                <div class="meta mb-2">
                  <div><a href="#">{{ Carbon\Carbon::parse($latest_blog->created_at)->locale('fr_FR')->isoFormat('Do MMMM YYYY') }}</a></div>
                  <div><a href="#">{{ $latest_blog->auteur }}</a></div>
                  <div><a href="#" class="meta-chat"><span class="fa fa-comment"></span> 3</a></div>
                </div>
                <h3 class="heading"><a href="#">{!! substr(strip_tags($latest_blog->description, '(...)'), 0, 100) !!}</a></h3>
              </div>
            </div>
          </div>
      @endforeach
      
      
    </div>
  </div>
</section>
    

@include('layouts.newsletter')


@endsection

