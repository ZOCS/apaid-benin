<div class="wrap-pagination-info">
    @if ($paginator->hasPages())
        <nav>
            <ul class="page-numbers">
                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                {{-- <li aria-disabled="true" aria-label="@lang('pagination.previous')"><span aria-hidden="true" class="page-number-item current">Previous</span></li> --}}
                {{-- <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span aria-hidden="true">&lsaquo;</span>
                </li> --}}
                @else
                <li>
                    <a class="page-number-item next-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                        aria-label="@lang('pagination.previous')">Previous</a>
                </li>
                @endif

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="disabled" aria-disabled="true"><a class="page-number-item">{{ $element }}</a></li>
                    {{-- <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li> --}}
                    @endif

                {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="active" aria-current="page"><a class="page-number-item current" href="{{ $url }}">{{ $page }}</a></li>
                    {{-- <li class="active" aria-current="page"><span>{{ $page }}</span></li> --}}
                            @else
                                <li><a class="page-number-item" href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                <li>
                    <a class="page-number-item next-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">Next</a>
                </li>
                {{-- <li>
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li> --}}
                @else
                {{-- <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <a aria-hidden="true" class="page-number-item next-link">Next</a>
                </li> --}}
                @endif
            </ul>
            <p class="result-count">Showing 1-9 of {{ count($paginator) }} result</p>
        </nav>
    @endif
</div>