@extends('layouts.navshop')

@push('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="shop/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="shop/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="shop/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="shop/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="shop/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="shop/css/style.css" type="text/css">
@endpush

@section('content')
    @if(Cart::instance('cart')->count() > 0)
        <!-- Breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="bread-inner">
                            <ul class="bread-list">
                                <li><a href="index1.html">Home<i class="ti-arrow-right"></i></a></li>
                                <li class="active"><a href="blog-single.html">Cart</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcrumbs -->
                
        <!-- Shopping Cart -->
        <div class="shopping-cart section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- Shopping Summery -->
                        <table class="table shopping-summery">
                            <thead>
                                <tr class="main-hading">
                                    <th>PRODUIT</th>
                                    <th>NOM</th>
                                    <th class="text-center">PRIX UNITAIRE</th>
                                    <th class="text-center">QUANTITE</th>
                                    <th class="text-center">TOTAL</th> 
                                    <th class="text-center"><i class="ti-trash remove-icon"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(Cart::instance('cart')->content() as $produit)
                                    <tr>
                                        <td class="image" data-title="No"><img src="{{ asset('storage/'. $produit->model->image) }}" alt="#"></td>
                                        <td class="product-des" data-title="Description">
                                            <p class="product-name"><a href="#">{{ $produit->model->nom }}</a></p>
                                            <p class="product-des">Maboriosam in a tonto nesciung eget  distingy magndapibus.</p>
                                        </td>
                                        <td class="price" data-title="Price"><span>{{ $produit->model->prix }} XOF</span></td>
                                        <td class="shoping__cart__quantity">
                                           
                                            <div class="quantity">
                                                <div class="">
                                                    <input type="number" name="qty" id="qty" data-id="{{ $produit->rowId }}" value="{{$produit->qty}}" style="width: 3rem;">
                                                </div>
                                            </div>
                                        </td>
                                        
                                        <td class="total-amount" data-title="Total"><span>{{$produit->subtotal()}}</span></td>
                                        <td class="action" data-title="Remove">
                                            <form action="{{route('cart.destroy', $produit->rowId)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger" type="submit"><i class="ti-trash remove-icon"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!--/ End Shopping Summery -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- Total Amount -->
                        <div class="total-amount">
                            <div class="row">
                                <div class="col-lg-8 col-md-5 col-12">
                                    <div class="left">

                                        <div class="rounded-pill px-4 py-3 text-uppercase bg-light font-weight-bold">Code Coupon</div>
                                        @if(!request()->session()->has('coupon'))
                                            <div class="coupon">
                                                <form action="{{ route('coupon.apply') }}"  method="POST">
                                                    @csrf
                                                    <input name="code" type="text" placeholder="Entrez votre code ici">
                                                    <button class="btn" type="submit">Appliquer</button>
                                                </form>
                                            </div>
                                        @else
                                                <div class="p-4">
                                                    <p class="font-italic mb-4">Le coupon est deja applique.</p>
                                                </div>
                                        @endif
                                        <div class="checkbox">
                                            <label class="checkbox-inline" for="2"><input name="news" id="2" type="checkbox"> Shipping (+10$)</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-7 col-12">
                                    <div class="right">
                                        <ul>
                                            <li>Sous-total Panier<span>{{ Cart::subtotal()}}</span></li>
                                            <li>Livraison<span>Pas de livraison.</span></li>
                                            <li>Coupon<span>@if(request()->session()->has('coupon'))  {{request()->session()->get('coupon')['code']}} @else {{ __('Pas de coupon disponible') }} @endif</span></li>
                                            <li>Taxe @if (config('cart.tax') > 0)
                                                <span>{{Cart::tax()}}</span>
                                            @else
                                                <span>Pas de taxe.</span>
                                            @endif</li>
                                            <li class="last">Vous payer<span>{{Cart::total()}}</span></li>
                                        </ul>
                                        <div class="button5">
                                            <a href="{{route('checkout.index')}}" class="btn">Passer a la caisse</a>
                                            <a href="{{route('boutique.index')}}" class="btn">Continuer vos achats</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ End Total Amount -->
                    </div>
                </div>
            </div>
        </div>
        <!--/ End Shopping Cart -->
                
        <!-- Start Shop Services Area  -->
        <section class="shop-services section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <!-- Start Single Service -->
                        <div class="single-service">
                            <i class="ti-rocket"></i>
                            <h4>Livraison gratuite</h4>
                            <p>Commandes supérieures à 60 000 Fcfa</p>
                        </div>
                        <!-- End Single Service -->
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <!-- Start Single Service -->
                        <div class="single-service">
                            <i class="ti-reload"></i>
                            <h4>Retour grqtuit</h4>
                            <p>Retour sous 90 jours</p>
                        </div>
                        <!-- End Single Service -->
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <!-- Start Single Service -->
                        <div class="single-service">
                            <i class="ti-lock"></i>
                            <h4>Payement sécurisé</h4>
                            <p>100% payement sécurisé</p>
                        </div>
                        <!-- End Single Service -->
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <!-- Start Single Service -->
                        <div class="single-service">
                            <i class="ti-tag"></i>
                            <h4>Meilleur prix</h4>
                            <p>Prix garanti</p>
                        </div>
                        <!-- End Single Service -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End Shop Newsletter -->
        
        <!-- Start Shop Newsletter  -->
        <section class="shop-newsletter section">
            <div class="container">
                <div class="inner-top">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 col-12">
                            <!-- Start Newsletter Inner -->
                            <div class="inner">
                                <h4>Newsletter</h4>
                                <p> Abonnez-vous à notre newsletter et beneficiez de <span>10%</span> de réduction sur votre premier achat</p>
                                <form action="{{ route('newsletter_email') }}" method="POST" target="_blank" class="newsletter-inner">
                                {{ csrf_field() }}
                                    <input name="user_email" placeholder="Votre adresse e-mail" required="" type="email">
                                    <button class="btn">S'abonner</button>
                                </form>
                            </div>
                            <!-- End Newsletter Inner -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Shop Newsletter -->
        
        
        
        
        @else
        <div class="col-md-12">
            Votre Panier est vide.
        </div>
    @endif

@endsection

@push('js')
    <script src="shop/js/jquery-3.3.1.min.js"></script>
    <script src="shop/js/bootstrap.min.js"></script>
    {{-- <script src="shop/js/jquery.nice-select.min.js"></script> --}}
    <script src="shop/js/jquery-ui.min.js"></script>
    <script src="shop/js/jquery.slicknav.js"></script>
    <script src="shop/js/mixitup.min.js"></script>
    <script src="shop/js/owl.carousel.min.js"></script>
    <script src="shop/js/main.js"></script>
    <script src="js/cart.js"></script>
    

@endpush