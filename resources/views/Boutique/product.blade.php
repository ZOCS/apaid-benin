<style>
    .title-search {
        text-align: center;
        font-size: 1.8rem;
        padding-top: 8rem;
        padding-left: 9rem;
        color: rgb(231, 4, 4);
    }
</style>


@if ($produits->isEmpty())
   <div class="title-search">
    {{ ("Sorry, Products not found!!!") }}
   </div> 
@else
    @php
    $items = Cart::instance('wishlist')->content()->pluck('id');
    @endphp
    <div class="row">
        @foreach($produits as $produit)
        <div class="col-lg-4 col-md-6 col-12 result">
            <div class="single-product">
                <div class="product-img">
                    <a href="product-details.html">
                        <img class="default-img" src="{{ asset('storage/' . $produit->image) }}" alt="#">
                        <img class="hover-img" src="{{ asset('storage/' . $produit->image) }}" alt="#">
                    </a>
                    <div class="button-head">
                        <div class="product-action">
                            <a title="Quick View" href="{{ route('produit.show', $produit->slug) }}"><i
                                    class=" ti-eye"></i><span>Quick Shop</span></a>
                            @if ($items->contains($produit->id))
    
                            <a title="Wishlist" class="fill-heart" href="#"><i class="ti-heart fill-heart"></i><span>Ajouter
                                    aux Souhaits</span></a>
                            @else
                            <a href="javascript:void(0);" onclick="addWish({{ $produit->id }})" title="Ajouter au Panier"><i
                                    class=" ti-heart"></i><span>Liste de Souhait</span></a>
                            @endif
                            <a title="Compare" href="#"><i class="ti-bar-chart-alt"></i><span>Add to Compare</span></a>
                        </div>
                        <div class="product-action-2">
                            <a href="javascript:void(0);" onclick="addCart({{ $produit->id }})"
                                title="Ajouter au panier">Ajouter</a>
                        </div>
                    </div>
                </div>
                <div class="product-content">
                    <h3><a href="product-details.html">{{$produit->nom}}</a></h3>
                    <div class="product-price">
                        <span>{{$produit->prix}}</span>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    
    </div>
@endif