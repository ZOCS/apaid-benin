@extends('layouts.navshop')


@push('css')
<link rel="stylesheet" href="{{asset('shop/css/elegant-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('shop/css/nice-select.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('shop/css/jquery-ui.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('shop/css/owl.carousel.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('shop/css/slicknav.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('shop/css/style.css')}}" type="text/css">
<link rel="stylesheet" href="{{ asset('eshop/css/price.css') }}" type="text/css">
{{-- <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css"> --}}
<link rel="stylesheet" href="{{asset('eshop/css/categorieSidebar.css')}}" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<style>
    .product-action .ti-heart:hover{
    color: #4dca13;
    }
    .product-action a:hover{
    color: #4dca13;
    }
    .fill-heart {
    color:#4dca13 !important;
    }

    .sel {
        width: 13rem;
        height: 0.15rem;
    }
    .sel1 {
        width: 5rem;
        height: 0.15rem;
    }   
</style>

@endpush

@section('content')
<!-- Hero Section End -->
{{-- {{ $slot }} --}}
<style>
    .fill-heart {
        color : #4dca13;
    }
    .heart{
        color : #4dca13 !important;
    }
   
</style>
<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>E-Agro Shop</h2>
                    <div class="breadcrumb__option">
                        <a href="./index.html">Accueil</a>
                        <span>boutique</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<!-- Product Style -->
<section class="product-area shop-sidebar shop section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-12">
                <div class="shop-sidebar">
                    <!-- Single Widget -->
                    <div class="single-widget category">
                        <h3 class="title">Categories</h3>
                        <ul class="categor-list">
                            @foreach($categ as $categorie)
                                <li><a href="{{ route('boutique.index', ['categorie' => $categorie->slug ]) }}">{{$categorie->nom}}</a></li>
                            @endforeach
                            
                        </ul>
                    </div>
                    <!--/ End Single Widget -->
                    <!-- Shop By Price -->
                    <div class="single-widget range">
                        <h3 class="title">Shop by Price</h3>
                        <div class="price-filter">
                            <div class="price-filter-inner">
                                <div id="slider-range"></div>
                                <div class="price_slider_amount">
                                    <div class="label-input">
                                        <span>Range:</span><input type="text" id="minprice" readonly /> - <input type="text" id="maxprice" readonly/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="check-box-list">
                            <li>
                                <label class="checkbox-inline" for="c1"><input name="news" id="c1" type="checkbox" onclick="functOne()">1000 CFA -
                                    5000 CFA<span class="count">(3)</span></label>
                            </li>
                            <li>
                                <label class="checkbox-inline" for="c2"><input name="news" id="c2" type="checkbox" onclick="functTwo()">5000 CFA -
                                    10000 CFA<span class="count">(5)</span></label>
                            </li>
                            <li>
                                <label class="checkbox-inline" for="c3"><input name="news" id="c3" type="checkbox" onclick="functThree()">10000 CFA -
                                    20000 CFA<span class="count">(8)</span></label>
                            </li>
                        </ul>
                    </div>
                    <!--/ End Shop By Price -->
                    <!-- Single Widget -->
                    <div class="single-widget recent-post">
                        <h3 class="title">Recent post</h3>
                        <!-- Single Post -->
                        <div class="single-post first">
                            <div class="image">
                                <img src="https://via.placeholder.com/75x75" alt="#">
                            </div>
                            <div class="content">
                                <h5><a href="#">Girls Dress</a></h5>
                                <p class="price">$99.50</p>
                                <ul class="reviews">
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li><i class="ti-star"></i></li>
                                    <li><i class="ti-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Post -->
                        <!-- Single Post -->
                        <div class="single-post first">
                            <div class="image">
                                <img src="https://via.placeholder.com/75x75" alt="#">
                            </div>
                            <div class="content">
                                <h5><a href="#">Women Clothings</a></h5>
                                <p class="price">$99.50</p>
                                <ul class="reviews">
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li><i class="ti-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Post -->
                        <!-- Single Post -->
                        <div class="single-post first">
                            <div class="image">
                                <img src="https://via.placeholder.com/75x75" alt="#">
                            </div>
                            <div class="content">
                                <h5><a href="#">Man Tshirt</a></h5>
                                <p class="price">$99.50</p>
                                <ul class="reviews">
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li class="yellow"><i class="ti-star"></i></li>
                                    <li class="yellow"><i class="ti-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Post -->
                    </div>
                    <!--/ End Single Widget -->
                    
                </div>
            </div>
            <div class="col-lg-9 col-md-8 col-12">
                <div class="row">
                    <div class="col-12">
                        <!-- Shop Top -->
                        <div class="shop-top">
                            <div class="shop-shorter">
                                <div class="single-shorter">
                                    <label>Show :</label>
                                    <select class="form-control sel1" name="post-per-page" id="perPage" onchange="perPage()">
                                        <option value="9" selected="selected">09</option>
                                        <option value="12">12</option>
                                        <option value="15">15</option>
                                        <option value="18">18</option>
                                        <option value="21">21</option>
                                    </select>
                                </div>
                                <div class="single-shorter">
                                    <label>Sort By :</label>
                                    <select name="orderby" id="orderby" class="form-control sel" onchange="sorting()">
                                        <option value="menu_order" selected="selected">Default sorting</option>
                                        <option value="popularity">Sort by popularity</option>
                                        <option value="promo">Sort by promotion</option>
                                        <option value="new">Sort by newness</option>
                                        <option value="price-asc">Sort by price: low to high</option>
                                        <option value="price-desc">Sort by price: high to low</option>
                                    </select>
                                </div>
                            </div>
                            <ul class="view-mode">
                                <li class="active"><a href="shop-grid.html"><i class="fa fa-th-large"></i></a></li>
                                <li><a href="shop-list.html"><i class="fa fa-th-list"></i></a></li>
                            </ul>
                        </div>
                        <!--/ End Shop Top -->
                    </div>
                </div>
                @php
                    $items = Cart::instance('wishlist')->content()->pluck('id');
                @endphp
                <div class="row" id="searchResults">
                    @foreach($produits as $produit)
                    <div class="col-lg-4 col-md-6 col-12 result">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="{{ route('produit.show', $produit->slug) }}">
                                    <img class="default-img" src="{{ asset('storage/' . $produit->image) }}" alt="#">
                                    <img class="hover-img" src="{{ asset('storage/' . $produit->image) }}" alt="#">
                                </a>
                                <div class="button-head">
                                    <div class="product-action">
                                        <a  title="Quick View" href="{{ route('produit.show', $produit->slug) }}"><i class=" ti-eye"></i><span>Quick Shop</span></a>
                                        @if ($items->contains($produit->id))
                                        
                                            <a title="Wishlist" class="fill-heart" href="#"><i class="ti-heart fill-heart"></i><span>Ajouter aux Souhaits</span></a>
                                        @else
                                           <a href="javascript:void(0);" onclick="addWish({{ $produit->id }})" title="Ajouter au Panier"><i class=" ti-heart"></i><span>Liste de Souhait</span></a>
                                        @endif
                                        <a title="Compare" href="#"><i class="ti-bar-chart-alt"></i><span>Add to Compare</span></a>
                                    </div>
                                    <div class="product-action-2">
                                        <a href="javascript:void(0);" onclick="addCart({{ $produit->id }})"  title="Ajouter au panier">Ajouter</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content">
                                <h3><a href="{{ route('produit.show', $produit->slug) }}">{{$produit->nom}}</a></h3>
                                <div class="product-price">
                                    <span>{{$produit->prix}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                {{$produits->links('pagination::shop-pagination')}}
            </div>
        </div>
    </div>
</section>
<!--/ End Product Style 1  -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
    
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
                        aria-hidden="true"></span></button>
            </div>
            <div class="modal-body fetched-data">
                <div class="row no-gutters">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <!-- Product Slider -->
                        <div class="product-gallery">
                            <div class="quickview-slider-active">
                                <div class="single-slider">
                                    <img src="https://via.placeholder.com/569x528" alt="#">
                                </div>
                                <div class="single-slider">
                                    <img src="https://via.placeholder.com/569x528" alt="#">
                                </div>
                                <div class="single-slider">
                                    <img src="https://via.placeholder.com/569x528" alt="#">
                                </div>
                                <div class="single-slider">
                                    <img src="https://via.placeholder.com/569x528" alt="#">
                                </div>
                            </div>
                        </div>
                        <!-- End Product slider -->
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="quickview-content">
                            <h2>Nom</h2>
                            <div class="quickview-ratting-review">
                                <div class="quickview-ratting-wrap">
                                    <div class="quickview-ratting">
                                        <i class="yellow fa fa-star"></i>
                                        <i class="yellow fa fa-star"></i>
                                        <i class="yellow fa fa-star"></i>
                                        <i class="yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                    <a href="#"> (1 customer review)</a>
                                </div>
                                <div class="quickview-stock">
                                    <span><i class="fa fa-check-circle-o"></i> in stock</span>
                                </div>
                            </div>
                            <h3>$29.00</h3>
                            <div class="quickview-peragraph">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum ad
                                    impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui nemo
                                    ipsum numquam.</p>
                            </div>
                            <div class="size">
                                <div class="row">
                                    <div class="col-lg-6 col-12">
                                        <h5 class="title">Size</h5>
                                        <select>
                                            <option selected="selected">s</option>
                                            <option>m</option>
                                            <option>l</option>
                                            <option>xl</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <h5 class="title">Color</h5>
                                        <select>
                                            <option selected="selected">orange</option>
                                            <option>purple</option>
                                            <option>black</option>
                                            <option>pink</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="quantity">
                                <!-- Input Order -->
                                <div class="input-group">
                                    <div class="button minus">
                                        <button type="button" class="btn btn-primary btn-number" disabled="disabled"
                                            data-type="minus" data-field="quant[1]">
                                            <i class="ti-minus"></i>
                                        </button>
                                    </div>
                                    <input type="text" name="quant[1]" class="input-number" data-min="1" data-max="1000"
                                        value="1">
                                    <div class="button plus">
                                        <button type="button" class="btn btn-primary btn-number" data-type="plus"
                                            data-field="quant[1]">
                                            <i class="ti-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <!--/ End Input Order -->
                            </div>
                            <div class="add-to-cart">
                                <a href="#" class="btn">Add to cart</a>
                                <a href="#" class="btn min"><i class="ti-heart"></i></a>
                                <a href="#" class="btn min"><i class="fa fa-compress"></i></a>
                            </div>
                            <div class="default-social">
                                <h4 class="share-now">Share:</h4>
                                <ul>
                                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a class="youtube" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    <li><a class="dribbble" href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal end -->

@endsection

@push('js')
	
    <script src="{{asset('shop/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('shop/js/bootstrap.min.js')}}"></script>
    {{-- <script src="{{asset('shop/js/jquery.nice-select.min.js')}}"></script> --}}
    <script src="{{asset('shop/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('shop/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('shop/js/mixitup.min.js')}}"></script>
    <script src="{{asset('shop/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('eshop/js/functions.js')}}"></script>
    {{-- <script src="{{asset('shop/js/main.js')}}"></script> --}}
     
@endpush

