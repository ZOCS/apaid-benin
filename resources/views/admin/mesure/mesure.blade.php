@extends('layouts.navadmin')


@section('content')
        @if(session()->has('message'))
            <div class="alert alert-success center">
                {{ session()->get('message') }}
            </div>
        @endif
         @if(count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						<li>
							{{ $error }}
						</li>
					@endforeach
				</ul>
			</div>
		@endif
                <!-- Wizard form -->
                <div class="pd-20 card-box mb-30">
					<div class="clearfix">
						<div class="pull-left">
							<h4 class="text-blue h4">Mesure</h4>
							<p class="mb-30">Enregistrement de mesure</p>
						</div>
						<div class="pull-right">
							<a href="#" class="btn btn-primary btn-sm scroll-click" rel="content-y" data-toggle="modal" data-target="#bd-example-modal-lg" type="button" role="button"> Ajouter mesure</a>
						</div>
					</div>
					
					<div class="wizard-content">
						<form class="tab-wizard wizard-circle wizard" method="post" action="{{route('admin.mesure.store')}}" enctype="multipart/form-data">
							@csrf
							<h5>Mesure</h5>
							<section>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label >Intitule *</label>
											<input type="text" name="title"  type="text" placeholder="Kilogramme" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label >Symbole de la mesure *</label>
											<input type="text" class="form-control" name="slug"  type="text" placeholder="Kg" required>
										</div>
									</div>
								</div>
							</section>
							<div class="row align-center d-flex">
								<div class="col-sm-6 col-md-4">
									<button type="reset" class="btn btn-danger">Effacer</button>
								</div>
								<div class="col-sm-6 col-md-6">
									<button type="submit" class="btn btn-primary">Enregistrer</button>
								</div>
							</div>
						</form>
					</div>
				</div>

            </div>
        </div>
@endsection