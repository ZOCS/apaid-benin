@extends('layouts.navadmin')

@section('content')

        <div class="min-height-200px">
            @if(session()->has('message'))
                <div class="alert alert-success center">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if(count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						<li>
							{{ $error }}
						</li>
					@endforeach
				</ul>
			</div>
		@endif
            <div class="card-box mb-40">
                <div class="clearfix">
                    <div class="pd-20 pull-left">
                        <h4 class="text-blue h4">La liste des mesures</h4>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="btn btn-primary btn-sm scroll-click" rel="content-y" data-toggle="modal" data-target="#bd-example-modal-lg" type="button" role="button"> Ajouter Categorie</a>
                    </div>
                </div>
                <div class="pb-20">
                    <table class="data-table table hover multiple-select-row nowrap">
                        <thead>
                            <tr>
                                <th class="table-plus datatable-nosort">Identifiant</th>
                                <th>Nom</th>
                                <th>Slug</th>
                                <th class="datatable-nosort">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $mesures as $mesure )
                            <tr>
                                <td>
                                    {{ $mesure->id }}
                                </td>
                                <td>
                                    {{ $mesure->titre }}
                                </td>
                                <td>{{ $mesure->slug }}</td>
                                <td>
                                    <div class="dropdown">
                                        <a class="dropdown-item" href="{{ route('admin.mesure.show', Crypt::encrypt($mesure)) }}"><i class="dw dw-eye"></i> Voir</a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                            <a class="dropdown-item" href="{{route('admin.mesure.edit',  Crypt::encrypt($mesure))}}"><i class="dw dw-edit2"></i> Modifier</a>
                                            <form action="{{ route('admin.mesure.destroy', Crypt::encrypt($mesure)) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="dropdown-item" id="sa-title" type="submit"><i class="dw dw-delete-3"></i> Supprimer</button>

                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                            
                        </tbody>
                        
                    </table>
                
                    {{-- <div class="d-flex justify-content-center">
                        {{ $mesures->links() }}
                    </div> --}}
                </div>

            </div>
            <div class="modal fade bs-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Insertion de mesure</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <form class="form" method="post" action="{{route('admin.mesure.store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-2 col-form-label">Intitule : </label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control" name="titre" type="text" placeholder="Agriculture">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-2 col-form-label">Slug</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control" name="slug" type="text" placeholder="Le slug">
                                    </div>
                                </div>
                                
                                <div class="row align-center">
                                    <div class="col-sm-6 col-md-4">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        {{-- <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
             
@endsection