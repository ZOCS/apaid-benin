@extends('layouts.navadmin')


@section('content')
        @if(session()->has('message'))
            <div class="alert alert-success center">
                {{ session()->get('message') }}
            </div>
        @endif

        <div class="min-height-300px">
			 <!-- Wizard form -->
                <div class="pd-20 card-box mb-30">
					<div class="clearfix">
						<div class="pull-left">
							<h4 class="text-blue h4">Modifier Utilisteur</h4>
							<p class="mb-30">Modification</p>
						</div>
					</div>
					
					<div class="wizard-content">
						<form class="tab-wizard wizard-circle wizard" method="post" action="{{route('user.edit', Crypt::encrypt($user))}}" enctype="multipart/form-data">
							@method('put')
							@csrf
							<h5>Utilisateur {{ $user->name }}</h5>
							<section>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label >Nom utilisateur *</label>
											<input type="text" name="nom" placeholder="Nom du organisme" value="{{ $user->name ? : '' }}" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label >Email *</label>
											<input type="text" class="form-control" name="responsable" value="{{ $user->email ? : '' }}" placeholder="Le nom du responsable" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>{{__('Domaine d\'intervention* :')}}</label>
											<input type="text" class="form-control" name="phone" value="{{ $user->phone ? : '' }}"  type="text" required>
										</div>
									</div>
                                    <div class="col-md-6">
										<div class="form-group">
											<label>{{__('Domaine d\'intervention* :')}}</label>
											<input type="text" class="form-control" name="pays" value="{{ $user->pays ? : '' }}"  type="text" required>
										</div>
									</div>
									
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Sexe :</label>
											<select class="custom-select2 form-control" style="width: 100%;" name="sexe" required>
                                                <option>Selectionnez le sexe</option>
												<option value="masculin" {{ old('sexe', isset($user->sexe) == "masculin" ? 'selected' : '') ? 'selected' : '' }}>Masculin</option>
												<option value="feminin" {{ old('sexe', isset($user->sexe) == "feminin" ? 'selected' : '') ? 'selected' : ''  }}>Feminin</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Role :</label>
											<select class="custom-select2 form-control" style="width: 100%;" name="role" required>
                                                <option>Selectionnez le role</option>
												<option value="admin" {{ old('role', isset($user->role) == "admin" ? 'selected' : '') ? 'selected' : '' }}>Admin</option>
												<option value="blogger" {{ old('role', isset($user->role) == "blogger" ? 'selected' : '') ? 'selected' : ''  }}>Blogger</option>
                                                <option value="manage" {{ old('role', isset($user->role) == "manage" ? 'selected' : '') ? 'selected' : ''  }}>Manager</option>
                                                <option value="user" {{ old('role', isset($user->role) == "user" ? 'selected' : '') ? 'selected' : ''  }}>User</option>
											</select>
										</div>
									</div>
								</div>
								
							</section>
							
							
							<div class="row align-center d-flex">
								<div class="col-sm-6 col-md-4">
									<button type="reset" class="btn btn-danger">Effacer</button>
								</div>
								<div class="col-sm-6 col-md-6">
									<button type="submit" class="btn btn-primary">Enregistrer</button>
								</div>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
@endsection