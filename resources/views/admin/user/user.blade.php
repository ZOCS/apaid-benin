@extends('layouts.navadmin')

@section('content')

        @if(session()->has('message'))
            <div class="alert alert-success center">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="card-box mb-40">
            <h2 class="h4 pd-20">Gestion Utilisateur</h2>
        
            <table class="data-table table nowrap">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Users</th>
                        <th>Nom</th>
                        <th>E-mail</th>
                        <th>Sexe</th>
                        <th>Phone</th>
                        <th>Pays</th>
                        <th>Role</th>
                        <th class="datatable-nosort">Action</th>
                    </tr>
                </thead>
                
                <tbody>
                @foreach( $users as $user )
                    <tr>
                        <td class="table-plus">
                            <img src="deskapp/vendors/images/product-1.jpg" width="70" height="70" alt="">
                        </td>
                        <td>
                           {{ $user->name }}
                        </td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->sexe }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->pays }}</td>
                        <td>@if($user->role == 'admin') <span  class="badge badge-success">{{ __('Admin') }} </span>
                            @elseif ($user->role == 'user')
                            <span  class="badge badge-warning">{{ __("User") }}</span>
                            @elseif ($user->role == 'manage')
                            <span  class="badge badge-primary">{{ __("Manager") }}</span>
                            @elseif ($user->role == 'blogger')
                            <span  class="badge badge-info">{{ __("Blogger") }}</span>
                            @endif </td>
                        <td>
                            
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="{{route('user.edit', Crypt::encrypt($user))}}"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="{{ route('user.destroy', $user) }}"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            
            </table>
        </div>
    </div>
</div>
              
@endsection