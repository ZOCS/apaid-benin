@extends('layouts.navadmin')

@section('content')
    <div class="card-box mb-30">
        <div class="pd-20">
            <h4 class="text-blue h4">Liste des offres</h4>
        </div>
        <div class="pb-20">
            <table class="table  data-table-export nowrap">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Reference</th>
                        <th>Entreprise</th>
                        <th>Domaine</th>
                        <th>Date limite</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($offres as $offre)
                        <tr>
                            <td class="table-plus">{{$offre->reference}}</td>
                            <td>{{$offre->entreprise}}</td>
                            <td>{{$offre->poste}}</td>
                            <td>{{$offre->expiration}}</td>
                            <td>
                            <div class="row">
                                <a class="btn btn-secondary" role="button" href="{{ route('admin.offre.show', $offre) }}"><i class="dw dw-eye"></i></a>
                                <a class="btn btn-warning" role="button" href="{{ route('admin.offre.edit', $offre) }}"><i class="icon-copy dw dw-edit-1"></i></a>
                                <form action="{{ route('admin.offre.destroy', $offre) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" id="sa-title" type="submit"><i class="dw dw-delete-3"></i></button>

                                </form>
                            </div>
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection