@extends('layouts.navadmin')


@section('content')
@if(session()->has('message'))
<div class="row pull-right">
    <div class="alert alert-success alert-dismissible fade show col-md-4" role="alert">
        <strong>Holy guacamole!</strong> {{ session()->get('message') }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
    </div>
</div>
@endif
@if(count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>
            {{ $error }}
        </li>
        @endforeach
    </ul>
</div>
@endif
{!! Toastr::message() !!}
<!-- Wizard form -->
<div class="pd-20 card-box mb-30">
    <div class="clearfix">
        <div class="pull-left">
            <h4 class="text-blue h4">Offre</h4>
            <p class="mb-30">Enregistrement de Offre</p>
        </div>
    </div>

    <div class="wizard-content">
        <form class="form" method="post" action="{{route('admin.offre.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{ __('Titre') }}</label>
                        <input class="form-control" name="title" type="text" placeholder="APAID">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{ __('Nom de l\'entreprise') }}</label>
                        <input class="form-control" name="entreprise" type="text" placeholder="APAID">
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{__('Date d\'expiration')}}</label>
                        <input class="form-control" name="expiration" placeholder="Date d'expiration" type="date">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Categorie offre* :</label>
                        <select class="custom-select2 form-control" multiple="multiple" style="width: 100%;" name="categorie[]"
                            required>
                            <option value="{{ '' }}">Choisir...</option>
                            @foreach($categories as $categorie)
                            <option value="{{ $categorie->id  }}">{{ $categorie->nom }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Choisir une image</label>
                        <div class="custom-file">
                            <input type="file" name="image" class="col-12 custom-file-input">
                            <label class="custom-file-label">Choisir une image</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__("Poste")}}</label>
                            <input class="form-control" name="poste" placeholder="Charge en communication" type="text">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="weight-600">Status</label>
                        <div class="custom-control custom-radio mb-5">
                            <input type="radio" id="customRadio1" name="status" value="1" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio1">Actif</label>
                        </div>
                        <div class="custom-control custom-radio mb-5">
                            <input type="radio" id="customRadio2" name="status" value="0" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio2">Non actif</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="summernote form-control border-radius-0" name="description"
                            placeholder="Entrez la description"></textarea>
                    </div>
                </div>
            </div>
        
            <div class="row align-center">
                <div class="col-sm-6 col-md-4">
                    <button type="reset" class="btn btn-danger">Effacer</button>
                </div>
                <div class="col-sm-6 col-md-6">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>
</div>
@endsection