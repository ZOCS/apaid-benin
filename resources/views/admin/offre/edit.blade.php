@extends('layouts.navadmin')


@section('content')
    <div class="pd-ltr-20 xs-pd-20-10">
        @if(session()->has('message'))
            <div class="alert alert-success center">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="min-height-300px">
            <div class="pd-20 card-box p-10 mb-30">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-blue h4">Offres</h4>
                        <p class="mb-30">Insertion des offres</p>
                    </div>
                    <div class="pull-right">
                        {{-- <a href="#" class="btn btn-primary btn-sm scroll-click" rel="content-y" data-toggle="modal" data-target="#bd-example-modal-lg" type="button" role="button"> Aouter Categorie</a> --}}
                    </div>
                </div>
                    <form class="form" method="post" action="{{route('admin.offre.update', $offre)}}" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="row">
						    <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('Titre de l\'offre') }}</label>
                                    <input class="form-control" name="title" value="{{$offre->title ? : ''}}" type="text" placeholder="APAID">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('Nom de l\'entreprise') }}</label>
                                    <input class="form-control" name="entreprise" value="{{$offre->entreprise ? : ''}}" type="text"
                                            placeholder="APAID">
                                </div>
                            </div>
                        </div>
                        <div class="row">
						    <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('Date d\'expiration') }}</label>
                                    <input class="form-control" name="expiration" value="{{$offre->expiration ? : ''}}" placeholder="Nom de l'auteur" type="date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Choisir une image</label>
                                    <input type="file" name="image" class="col-12 custom-file-input"
                                            value="{{ asset('storage/'. $offre->image) }}">
                                    <label class="custom-file-label">Choisir une image</label>
                                    <img class="col-sm-4" src="{{ asset('storage/' . $offre->image) ? : '' }}" alt="Image produit"
                                        style="width:120px; height:100px;">
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row"> --}}
						    
						    {{-- <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-12 col-md-2 col-form-label">{{ ('Date de publication') }}</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control" name="domaine" value="{{$offre->publie ? : ''}}"  type="date">
                                    </div>
                                </div>
                            </div> --}}
                        {{-- </div> --}}
                        {{-- <div class="row"> --}}
						    {{-- <div class="col-md-6">
                               <div class="form-group">
                                    <label class="col-sm-12 col-md-2 col-form-label">{{ ('Experiences') }}</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control" name="domaine" value="{{$offre->experience ? : ''}}"  type="text">
                                    </div>
                                </div>
                            </div> --}}
						    {{-- <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-12 col-md-2 col-form-label">{{ ('Niveau') }}</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control" name="domaine" value="{{$offre->publie ? : ''}}"  type="text">
                                    </div>
                                </div>
                            </div> --}}
                        {{-- </div> --}}
                        <div class="form-group row">
                            <label>Description</label>
                                <textarea class="summernote form-control border-radius-2" name="description" placeholder="Entrez la description">{{$offre->description ? : ''}}</textarea>
                        </div>
                        
                        <div class="row align-center">
                            <div class="col-sm-6 col-md-4">
                                <button type="reset" class="btn btn-danger">Effacer</button>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection