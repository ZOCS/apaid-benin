@extends('layouts.navadmin')


@section('content')
        @if(session()->has('message'))
            <div class="alert alert-success center">
                {{ session()->get('message') }}
            </div>
        @endif
        
                <!-- Wizard form -->
                <div class="pd-20 card-box mb-30">
					<div class="clearfix">
						<div class="pull-left">
							<h4 class="text-blue h4">Categorie</h4>
							<p class="mb-30">Enregistrement de categorie</p>
						</div>
						<div class="pull-right">
							<a href="#" class="btn btn-primary btn-sm scroll-click" rel="content-y" data-toggle="modal" data-target="#bd-example-modal-lg" type="button" role="button"> Ajouter Categorie</a>
						</div>
					</div>
					
					<div class="wizard-content">
						<form class="tab-wizard wizard-circle wizard" method="post" action="{{route('admin.categorie.update', $categorie)}}" enctype="multipart/form-data">
							@csrf
                            @method('put')
							<h5>Categorie</h5>
							<section>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label >Intiule *</label>
											<input type="text" name="nom"  type="text" value="{{ $categorie->titre ? : '' }}" placeholder="Nom de la categorie" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label >Slug *</label>
											<input type="text" class="form-control" name="slug" value="{{ $categorie->slug ? : '' }}"  type="text" placeholder="Le slug" required>
										</div>
									</div>
								</div>
                                <div class="row">
                                    <div class="col-md-6">
										<div class="form-group">
											<label>Categorie Produit* :</label>
											<select class="custom-select2 form-control" multiple="multiple" style="width: 100%;" name="categorie[]" required>
                                                @foreach($categories as $categorie)
                                                <option value="{{ $categorie->id  }}" {{ old('categorie[]', isset($produit) ? (in_array($categorie->id, $produit->categories->pluck('id')->toArray()) ? 'selected' : '') : '') }}>{{ $categorie->nom  }}</option>
                                                @endforeach
											</select>
										</div>
									</div>
                                </div>
							</section>
							<div class="row align-center d-flex">
								<div class="col-sm-6 col-md-4">
									<button type="reset" class="btn btn-danger">Effacer</button>
								</div>
								<div class="col-sm-6 col-md-6">
									<button type="submit" class="btn btn-primary">Enregistrer</button>
								</div>
							</div>
						</form>
					</div>
				</div>

            </div>
        </div>
@endsection