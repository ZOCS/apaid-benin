@extends('layouts.navadmin')

@section('content')
        @if(session()->has('message'))
            <div class="alert alert-success center">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="clearfix">
            <div class="pull-left">
                <h4 class="text-blue h4">Type de categorie</h4>
                <p class="mb-30"></p>
            </div>
            <div class="pull-right">
                <a href="#" class="btn btn-primary btn-sm scroll-click" rel="content-y" data-toggle="modal"
                    data-target="#bd-example-modal-lg" type="button" role="button"> Ajouter Categorie</a>
            </div>
        </div>
        <div class="card-box mb-40">
            <h2 class="h4 pd-10">Liste des categories</h2>
            <table class="checkbox-datatable table  data-table-export nowrap">
                <thead>
                    <tr>
                        {{-- <th class="table-plus datatable-nosort">Image</th> --}}
                        <th>Nom </th>
                        <th>Slug</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach( $categories as $categorie)
                    <tr>
                        <td>
                           {{$categorie->nom}}
                        </td>
                        <td>{{$categorie->slug}}</td>
                        
                        <td>
                            <div class="row">
                                <a class="btn btn-secondary" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                <a class="btn btn-warning" href="{{route('admin.edit-product', $categorie)}}"><i class="dw dw-edit2"></i> Modifier</a>
                                <form action="{{ route('admin.destroy-product', $categorie) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" id="sa-title" type="submit"><i class="dw dw-delete-3"></i> Supprimer</button>

                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach 
                </tbody>
            </table>
        </div>
   
        <div class="modal fade bs-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog"
            aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel">Insertion Categorie</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form" method="post" action="{{route('admin.categorie.store')}}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-12 col-md-2 col-form-label">Nom categorie</label>
                                <div class="col-sm-12 col-md-10">
                                    <input class="form-control" name="nom" type="text" placeholder="Agriculture">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-12 col-md-2 col-form-label">Slug</label>
                                <div class="col-sm-12 col-md-10">
                                    <input class="form-control" name="slug" type="text" placeholder="Le slug">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-12 col-md-2 col-form-label">Categorie Parent</label>
                                <div class="col-sm-12 col-md-10">
                                    <select class="custom-select2 col-12 " name="parent_id" style="width: 100%;">
                                        <option value="{{ '' }}">Choisir..</option>
                                        @foreach($categories as $categorie)
                                        <option value="{{ $categorie->id  }}">{{ $categorie->nom }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
        
                            <div class="row align-center">
                                <div class="col-sm-6 col-md-4">
                                    <button type="reset" class="btn btn-danger">Effacer</button>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

              
@endsection