@extends('layouts.navadmin')


@section('content')
        @if(session()->has('message'))
            <div class="alert alert-success center">
                {{ session()->get('message') }}
            </div>
        @endif
        @if(count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						<li>
							{{ $error }}
						</li>
					@endforeach
				</ul>
			</div>
		@endif
        <div class="min-height-300px">
            <div class="pd-20 card-box mb-30">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-blue h4">Projets</h4>
                        <p class="mb-30">Insertion de projet</p>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="btn btn-primary btn-sm scroll-click" rel="content-y" data-toggle="modal" data-target="#bd-example-modal-lg" type="button" role="button"> Ajouter Categorie</a>
                    </div>
                </div>
                    <form class="form" method="post" action="{{route('admin.store-projet')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-2 col-form-label">Titre du projet</label>
                            <div class="col-sm-12 col-md-10">
                                <input class="form-control" name="title" type="text" placeholder="Titre ddu projet">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-2 col-form-label">Auteur</label>
                            <div class="col-sm-12 col-md-10">
                                <input class="form-control" name="auteur"  placeholder="Nom de l'auteur" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Image Projet</label>
							<div class="col-sm-12 col-md-10 custom-file">
								<input type="file" name="image" class="col-12 custom-file-input">
								<label class="custom-file-label">Choisir une image</label>
							</div>
						</div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-2 col-form-label">Categorie</label>
                            <div class="col-sm-12 col-md-10">
                                <select class="custom-select2 col-12 form-control" multiple="multiple" name="categorie[]" required>
                                    <option value={{  ''  }}>Choisir...</option>
                                    @foreach($categories as $categorie)
                                        <option value="{{ $categorie->id  }}">{{ $categorie->nom  }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-2 col-form-label">Description</label>
                            <div class="col-sm-12 col-md-10">
                                <textarea class="summernote form-control border-radius-0" name="description" placeholder="Entrez la description"></textarea>
                            </div>
                        </div>
                        
                        <div class="row align-center">
                            <div class="col-sm-6 col-md-4">
                                <button type="reset" class="btn btn-danger">Effacer</button>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal fade bs-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Insertion Categorie</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <form class="form" method="post" action="{{route('admin.categorie.store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-2 col-form-label">Nom categorie</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control" name="nom" type="text" placeholder="National...">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-2 col-form-label">Slug</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control" name="slug" type="text" placeholder="Le slug">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-2 col-form-label">Categorie Parent</label>
                                    <div class="col-sm-12 col-md-10">
                                        <select class="custom-select2 col-12 " name="parent_id" style="width: 100%;" >
                                           <option value="{{ '' }}">Choisir..</option>
                                            @foreach($categories as $categorie)
                                            <option value="{{ $categorie->id  }}">{{ $categorie->nom  }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-danger">Effacer</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection