@extends('layouts.navadmin')


@section('content')
<div class="card-box mb-30">
					<div class="pd-20">
						<h4 class="text-blue h4">Liste des projets</h4>
					</div>
					<div class="pb-20">
						<table class="checkbox-datatable table nowrap">
							<thead>
								<tr>
									<th><div class="dt-checkbox">
											<input type="checkbox" name="select_all" value="1" id="example-select-all">
											<span class="dt-checkbox-label"></span>
										</div>
									</th>
									<th>Titre</th>
									<th>Auteur</th>
									<th>Status Archive</th>
									<th>Description</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($projets as $projet)
									<tr>
										<td></td>
										<td>{{$projet->title}}</td>
										<td>{{$projet->auteur}}</td>
										<td>{{$projet->status}}</td>
										<td>{{Str::limit($projet->description, 10)}}</td>
										<td>$162,700</td>
										<td>
											<div class="dropdown">
												<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
													<i class="dw dw-more"></i>
												</a>
												<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
													<a class="dropdown-item" href="{{route('admin.edit-projet', Crypt::encrypt($projet))}}"><i class="dw dw-edit2"></i> Modifier</a>
													<form action="{{ route('admin.destroy-projet', Crypt::encrypt($projet)) }}" method="POST">
														@csrf
														@method('DELETE')
														<button class="dropdown-item" id="sa-title" type="submit"><i class="dw dw-delete-3"></i> Supprimer</button>

													</form>
												</div>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>

@endsection
