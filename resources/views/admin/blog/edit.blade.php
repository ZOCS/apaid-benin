@extends('layouts.navadmin')


@section('content')
        @if(session()->has('message'))
        <div class="row">    
            <div class="alert alert-success alert-dismissible fade show col-lg-4" role="alert">
                <strong>Holy guacamole!</strong> {{ session()->get('message') }}.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
        </div>
        @endif
        <div class="min-height-300px">
            <div class="pd-20 card-box mb-30">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-blue h4">Blog</h4>
                        <p class="mb-30">Insertion de blog</p>
                    </div>
                    <div class="pull-right">
                        {{-- <a href="#" class="btn btn-primary btn-sm scroll-click" rel="content-y" data-toggle="modal" data-target="#bd-example-modal-lg" type="button" role="button"> Ajouter Categorie</a> --}}
                    </div>
                </div>
                    <form class="form" method="post" action="{{route('admin.blog.update', $blog)}}" enctype="multipart/form-data">
                        
                        @method('put')
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-2 col-form-label">Libelle du blog</label>
                            <div class="col-sm-12 col-md-10">
                                <input class="form-control" name="title" type="text" value="{{ $blog->title ? : '' }}" placeholder="Titre du blog">
                            </div>
                        </div>
                        <div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Image Blog</label>
							<div class="col-sm-12 col-md-10 custom-file">
								<input type="file" name="image" class="col-12 custom-file-input">
								<label class="custom-file-label">Choisir une image</label>
							</div>
						</div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-2 col-form-label">Auteur</label>
                            <div class="col-sm-12 col-md-10">
                                <input class="form-control" name="auteur" value="{{ $blog->auteur ? : '' }}" type="text" placeholder="Auteur">
                            </div>
                        </div>
						<div class="form-group">
							<label>Categorie Produit* :</label>
							<select class="custom-select2 form-control" multiple="multiple" style="width: 100%;" name="categorie[]" required>
                                @foreach($categories as $categorie)
                                    <option value="{{ $categorie->id  }}" {{ old('categorie[]', isset($produit) ? (in_array($categorie->id, $produit->categories->pluck('id')->toArray()) ? 'selected' : '') : '') }}>{{ $categorie->nom  }}</option>
                                @endforeach
							</select>
						</div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status (Actif ou Archiver)</label>
                                    <div class="custom-control custom-checkbox mb-5">
                                        <input type="checkbox" name="status" value="1" class="custom-control-input" id="featured" {{
                                            old('featured', $retVal =  ($blog->status == true) ? 'checked="checked"' : '') ? 'checked="checked"' : ''
                                        }}/>
                                        <label class="custom-control-label" for="featured">Archiver</label>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-2 col-form-label">Description</label>
                            <div class="col-sm-12 col-md-10">
                                <textarea class="summernote form-control" rows="3" name="description" placeholder="Entrez la description"> value="{{ $blog->description ? : '' }}"</textarea>
                            </div>
                        </div>
                        
                        <div class="row align-center">
                            <div class="col-sm-6 col-md-4">
                                <button type="reset" class="btn btn-danger">Effacer</button>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
@endsection