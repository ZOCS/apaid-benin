@extends('layouts.navadmin')


@section('content')
<div class="card-box mb-30">
					<div class="pd-20">
						<h4 class="text-blue h4">Liste des blogs</h4>
					</div>
					<div class="pb-20">
						<table class="checkbox-datatable table  data-table-export nowrap">
							<thead>
								<tr>
									<th>
										<div class="dt-checkbox">
											<input type="checkbox" name="select_all" value="1" id="example-select-all">
											<span class="dt-checkbox-label"></span>
										</div>
									</th>
									<th>Image</th>
									<th>Titre</th>
									<th>Auteur</th>
									<th>Date de creation</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($blogs as $blog)
									<tr>
										<th>
											<div class="dt-checkbox">
												<input type="checkbox" name="select_all" value="{{ $blog->id }}" id="example-select-all">
												<span class="dt-checkbox-label"></span>
											</div>
										</th>
										<td class="table-plus">
											 <img src="{{ asset('storage/' . $blog->image) }}" width="70" height="70" alt="">
										</td>
										<td>{{$blog->title}}</td>
										<td>{{$blog->auteur}}</td>
										<td> @php setLocale(LC_TIME, 'French'); @endphp {{$blog->created_at->formatLocalized('%d %B %Y')}}</td>
										<td>
											<div class="row">
												<a class="btn btn-secondary" href="#"><i class="dw dw-eye"></i></a>
												<a class="btn btn-warning" href="{{route('admin.blog.edit', $blog)}}"><i class="dw dw-edit2"></i></a>
												<form action="{{ route('admin.blog.destroy',$blog) }}" 		method="POST">
														@csrf
														@method('DELETE')
														<button class="btn btn-danger" id="sa-title" type="submit"><i class="dw dw-delete-3"></i></button>

												</form>
                                    		</div>
                                		</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						<div class="d-flex justify-content-center">
                        	{{ $blogs->links() }}
                    	</div>
					</div>
				</div>
@endsection
