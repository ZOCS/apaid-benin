@extends('layouts.navadmin')

@section('content')

        <div class="min-height-200px">
            @if(session()->has('message'))
                <div class="alert alert-success center">
                    {{ session()->get('message') }}
                </div>
            @endif
            <div class="card-box mb-40">
                <div class="pd-20">
                    <h4 class="text-blue h4">La liste des organismes</h4>
                </div>
                <div class="pb-20">
                    <table class="data-table table hover multiple-select-row nowrap">
                        <thead>
                            <tr>
                                <th class="table-plus datatable-nosort">Logo</th>
                                <th>Nom</th>
                                <th>Responsable</th>
                                <th>Status</th>
                                <th>Domaine</th>
                                <th class="datatable-nosort">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $organismes as $organisme )
                            <tr>
                                <td class="table-plus">
                                    <img src="{{ asset('storage/' . $organisme->logo) }}" width="70" height="70" alt="">
                                </td>
                                <td>
                                    {{ $organisme->nom }}
                                </td>
                                <td>{{ $organisme->nom_responsable }}</td>
                                <td>{{ $organisme->status }}</td>
                                <td>{{ $organisme->domaine }}</td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="dw dw-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                            <a class="dropdown-item" href="{{route('admin.editOrganisme', Crypt::encrypt($organisme))}}"><i class="dw dw-edit2"></i> Modifier</a>
                                            <form action="{{ route('admin.destroyOrganisme', Crypt::encrypt($organisme)) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="dropdown-item" id="sa-title" type="submit"><i class="dw dw-delete-3"></i> Supprimer</button>

                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                            
                        </tbody>
                        
                    </table>
                
                    <div class="d-flex justify-content-center">
                        {{ $organismes->links() }}
                    </div>
                </div>

            </div>
        </div>
             
@endsection