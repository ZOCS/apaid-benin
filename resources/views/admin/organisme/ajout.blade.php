@extends('layouts.navadmin')


@section('content')
        @if(session()->has('message'))
            <div class="row pull-right">    
            <div class="alert alert-success alert-dismissible fade show col-md-4" role="alert">
                <strong>Holy guacamole!</strong> {{ session()->get('message') }}.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
        </div>
        @endif
		@if(count($errors) > 0))
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						<li>
							{{ $error }}
						</li>
					@endforeach
				</ul>
			</div>
		@endif
        
                <!-- Wizard form -->
                <div class="pd-20 card-box mb-30">
					<div class="clearfix">
						<div class="pull-left">
							<h4 class="text-blue h4">Produit</h4>
							<p class="mb-30">Enregistrement de produit</p>
						</div>
						
					</div>
					<div class="wizard-content">
						<form class="tab-wizard wizard-circle wizard" method="post" action="{{route('admin.storeOrganisme')}}" enctype="multipart/form-data">
						
							@csrf
							<h5>Organisme</h5>
							<section>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label >Nom organismes *</label>
											<input type="text" name="nom" placeholder="Nom du organisme" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label >Nom du responsable *</label>
											<input type="text" class="form-control" name="responsable"  placeholder="Le nom du responsable" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>{{__('Domaine d\'intervention* :')}}</label>
											<input type="text" class="form-control" name="domaine"  type="text" placeholder="Domaine d'intervention" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Lien du site* :</label>
											<input  class="form-control" name="lien"  type="text" >
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Categorie Organisme* :</label>
											<select class="custom-select form-control" style="width: 100%;" name="status" required>
                                                <option>Selectionnez le status</option>
												<option value="ong" >Ong</option>
												<option value="structure" >Structure</option>
												<option value="entreprise" >Entreprise</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label >Logo organisme :</label>
											<div class="custom-file">
												<input class="custom-file-input" type="file" name="logo"  required>
												<label class="custom-file-label">Choisir une image</label>
                                                {{-- <img class="col-sm-4" src="{{ asset('storage/' . $organisme->logo) ? : '' }}" alt="Image organisme" style="width:120px; height:100px;"> --}}
												
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Adresse organisme* :</label>
											<input  class="form-control" name="adresse"  type="text" placeholder="Adresse de l'organisme" required>
										</div>
									</div>
								</div>
							</section>
							<!-- Step 2 -->
							<h5>Description organisme</h5>
							<section>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Description :</label>
											<textarea class="summernote form-control border-radius-5" name="description" placeholder="Entrez la description" required></textarea>
										</div>
									</div>
								</div>
							</section>
							<!-- Step 3 -->
							
							<div class="row align-center d-flex">
								<div class="col-sm-6 col-md-4">
									<button type="reset" class="btn btn-danger">Effacer</button>
								</div>
								<div class="col-sm-6 col-md-6">
									<button type="submit" class="btn btn-primary">Enregistrer</button>
								</div>
							</div>
						</form>
					</div>
				</div>

            </div>
            
        </div>
@endsection