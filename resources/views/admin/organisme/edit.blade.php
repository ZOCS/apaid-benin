@extends('layouts.navadmin')


@section('content')
        @if(session()->has('message'))
            <div class="alert alert-success center">
                {{ session()->get('message') }}
            </div>
        @endif

        <div class="min-height-300px">
			 <!-- Wizard form -->
                <div class="pd-20 card-box mb-30">
					<div class="clearfix">
						<div class="pull-left">
							<h4 class="text-blue h4">Organisme</h4>
							<p class="mb-30">Enregistrement de organisme</p>
						</div>
					</div>
					
					<div class="wizard-content">
						<form class="tab-wizard wizard-circle wizard" method="post" action="{{route('admin.updateOrganisme', Crypt::encrypt($organisme))}}" enctype="multipart/form-data">
							@method('put')
							@csrf
							<h5>Organisme</h5>
							<section>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label >Nom organismes *</label>
											<input type="text" name="nom" placeholder="Nom du organisme" value="{{ $organisme->nom ? : '' }}" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label >Nom du responsable *</label>
											<input type="text" class="form-control" name="responsable" value="{{ $organisme->nom_responsable ? : '' }}" placeholder="Le nom du responsable" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>{{__('Domaine d\'intervention* :')}}</label>
											<input type="text" class="form-control" name="domaine" value="{{ $organisme->domaine ? : '' }}"  type="text" placeholder="Domaine d'intervention" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Lien du site* :</label>
											<input  class="form-control" name="lien"  type="text" >
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Categorie Organisme* :</label>
											<select class="custom-select2 form-control" multiple="multiple" style="width: 100%;" name="status" required>
                                                <option>Selectionnez le status</option>
												<option value="ong" {{ old('status', isset($organisme->status) == "ong" ? 'selected' : '') ? 'selected' : '' }}>Ong</option>
												<option value="structure" {{ old('status', isset($organisme->status) == "structure" ? 'selected' : '') ? 'selected' : ''  }}>Structure</option>
												<option value="entreprise" {{ old('status', isset($organisme->status) == "entreprise" ? 'selected' : '') ? 'selected' : ''  }}>Entreprise</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label >Logo organisme :</label>
											<div class="custom-file">
												<input class="custom-file-input" type="file" name="logo"  required>
												<label class="custom-file-label">Choisir une image</label>
                                                <img class="col-sm-4" src="{{ asset('storage/' . $organisme->logo) ? : '' }}" alt="Image organisme" style="width:120px; height:100px;">
												
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Adresse organisme* :</label>
											<input  class="form-control" value="{{ $organisme->adresse ? : ''}}" name="adresse"  type="text" placeholder="Adresse de l'organisme" required>
										</div>
									</div>
								</div>
							</section>
							<!-- Step 2 -->
							<h5>Description du prroduit</h5>
							<section>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Description :</label>
											<textarea class="summernote form-control border-radius-5" name="description" placeholder="Entrez la description" required>{{ $organisme->description ? : '' }}</textarea>
										</div>
									</div>
								</div>
							</section>
							<!-- Step 3 -->
							
							<div class="row align-center d-flex">
								<div class="col-sm-6 col-md-4">
									<button type="reset" class="btn btn-danger">Effacer</button>
								</div>
								<div class="col-sm-6 col-md-6">
									<button type="submit" class="btn btn-primary">Enregistrer</button>
								</div>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
@endsection