@extends('layouts.navadmin')


@section('content')
@if(session()->has('message'))
<div class="row pull-right">
    <div class="alert alert-success alert-dismissible fade show col-md-4" role="alert">
        <strong>Holy guacamole!</strong> {{ session()->get('message') }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
    </div>
</div>
@endif
@if(count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>
            {{ $error }}
        </li>
        @endforeach
    </ul>
</div>
@endif
{!! Toastr::message() !!}
<!-- Wizard form -->
<div class="pd-20 card-box mb-30">
    <div class="clearfix">
        <div class="pull-left">
            <h4 class="text-blue h4">Service</h4>
            <p class="mb-30">Ajouter service</p>
        </div>
    </div>

    <div class="wizard-content">
        <form class="tab-wizard wizard-circle wizard" method="post" action="{{route('admin.services.store')}}"
            enctype="multipart/form-data">
            @csrf
            <h5>Service</h5>
            <section>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Titre du service *</label>
                            <input type="text" name="titre" type="text" placeholder="Nom du service" class="form-control"
                                required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Icon du service :</label>
                            <div class="custom-file">
                                <input class="custom-file-input" type="file" name="icon" required>
                                <label class="custom-file-label">Choisir icon</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Slug du service</label>
                            <input type="text" class="form-control" name="prix_promo" type="number"
                                placeholder="Le prix d promotion" required>
                        </div>
                    </div>
                </div>
                
                
            </section>
            <!-- Step 2 -->
            <h5>Description du service</h5>
            <section>
                <div class="row">
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description :</label>
                            <textarea class="summernote form-control border-radius-5" name="description"
                                placeholder="Entrez la description" required></textarea>
                        </div>
                    </div>
                </div>
            </section>
            
            <div class="row align-center d-flex">
                <div class="col-sm-6 col-md-4">
                    <button type="reset" class="btn btn-danger">Effacer</button>
                </div>
                <div class="col-sm-6 col-md-6">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>

 
</div>
@endsection