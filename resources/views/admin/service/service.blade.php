@extends('layouts.navadmin')

@section('content')

<div class="min-height-200px">
    @if(session()->has('message'))
    <div class="alert alert-success center">
        {{ session()->get('message') }}
    </div>
    @endif
    <div class="card-box mb-40">
        <div class="pd-20">
            <h4 class="text-blue h4">La liste des services</h4>
        </div>
        <div class="pb-20">
            <table class="checkbox-datatable table  data-table-export nowrap">
                <thead>
                    <tr>
                        <th>
                            <div class="dt-checkbox">
                                <input type="checkbox" name="select_all" value="" id="example-select-all">
                                <span class="dt-checkbox-label"></span>
                            </div>
                        </th>
                        <th>Icon</th>
                        <th>Titre</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $services as $service )
                    <tr>
                        <td>
                            <div class="dt-checkbox">
                                <input type="checkbox" name="select_all" value="{{ $service->id }}" id="example-select-all">
                                <span class="dt-checkbox-label"></span>
                            </div>
                        </td>
                        <td>
                            <img src="{{asset('storage/'. $service->icon) }}" style="width: 2.5rem; height: 2.5rem;">
                        </td>
                        <td>{{ $service->titre }}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <a class="btn btn-warning" role="button" href="{{route('admin.services.edit', $service)}}"><i
                                            class="dw dw-edit2"></i></a>
                                </div>
                                <div class="col-md-4">
                                    <form action="{{ route('admin.services.destroy', $service) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" id="sa-title" type="submit"><i class="dw dw-delete-3"></i></button>
                            
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                </tbody>

            </table>
        </div>

    </div>
</div>

@endsection