@extends('layouts.navadmin')

@section('content')

        <div class="min-height-200px">
            @if(session()->has('message'))
                <div class="alert alert-success center">
                    {{ session()->get('message') }}
                </div>
            @endif
            <div class="card-box mb-40">
                <div class="pd-20">
                    <h4 class="text-blue h4">La liste des produits</h4>
                </div>
                <div class="pb-20">
                    <table class="checkbox-datatable table  data-table-export nowrap">
                        <thead>
                            <tr>
                                <th>
                                    <div class="dt-checkbox">
                                        <input type="checkbox" name="select_all" value="1" id="example-select-all">
                                        <span class="dt-checkbox-label"></span>
                                    </div>
                                </th>
                                <th>Image</th>
                                <th>Nom</th>
                                {{-- <th>status</th> --}}
                                <th>Prix</th>
                                <th>Categorie</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $produits as $produit )
                            <tr>
                                <td><div class="dt-checkbox">
                                    <input type="checkbox" name="select_all" value="{{ $produit->id }}" id="example-select-all">
                                    <span class="dt-checkbox-label"></span>
                                </div>
                                <td>
                                    <img src="{{ asset('storage/' . $produit->image) }}" style="width: 2.5rem; height: 2.5rem;" alt="">
                                </td>
                                <td>
                                    {{ $produit->nom }}
                                </td>
                                {{-- <td>{{ $produit->status }}</td> --}}
                                <td>{{ $produit->prix }}</td>
                                <td>
                                    @foreach($produit->categories as $category)
                                        {{ $category->nom }}
                                    @endforeach
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a class="btn btn-warning" role="button" href="{{route('admin.edit-product', $produit)}}"><i class="dw dw-edit2"></i></a>
                                        </div>
                                        <div class="col-md-4">
                                            <form action="{{ route('admin.destroy-product', $produit) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger" id="sa-title" type="submit"><i class="dw dw-delete-3"></i></button>
                                            
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                            
                        </tbody>
                        
                    </table>
                
                    {{-- <div class="d-flex justify-content-center">
                        {{ $produits->links() }}
                    </div> --}}
                </div>

            </div>
        </div>
             
@endsection