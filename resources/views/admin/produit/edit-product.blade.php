@extends('layouts.navadmin')


@section('content')
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
        @if(session()->has('message'))
            <div class="alert alert-success center">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="min-height-300px">
			 <!-- Wizard form -->
                <div class="pd-20 card-box mb-30">
					<div class="clearfix">
						<div class="pull-left">
							<h4 class="text-blue h4">Produit</h4>
							<p class="mb-30">Enregistrement de produit</p>
						</div>
						<div class="pull-right">
							<a href="#" class="btn btn-primary btn-sm scroll-click" rel="content-y" data-toggle="modal" data-target="#bd-example-modal-lg" type="button" role="button"> Ajouter Categorie</a>
						</div>
					</div>
					
					<div class="wizard-content">
						<form class="tab-wizard wizard-circle wizard" method="post" action="{{route('admin.update-product', $produit)}}" enctype="multipart/form-data">
							@method('put')
							@csrf
							<h5>Produit</h5>
							<section>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label >Nom du produit *</label>
											<input type="text" name="nom"  type="text" placeholder="Nom du produit" value="{{ $produit->nom ? : '' }}" class="form-control" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label >Prix du produit *</label>
											<input type="text" class="form-control" name="prix"  type="number" value="{{ $produit->prix ? : '' }}" placeholder="Le prix du produit" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Prix promo :*</label>
											<input type="text" class="form-control" name="prix_promo" value="{{ $produit->prix_promo ? : '' }}"  type="number" placeholder="Le prix d promotion" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Quantité du Produit* :</label>
											<input  class="form-control" name="stock" value="{{ $produit->stock ? : '' }}"  type="number" placeholder="La quantite" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Categorie Produit* :</label>
											<select class="custom-select2 form-control" multiple="multiple" style="width: 100%;" name="categorie[]" required>
                                                @foreach($categories as $categorie)
                                                <option value="{{ $categorie->id  }}" {{ old('categorie[]', isset($produit) ? (in_array($categorie->id, $produit->categories->pluck('id')->toArray()) ? 'selected' : '') : '') }}>{{ $categorie->nom  }}</option>
                                                @endforeach
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label >Choisir une image :</label>
											<div class="custom-file">
												<input class="custom-file-input" type="file" name="image"  required>
												<label class="custom-file-label">Choisir une image</label>
                                                <img class="col-sm-4" src="{{ asset('storage/' . $produit->image) ? : '' }}" alt="Image produit" style="width:120px; height:100px;">
												
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label >Galerie images :</label>
											<div class="custom-file">
												<input class="custom-file-input" type="file" name="images[]"  required multiple>
												<label class="custom-file-label">Choisir des images</label>
											</div>
										</div>
									</div>
								</div>
							</section>
							<!-- Step 2 -->
							<h5>Description du prroduit</h5>
							<section>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label> Details :</label>
											<textarea class="summernote form-control border-radius-5" name="details" placeholder="Entrez les details concernant le produit" required>{!!  $produit->detail ? : '' !!}</textarea>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>Description :</label>
											<textarea class="summernote form-control border-radius-5" name="description" placeholder="Entrez la description" required>{{ $produit->description ? : '' }}</textarea>
										</div>
									</div>
								</div>
							</section>
							<!-- Step 3 -->
							<h5>Status</h5>
							<section>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<div class="custom-control custom-checkbox mb-5">
												<input type="checkbox" name="featured" value="1" class="custom-control-input" id="featured" {{ old('featured', isset($produit->featured) ? 'checked="checked"' : '') ? 'checked="checked"' : '' }}/>
												<label class="custom-control-label" for="featured">Produit en vedette</label>
											</div>
										</div>
										<div class="form-group">
											<div class="custom-control custom-checkbox mb-5">
												<input type="checkbox" name="popular" value="1" {{ old('popular', isset($produit->popular) ? 'checked="checked"' : '') ? 'checked' : '' }} class="custom-control-input" id="popular">
												<label class="custom-control-label" for="popular">Produit populaire</label>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="custom-control custom-checkbox mb-5">
												<input type="checkbox" name="new_arrival" value="1" {{ old('new_arrival', isset($produit->new_arrival) ? 'checked="checked"' : '') ? 'checked' : '' }} class="custom-control-input" id="new_arrival">
												<label class="custom-control-label" for="new_arrival">Nouveau arrivé</label>
											</div>
										</div>
										<div class="form-group">
											<div class="custom-control custom-checkbox mb-5">
												<input type="checkbox" name="promo" value="1" {{ old('promo', isset($produit->promo) ? 'checked="checked"' : '') ? 'checked' : '' }} class="custom-control-input" id="promo">
												<label class="custom-control-label" for="promo">Produit en Promotion</label>
											</div>
										</div>
									</div>
								</div>
							</section>
							<!-- Step 4 -->
							<h5>Autres infos</h5>
							<section>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Taux promo :</label>
											<input type="text" class="form-control" name="taux_promo" placeholder="Pourcentage de reduction" value="{{ $produit->taux_promo ? : '' }}">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Auteur du Produit</label>
											<input type="text" class="form-control" name="auteur"   placeholder="Nom de l'auteur" value="{{ $produit->auteur ? : '' }}" required>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Status</label>
											<select class="form-control" name="status" required>
												<option>Selectionnez le status</option>
												<option value="Actif" {{ old('promo', isset($produit->status) == "Actif" ? 'selected' : '') ? 'selected' : '' }}>Actif</option>
												<option value="Non Actif" {{ old('promo', isset($produit->status) == " Non  Actif" ? 'selected' : '') ? 'selected' : ''  }}>Non Actif</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Url Produit</label>
											<input type="text" class="form-control" name="slug"   placeholder="Url produit" value="{{ $produit->slug ? : '' }}" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>{{__('L\'unite de mesure')}}</label>
											<select class="form-control" name="mesure_id" required>
												<option>Selectionnez la mesure</option>
												@foreach($mesures as $mesure)
													<option value="{{$mesure->id}}" @if($mesure->id == $produit->mesure->id) 'selected' @else '' @endif >{{$mesure->title}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</section>
							<div class="row align-center d-flex">
								<div class="col-sm-6 col-md-4">
									<button type="reset" class="btn btn-danger">Effacer</button>
								</div>
								<div class="col-sm-6 col-md-6">
									<button type="submit" class="btn btn-primary">Enregistrer</button>
								</div>
							</div>
						</form>
					</div>
				</div>

            </div>
            <div class="modal fade bs-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Insertion Categorie</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <form class="form" method="post" action="{{route('categorie.store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-2 col-form-label">Nom categorie</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control" name="nom" type="text" placeholder="Agriculture" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-2 col-form-label">Slug</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control" name="slug" type="text" placeholder="Le slug">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-2 col-form-label">Categorie Parent</label>
                                    <div class="col-sm-12 col-md-10">
                                        <select class="custom-select2 form-control" multiple="multiple" name="parent_id[]" >
                                            @foreach($categories as $categorie)
                                            <option value="{{ $categorie->id  }}">{{ $categorie->nom  }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row align-center">
                                    <div class="col-sm-6 col-md-4">
                                        <button type="reset" class="btn btn-danger">Effacer</button>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>  							
</div>
@endsection