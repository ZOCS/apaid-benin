function addWish(id)
{
    $.ajaxSetup({
		'headers': {
		    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
			"Content-Type": "application/json",
            "Accept": "application/json, text-plain, */*",
            "X-Requested-With": "XMLHttpRequest",
		}
	});
    $.ajax({
        url: "/AddWhislist",
        method: "GET",
        data: {id:id },
        success:function(response)
        {
            // $('#result').html(response);
            location.reload();
        }
    })
}

function addCart(id)
{
    $.ajaxSetup({
		'headers': {
		    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
			"Content-Type": "application/json",
            "Accept": "application/json, text-plain, */*",
            "X-Requested-With": "XMLHttpRequest",
		}
	});
    $.ajax({
        url:"/cart.store",
        method: "GET",
        data: {id:id },
        success:function(response)
        {
            // $('#result').html(response);
            location.reload();
        }
    })
}

//Premiere Fonction de filtrage des produits avec checkbox
function functOne() 
{
    var check = document.getElementById("c1");
    if (check.checked == true) {
        // alert("OK")
        var min_price = 1000;
		var max_price = 5000;
		
        $.ajax({
            type: "GET",
            url: "/filter_by_price",
            data: { min_price: min_price, max_price: max_price, },
            dataType: 'html',

            success: function (response) {
                // $('#searchResults').show('show');
                console.log(response);
                // $(#myDiv).load(location.href+ #myDiv>*,);
                $("#searchResults").html(response);

            },
        });
    } else {
        alert("s'il vous plait, pas de produit trouves!")
    }
}

//Deuxieme Fonction de filtrage des produits avec checkbox

function functTwo() 
{
    var check = document.getElementById("c2");
    if (check.checked == true) {
        // alert("OK")
        var min_price = 5000;
		var max_price = 10000;
					
		$.ajax({
			type: "GET",
			url: "/filter_by_price",
			data: { min_price: min_price, max_price: max_price, },
			dataType: 'html',

			success: function (response) {
			// $('#searchResults').show('show');
				console.log(response);
			// $(#myDiv).load(location.href+ #myDiv>*,);
		    $("#searchResults").html(response);

			},
	    });
    } else {
        alert("s'il vous plait, pas de produit trouves!")
    }
}

//Troisieme Fonction de filtrage des produits avec checkbox
function functThree() 
{
    var check = document.getElementById("c3");
    if (check.checked == true) {
        // alert("OK")
        var min_price = 10000;
		var max_price = 20000;
			
		$.ajax({
		    type: "GET",
            url: "/filter_by_price",
            data: { min_price: min_price, max_price: max_price, },
            dataType: 'html',
            success: function (response) {
                // $('#searchResults').show('show');
                console.log(response);
                // $(#myDiv).load(location.href+ #myDiv>*,);
                $("#searchResults").html(response);

            },
    });
    } else {
        alert("Désolé, pas de produit trouves!")
    }
}

function sorting()
{
    var sortby = $('#orderby').val();

    $.ajax({
        type: "GET",
        url: "/sorting",
        data: {orderby: sortby},
        success: function (data) {
            $("#searchResults").html(data);
            console.log(data)
        }
    })
    
}

function perPage()
{
    var perPage = $('#perPage').val();

    $.ajax({
        type: "GET",
        url: "/perpage",
        data: { perPage: perPage },
        success: function (data) {
            $("#searchResults").html(data);
            console.log(data)
        }
    })
}

