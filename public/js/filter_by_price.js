function addWish(id)
{
    $.ajaxSetup({
		'headers': {
		    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
			"Content-Type": "application/json",
            "Accept": "application/json, text-plain, */*",
            "X-Requested-With": "XMLHttpRequest",
		}
	});
    $.ajax({
        url: "/AddWhislist",
        method: "GET",
        data: {id:id },
        success:function(response)
        {
            // $('#result').html(response);
            location.reload();
        }
    })
}

function addCart(id)
{
    $.ajaxSetup({
		'headers': {
		    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
			"Content-Type": "application/json",
            "Accept": "application/json, text-plain, */*",
            "X-Requested-With": "XMLHttpRequest",
		}
	});
    $.ajax({
        url:"/cart.store",
        method: "GET",
        data: {id:id },
        success:function(response)
        {
            // $('#result').html(response);
            location.reload();
        }
    })
}

