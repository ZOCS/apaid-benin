var selects = document.querySelectorAll('#sort');
Array.from(selects).forEach((element) => {
    element.addEventListener('change', function (){
        var rowId = this.getAttribute('data-id');
        var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        fetch(`/sort`,
            {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json, text-plain, */*",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-TOKEN": token
                },
                method: 'PATCH',
                body: JSON.stringify({
                    qty: this.value
                }) 
            }
        ).then((data) => {
            console.log(data);
            location.reload();
        }).catch((error) => {
            console.log(error);
        })
    });
});  
































