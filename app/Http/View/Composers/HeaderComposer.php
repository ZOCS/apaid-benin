<?php

namespace App\Http\View\Composers;

use App\Models\Blog;
use App\Repositories\UserRepository;
use Illuminate\View\View;
use App\Models\Categorie;
use App\Models\Organisme;

class HeaderComposer
{

    
    public function compose(View $view){
        $view->with(['categories' => Categorie::all(), 'lastest_blogs' => Blog::orderby('created_at', 'desc')->take(3)->get(), 'organismes' => Organisme::take(3)->get()]);
    }
}