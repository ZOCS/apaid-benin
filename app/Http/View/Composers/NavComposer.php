<?php

namespace App\Http\View\Composers;

use App\Repositories\UserRepository;
use Illuminate\View\View;
use App\Models\Categorie;
use App\Models\Produit;
use App\Models\User;

class NavComposer
{

    
    public function compose(View $view){
        $view->with(['categ'=> Categorie::whereNull('parent_id')->with('children')->get(), 'tendance' => Categorie::whereNull('parent_id')->with('children')->take(4)->get(), 'admins' => User::where('role', 'admin')->get(), 'promos' => Produit::where('promo', true)->get(), 'populars' => Produit::where('popular', true)->get(), 'featureds' => Produit::where('featured', true)->get(), 'new_arrivals' => Produit::where('new_arrival', true)->get(), 'produit_acs' => Produit::all()->take(8)]);
    }
}