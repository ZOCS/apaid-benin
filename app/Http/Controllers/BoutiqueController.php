<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;


class BoutiqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Cart::content());
        if(request()->categorie){
            $produits = Produit::with('categories')->whereHas('categories', function ($query){
                $query->where('slug', request()->categorie);
            })->paginate(3);
            
            // $produit_promos = Produit::with('categories')->where('promo', '1')->paginate(4);
            // $produit_latest = Produit::latest('id')->take(4);

        }else{
            $produits = Produit::with('categories')->paginate(3);
            $produit_promos = Produit::with('categories')->where('promo', '1')->paginate(4);
            $produit_latests = Produit::orderBy('created_at', 'desc')->get();

        }
        // $produits = Produit::with('categories')->orderBy('created_at', 'desc')->paginate(9);
        $produit_promos = Produit::with('categories')->where('promo', '1')->paginate(4);
        $produit_latests = Produit::orderBy('created_at', 'desc')->get();
        return view('Boutique.shop', compact('produits', 'produit_promos', 'produit_latests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $id;
    }

    public function whislist()
    {
        return view("Boutique.wishlist");
    }

    public function filter_by_price(Request $request)
    {
        if ($request->ajax() && isset($request->min_price) && isset($request->max_price)) {
            $min = $request->min_price;
            $max = $request->max_price;
            $produits = Produit::whereBetween('prix', [(int) $min, (int) $max])->get();
            // dd($produits);
            response()->json($produits);
            return view('Boutique.product', compact('produits'));

            // return view('shop.magasin', compact('products'));
        } else {
            $produits = Produit::paginate(3);
            return response()->json($produits);
        }
    }

    public function sortby(Request $request)
    {
        // return response()->json('Ca marche');
        // dd($request->sort);
        if($request->selected == "new"){
            $produits = Produit::orderBy('created_at', 'desc')->where('new_arrival', '1')->get();
        }
        // $produits = Produit::with('categories')->orderBy('created_at', 'desc')->paginate(9);
        $produit_promos = Produit::with('categories')->where('promo', '1')->paginate(4);
        $produit_latests = Produit::orderBy('created_at', 'desc')->get();
        return view('Boutique.shop', ['produits' => $produits, 'produit_promos' => $produit_promos, 'produit_latests' => $produit_latests]);
    }
}
