<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\models\Groupe;
use App\Models\Projet;
use App\Models\Categorie;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Crypt;

class ProjetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs_recent = Blog::all()->take(3)->sortBy('created_at');
        $projets = Projet::paginate('6');
        return view('pages.projet.projet', compact('blogs_recent', 'projets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groupes = Categorie::all();
        return view('admin.projet.insert-projet', compact('groupes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg',
            'auteur' => 'required',
            'description' => 'required',
        ]);

        $data = array(
        	'title'=> $request->title,
            'image'=> $request->file('image')->store('projets', 'public'),
        	'auteur'=> $request->auteur,
            'description'=> $request->description,
        );
        $offre = Projet::create($data);
        $offre->categories()->attach($request->categorie);
            Toastr::success('L\'enregistrement a reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);
       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($projet)
    {      
        $projet = Crypt::decrypt($projet);
        $blogs = Blog::all()->take(3)->sortBy('created_at');
        return view('pages.projet.detail', compact('projet', 'blogs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $projet= Crypt::decrypt($id);

        return view('admin.projet.edit', compact('projet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $projet = Crypt::decrypt($id);
        
            $projet->title = $request->title;
            $projet->image = $request->file('image')->store('projets', 'public');
            $projet->auteur = $request->auteur;
            $projet->description = $request->description;
            $projet->status = $request->status;
            $projet->update();
           Toastr::success('Le projet a ete mise a jour.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

        return redirect('admin.list-projet');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $projet = Crypt::decrypt($id);
       $projet->delete();
       Toastr::success('Le projet a été bien supprimée.!!!', 'Notification', ["positionClass" => "toast-top-right"]);
       return redirect('admin.list-projet');

    }

    public function search(){
        request()->validate([
            'search' => 'required|min:3'
        ]);
        $q = request()->input('search');
        $blogs = Blog::all()->take(3)->sortBy('created_at');

        //dd($q);
       $projets = Projet::where('title', 'like', "%$q%")->orWhere('description', 'like', "%$q%")->paginate(4);
        return view('pages.projet.projet', compact('blogs'))->with('projets', $projets);
    }
}
