<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use Brian2694\Toastr\Facades\Toastr;
use App\Models\Categorie;
use Illuminate\Support\Facades\Crypt;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::paginate(10);

        return view('admin.blog.blog-list', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg',
            'auteur' => 'required|min:2',
            'description' => 'required|min:255'
        ]);
        $data = array(
            'title' => $request->title,
            'image' => $request->file('image')->store('blogs', 'public'),
            'auteur' => $request->auteur,
            'description' => $request->description,
        );
        $prod = Blog::create($data);
        $prod->categories()->attach($request->categorie);
        Toastr::success('L\'enregistrement a reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($blog)
    {
        $blog = Crypt::decrypt($blog);
        //dd($produit);

        return view('admin.blog.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $blog = Crypt::decrypt($blog);
        $categories = Categorie::all();
        $blog = Blog::find($id);
        // dd($blog);
        return view('admin.blog.edit', compact('blog', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blog::find($id);

        $blog->title = $request->title;
        $blog->image = $request->file('image')->store('blogs', 'public');
        $blog->status = $request->status;
        $blog->auteur = $request->auteur;
        $blog->description = $request->description;

        $blog->update();
        $blog->categories()->sync($request->categorie);
        $blogs = Blog::with('categories')->paginate(6);
        Toastr::success('L\'enregistrement a reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

        return redirect('admin.blog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::destroy($id);
        Toastr::success('Le blog a ete supprime avec succes.', 'Notification', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }

    public function blog()
    {
        return view('pages.blog');
    }
}
