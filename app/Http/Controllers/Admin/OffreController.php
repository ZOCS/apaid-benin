<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Offre;
use App\Models\Candidature;
use App\Models\Categorie;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class OffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offres = Offre::paginate(10);
       return view('admin.offre.offre', compact('offres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categorie::all();
        return view('admin.offre.ajout', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg',
            'entreprise' => 'required',
            'poste' => 'required',
            'expiration' => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);
        $date = Carbon::now()->format('d-m-Y');
        $uniq = mt_rand(1000, 9999);
        $code = 'OFR_' . $date . '_' . $uniq;
        $data = array(
            'reference'=> $code,
        	'title'=> $request->title,
        	'entreprise'=> $request->entreprise,
            'image'=> $request->file('image')->store('offres', 'public'),
            'status' => $request->status,
        	'poste'=> $request->poste,
        	'expiration'=> $request->expiration,
            'description'=> $request->description,
        );
        $offre = Offre::create($data);
        $offre->categories()->attach($request->categorie);
        Toastr::success('L\'enregistrement a reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $offre = Offre::find($id);
        //dd($produit);

        return view('admin.offre.show', compact('offre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offre = Offre::find($id);
        return view('admin.offre.edit', compact('offre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $offre)
    {
        $off = Offre::find($offre);
        $off->title = $request->title;
        $off->entreprise = $request->entreprise;
        $off->image =  $request->file('image')->store('offres', 'public');
        $off->expiration =  $request->expiration;
        $off->description =  $request->description;
        $off->update();
        $off->categories()->sync($request->categorie);
        Toastr::success('L\'enregistrement a reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

       return redirect('admin.offre.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Crypt::decrypt($id);
        //dd($produit);

        return redirect('admin.list-offre')->with('success', );
    }

    public function candidature(Request $request){

        $data  = array(
            'nom_user' => Auth::user()->nom,
            'id_offre' => $request->id_offre,
        );

        Candidature::create($data);
        User::attach($request->offre);

    }
}
