<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contact(){
        return view('pages.contact');
    }

    public function store(Request $request)
    {
            $name = $request->get('name');
		    $email = $request->get('email');
			$subject = $request->get('subject');
			$message = $request->get('message');

            $details = [
                'name' => $name,
                'email' => $email,
                'subject' => $subject,
                'message' => $message
            ];

            
            Mail::to('contact.business@cornercarde.store')->send(new ContactMail($details));
		
            //Flashy::success('Votre message a ete envoye avec succes!', 'http://www.easyma.com');
            // flashy('Votre message a ete envoye avec succes!');
       Toastr::success('Votre message a ete envoye avec success.', 'Notification', ["positionClass" => "toast-top-right"]);
        
		return redirect()->back();
    }

    public function cgu() {
        return view('pages.cgu');
    }
}
