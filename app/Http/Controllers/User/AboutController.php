<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\AdhesionMail;
use App\Models\Adhesion;
use App\Models\Categorie;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AboutController extends Controller
{
    public function about(){
        return view('pages.about');
    }

    public function adhesion(){
        return view('pages.adhesion');
    }

    public function preStore(Request $request) {

        $data = [
            'nom' => $request->nom,
            'responsable' => $request->responsable,
            'domaine' => $request->domaine,
            'pays' => $request->pays,
            'ville' => $request->ville,
            'demande' => $request->file('demande')->store('demande', 'public'),
            'objectifs' => $request->objectif,
            'realisation' => $request->realisation,
            'email' => $request->email,
            'phone' => $request->phone,
       ];
        Validator::make($data, [
            'nom' => ['required', 'string', 'max:255'],
            'responsable' => ['required', 'string', 'max:255'],
            'pays' => ['required', 'string', 'max:255'],
            'ville' => ['required', 'string', 'max:100'],
            'domaine' => ['required', 'string', 'max:255'],
            'pays' => ['required', 'string', 'max:255'],
            'realisation' => ['required',],
            'demande' => ['required', 'mimes:doc,pdf,docx', 'max:10000'],
            'objectifs' => ['required', 'string'],
        ]);

        $res = Adhesion::create($data);
        $res->save();

        Mail::to('zoschrispuce@gmail.com')->send(new AdhesionMail($data));

        Toastr::success('Votre demande à été envoyé', 'Notification', ["positionClass" => "toast-top-right"]);

        return \redirect()->back();
    }

    public function store(Request $request){

       $adhesion = new Adhesion();
       $adhesion->nom = $request->session()->get('nom');
       $adhesion->responsable = $request->session()->get('responsable');
       $adhesion->domaine = $request->session()->get('domaine');
       $adhesion->pays = $request->session()->get('pays');
       $adhesion->ville = $request->session()->get('ville');
       $adhesion->demande = $request->session()->get('demande')->file('demande')->store('demande', 'public');
       $adhesion->objectif = $request->session()->get('objectif')->file('objectif')->store('objectif', 'public');
       $adhesion->realisation = $request->session()->get('realisation');
       Toastr::success('L\'enregistrement a reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

       return redirect('/adhesion');
    }

    public function details(){
        return view('boutique.details');
    }

    public function checkout(){
        return view('boutique.checkout');
    }

    public function cart(){
        return view('boutique.cart');
    }
}
