<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Session;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Cart::content());
        return view('boutique.cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produit = Produit::find($request->id);
       $duplicata = Cart::search(function($cartItem, $rowId) use ($produit){
            return $cartItem == $produit->id;
        });

        if($duplicata->isNotEmpty()){
            return redirect()->back()->with('success', 'Le produit a deja ete ajoute');
        }
        // $produit = Produit::find($request->id);

        Cart::instance('cart')->add($produit->id, $produit->nom, 1, $produit->prix,)->associate('App\Models\Produit');
        Toastr::success('Le produit ete bien ajoute', 'Notification', ["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }
    public function whislistAdd(Request $request)
    {
        $produit = Produit::find($request->id);
        Cart::instance('wishlist')->add($produit->id, $produit->nom, 1, $produit->prix)->associate('App\Models\Produit');
        Toastr::success('Le produit ete bien ajoute', 'Notification', ["positionClass" => "toast-top-right"]);
        response()->json();
        return redirect()->back();
    }

    public function instant()
    {
        dd(Cart::instance('wishlist')->content());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rowId)
    {
        $data = $request->json()->all();

        Validator::make($request->all(), [
            'qty' => 'required|numeric'
        ]);
        //dd($data);
        Cart::instance('cart')->update($rowId, $data['qty']);
        Toastr::info('La quantité du produit est passeé à '. $data['qty'] . '.', 'Notification', ["positionClass" => "toast-top-right"]);
        
        Session::flash('success', 'La quantité du produit est passeé à ' . $data['qty'] . '.');

        return response()->json(['success' => 'La quantité de votre panier a ete mise à jour. ']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
       Toastr::success('Le produit ete bien supprime.', 'Notification', ["positionClass" => "toast-top-right"]);
        return back(); 
    }
}
