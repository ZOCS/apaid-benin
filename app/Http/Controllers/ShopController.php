<?php

namespace App\Http\Controllers;

use App\Models\Adhesion;
use App\Models\Organisme;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function accueil(){
        
        return view('Boutique.accueil');
    }

    public function shop(){
        return view('Boutique.shop');
    }
    public function produit(){
        return view('Boutique.shop');
    }

    public function partenaire(){
        $entreprises = Organisme::where('status', 'entreprises')->paginate(6);

        return view('pages.organisme.entreprise' , compact('entreprises'));
    }

    public function organisme(){
        $structures = Organisme::where('status', 'structure')->paginate(6);

        return view('pages.organisme.structure', compact('structures'));
    }

    public function ong(){
        $ongs = Organisme::where('status', 'ong')->paginate(6);
        return view('pages.organisme.ong',compact('ongs'));
    }

    public function projet(){
        return view('pages.projet');
    }

    public function blog_single() {
        return view('pages.blog-single');
    }

    public function payment() {
        return view('pages.payment');
    }

    public function store(Request $request){
        Adhesion::create($request->all());
        return redirect('payment');
    }
}
