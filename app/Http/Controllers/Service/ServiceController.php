<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Mail\Service as MailService;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;
use Spatie\SchemaOrg\Series;

class ServiceController extends Controller
{

    /** Fonction controller pour les pages utilisateur. */
    public function services()
    {
        $services = Service::paginate(6);
        return view('pages.services.services', compact('services'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function single($id)
    {
        $services = Service::take(4)->get();
        $service = Service::findOrFail($id);
        return view('pages.services.single', compact('service', 'services'));
    }

    /** Fonction controller pour l'administration. */

    public function index()
    {
        $services = Service::paginate(10);
        return  view('admin.service.service', compact('services'));
    }

    public function create()
    {
        return view('admin.service.ajout');
    }
    public function store(Request $request)
    {
        $request->validate([
            'titre' => 'required',
            'description' => 'required',
            'icon' => 'required|mimes:pdf,docx|max:9000',
        ]);
        if ($request->slug) {
            $slug = $request->slug;
        } else {
            $slug = strtolower($request->titre);
        }
        Service::create([
            'titre' => $request->titre,
            'icon' => $request->file('icon')->store('icons', 'public'),
            'slug' => $slug,
            'description' => $request->description

        ])->save();

        Toastr::success('Sauvegarde reussie.!!!', 'Notification', ["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }

    public function edit($id)
    {
        $service = Service::find($id);
        return view('pages.services.single', compact('service'));
    }

    public function update(Request $request, $id)
    {
        $service = Service::find($id);

        $service->titre = $request->titre;
        $service->icon = $request->icon;
        $service->slug = $request->slug;
        $service->description = $request->description;

        $service->update();

        Toastr::success('Mise à jour réussit.!!!', 'Notification', ["positionClass" => "toast-top-right"]);
        return view('admin.service.service');
    }

    public function destroy($id)
    {
        $service = Service::destroy($id);

        Toastr::success('L\'enregistrement a reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }

    public function subscribeService(Request $request)
    {
        $service = Service::where('id', $request->service)->first();
        // dd($service);
        $name = $request->get('name');
        $email = $request->get('email');
        $message = $request->get('message');

        $details = [
            'name' => $name,
            'email' => $email,
            'message' => $message,
            'service' => $service->titre
        ];


        Mail::to('zoschrispuce@gmail.com')->send(new MailService($details));

        //Flashy::success('Votre message a ete envoye avec succes!', 'http://www.easyma.com');
        // flashy('Votre message a ete envoye avec succes!');
        Toastr::success('Votre message a ete envoye avec success.', 'Notification', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }
}
