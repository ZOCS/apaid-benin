<?php

namespace App\Http\Controllers;

use App\Models\Mesure;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class MesureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mesures = Mesure::all();

        return view('admin.mesure.liste', compact('mesures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.mesure.mesure');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titre' => 'required',
            'slug' => 'required'
        ]);
        $data = array(
            'titre'=> $request->titre,
        	'slug'=> $request->slug, 
        );
        $cat = Mesure::create($data);
        $cat->save();
        Toastr::success('Sauvegarde reussie.', 'Notification', ["positionClass" => "toast-top-right"]);

       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Mesure $mesure)
    {
        
        return view('admin.mesure.edit', compact('mesure'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mesure $mes)
    {
       $mesure = Mesure::find($mes);
        
        $mesure->titre = $request->title;
        $mesure->slug = $request->slug;

        $mesure->update();
        Toastr::success('La mesure a ete mise a jour.', 'Notification', ["positionClass" => "toast-top-right"]);

        return redirect('mesure.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
