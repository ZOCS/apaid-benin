<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Mail\LoginMail;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function authenticated(Request $request, $user)
    {
        // to admin dashboard
        if($user->isAdmin()) {
            Mail::to($user->email)->send(new LoginMail());
            return redirect(route('admin.dashboard'));
        }
        //to manager dashboard
        if($user->isManage()) {
            Mail::to($user->email)->send(new LoginMail());
            return redirect(route('manage.dashboard'));
        }
        //to blogger dashboard
        if($user->isBlogger()) {
            Mail::to($user->email)->send(new LoginMail());
            return redirect(route('blogger.dashboard'));
        }
        // to user dashboard
        else if($user->isUser()) {
            Mail::to($user->email)->send(new LoginMail());
            return redirect(url()->previous());
        }
        
        abort(404);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
