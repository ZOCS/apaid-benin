<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Hash;
use App\Notifications\UserNotification;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'sexe' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:20'],
            'terms' => ['required', 'string', 'max:255'],
            'pays' => ['required', 'string', 'max:255'],
            'cv' => [ 'string','mimes:pdf,doc,docx', 'max:10000'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @param \App\Models\User $user
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
       return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'sexe' => $data['sexe'],
            'phone' => $data['phone'],
            'pays' => $data['pays'],
            'terms' => $data['terms'],
            'password' => Hash::make($data['password']),
        ]);

        Mail::to($data['email'])->send(new RegisterMail());
    }

    // protected function registered(Request $request, $user)
    // {
    //     $admins = User::where('role', 'admin')-get();
    //     foreach ($admins as $admin) {
    //         $admin->notify(new );
    //     }
    // }
}
