<?php

namespace App\Http\Controllers;

use App\Mail\OrderUser;
use App\Mail\AccountUser;
use App\Models\Commande;
use App\Models\User;
use App\Notifications\CommandesNotification;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Mail;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Hash;



class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('boutique.checkout');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

    // Mettre les information de l'utilisateur en session
    public function dataUser(Request $request)
    {
        $data = array(
            'nom' => $request->lname,
            'prenom' => $request->fname,
            'email' => $request->email,
            'phone' => $request->phone,
            'adresse' => $request->adresse,
            'zip' => $request->zip,
            'pays' => $request->pays,
            'ville' => $request->ville,
            'account' => $request->account,
        );
        session($data);
        return response()->json($data);
    }
    //Traitement du Paiement
    public function checkout(Request $request)
    {
        $order = new Commande();
        $order->lname = $request->session()->get('nom');
        $order->fname = $request->session()->get('prenom');
        $order->email = $request->session()->get('email');
        $order->phone = $request->session()->get('phone');
        $order->adresse = $request->session()->get('adresse');
        $order->zip = $request->session()->get('zip');
        $order->pays = $request->session()->get('pays');
        $order->ville = $request->session()->get('ville');

        //Serialisation des produits
        $produits = [];
        $i = 0;
        foreach (Cart::instance('cart')->content() as $produit) {
            $produits['produit_' . $i]['name'] = $produit->model->nom;
            $produits['produit_' . $i]['price'] = $produit->model->prix;
            $produits['produit_' . $i]['quantity'] = $produit->qty;
            $i++;
        };

        $order->payement_id = request()->transaction_id;
        $order->prix = Cart::total();
        $order->payement_ceated_at = Date::now();
        $order->produits = serialize($produits);
        $order->save();
        $admins = User::where('role', 'admin')->get();
        foreach ($admins as $admin ) {
            $admin->notify(new CommandesNotification($order));
        }
        // $order->notify(new CommandesNotification($order));
        Mail::send(new OrderUser($order));

        // Creation de compte utilisateur si l'utlisateur n'en possede pas.
        // if($request->session()->get('account') == true)
        // {
        //     User::create([
        //         'name' => $request->session()->get('nom'),
        //         'email' => $request->session()->get('email'),
        //         'sexe' => "Masculin",
        //         'phone' => $request->session()->get('phone'),
        //         'pays' => $request->session()->get('pays'),
        //         'terms' => true,
        //         'password' => Hash::make('password'),
        //     ]);
        //     Mail::send(new AccountUser($order));

        // }

        Cart::destroy();
        Toastr::success('Votre commande a été bien envoyé !.', 'Notification', ["positionClass" => "toast-top-right"]);
        return view('Boutique.thankyou');
    }

    public function  thankyou(){
        return view('Boutique.thankyou');
    }
}
