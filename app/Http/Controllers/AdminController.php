<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\User;
use App\Models\Mesure;
use App\models\Projet;
use App\Models\Produit;
use App\Models\Categorie;
use App\Models\Commande;
use App\Models\Organisme;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Crypt;

class AdminController extends Controller
{

    //  public function __construct()
    // {
    //     $this->middleware(['auth', 'isadmin']);
    // }

    public function index(){
        $users = User::all();
        $users_count = User::count();
        $produit_count = Produit::count();
      
        return view('admin.dashbord', compact('users', 'users_count', 'produit_count'));
    }

    public function essai()
    {
        $p = Commande::pluck('id');
        // dd($p);
        $order = \App\Models\Commande::where('id', $p)->get();
        // dd($order);
        foreach ($p->notifications as $notification){
             echo $notification->data['order_fname'];

        }
        // return "ok";
    }
    public function insertProduct(){
        $produit = Produit::all();
        $mesures = Mesure::all();
        return view('admin.produit.insert-produit', compact('produit', 'mesures'));
    }

    public function listBlog(){
        
    }

    public function insertBlog(){
        return view('admin.blog.insert-blog');
    }

    public function insertProjet(){
        $groupes = Categorie::all();
        return view('admin.projet.insert-projet', compact('groupes'));
    }

    public function listProjet(){
        $groupes = Categorie::all();
        $projets = Projet::all();
        return view('admin.projet.list-projet', compact('projets'));
    }
    public function ajoutOffre(){
        $categories = Categorie::all();
        return view('admin.offre.ajout',compact('categories'));
    }

    public function storeOrganisme(Request $request){
         $data = array(
            'logo'=> $request->file('logo')->store('organismes', 'public'),
        	'nom'=> $request->nom,
        	'nom_responsable'=> $request->responsable,
            'domaine'=> $request->domaine,
        	'adresse'=> $request->adresse,
        	'status'=> $request->status,
        	'lien'=> $request->lien,
            'description'=> $request->description,
        );
        $offre = Organisme::create($data);
        $offre->save();
       Toastr::success('L\'enregistrement a reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

       return redirect()->back();
    }

    public function indexOrganisme()
    {
        $organismes = Organisme::paginate(10);
        return view('admin.organisme.organisme', compact('organismes'));
    }

     public function createOrganisme()
    {
        return view('admin.organisme.ajout');
    }

    public function editOrganisme($org)
    {
        $organisme = Crypt::decrypt($org);

        return view('admin.organisme.edit', compact('organisme'));
    }
    public function updateOrganisme(Request $request, $id)
    {
        $organisme = Crypt::decrypt($id);
        
            $organisme->nom = $request->nom;
            $organisme->nom_responsable = $request->responsable;
            $organisme->logo = $request->file('logo')->store('organisme', 'public');
            $organisme->domaine = $request->domaine;
            $organisme->adresse = $request->adresse;
            $organisme->lien = $request->lien;
            $organisme->description = $request->description;
            $organisme->status = $request->status;
            $organisme->update();
       Toastr::success('Mise a jour reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

        return redirect('admin.listOrganisme');
    }

    public function destroyOrganisme($org)
    {
        $id = Crypt::decrypt($org);
        $id->delete();
        // Organisme::destroy($id);
        Toastr::success("L\'organisme a ete supprimée", 'Notification', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }


}
