<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Blog;
use App\Models\Categorie;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Crypt;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::paginate(6);
        //  $date = Carbon::parse('22-11-2021')->locale('fr_FR')->isoFormat('LLLL');
        //  dd($date);
        return view('pages.blog.blog', compact('blogs'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.insert-blog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg',
            'auteur' => 'required|min:2',
            'description' => 'required|min:255'
        ]);
        $data = array(
            'title'=> $request->title,
            'image'=> $request->file('image')->store('blogs', 'public'),
        	'auteur'=> $request->auteur,
            'description'=> $request->description,
        );
        $prod = Blog::create($data);
        $prod->categories()->attach($request->categorie);
       Toastr::success('L\'enregistrement a reussi.!!!', 'Notification', ["positionClass" => "toast-top-right"]);

       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $blog)
    {   
       $blog = Crypt::decrypt($blog);
        //dd($produit);

        return view('admin.blog.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($blog)
    {
        $blog = Crypt::decrypt($blog);
        $categories = Categorie::all();
        $categ= Blog::find($blog->id)->categories()->get();
       // dd($categ);
        return view('admin.blog.edit', compact('blog', 'categories', 'categ'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blog::find($id);
        
        $blog->nom = $request->title;
        $blog->image = $request->file('image')->store('blogs', 'public');
        $blog->stock = $request->stock;
        $blog->auteur = $request->auteur;
        $blog->description = $request->description;

        $blog->update();
        $blog->categories()->sync($request->categorie);
        $blogs = Blog::with('categories')->paginate(6);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function blog(){
        return view('pages.blog');
    }
}
