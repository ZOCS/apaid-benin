<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;

class ShopRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
         if (Auth::guard($guard)->check()) {

            if (route('checkout')) {
                return redirect('boutique.index');
            }

            return redirect(RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}
