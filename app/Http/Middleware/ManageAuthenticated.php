<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ManageAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::check() )
        {
            // if user admin take him to his dashboard
            if ( Auth::user()->role == 'manage' ) {
                 return $next($request);
            }
            // allow user to proceed with request
            // else if ( Auth::user()->role == 'user' ) {
            //      return $next($request);
            // }
        }
        abort(404);  // for other user throw 404 error
    }
}
