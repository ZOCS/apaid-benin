<?php

namespace App\Http\Livewire;

use Livewire\Component;

class WishlistComponent extends Component
{
    public function render()
    {
        return view('livewire.wishlist-component');
    }

    public function whislistAdd($productId, $libelle, $qty, $price)
    {
        Cart::instance('wishlist')->add($productId, $libelle, $qty, $price)->associate('App\Models\Produit');
        return redirect()->back();
    }
}
