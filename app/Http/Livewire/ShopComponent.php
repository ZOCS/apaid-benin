<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ShopComponent extends Component
{

    public $min_price;
    public $max_price;
    public function render()
    {
        return view('livewire.shop-component');
    }

    public function mount(){
        $this->min_price = 1;
        $this->max_price = 20000;
    }
}
