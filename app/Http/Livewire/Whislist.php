<?php

namespace App\Http\Livewire;

use Gloudemans\Shoppingcart\Facades\Cart;
use Livewire\Component;

class Whislist extends Component
{
    public function render()
    {
        return view('livewire.whislist');
    }

    public function whislistAdd($productId, $libelle, $qty, $price)
    {
        Cart::instance('wishlist')->add($productId, $libelle, $qty, $price)->associate('App\Models\Produit');
        return redirect()->back();
    }
}
