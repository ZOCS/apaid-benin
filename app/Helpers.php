<?php 

function getPrice($price) {
    $price = floatval($price) * 1000;

    return number_format($price, 2, ',', ' ') . ' XOF';

}