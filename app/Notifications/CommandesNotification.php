<?php

namespace App\Notifications;

use App\Mail\OrderAdmin;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CommandesNotification extends Notification
{
    use Queueable;
    public $order;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new OrderAdmin($notifiable))->to('zoschrispuce@gmail.com');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'order_id' => $this->order->id,
            'order_fname' => $this->order->fname,
            'order_lname' => $this->order->lname,
            'order_email' => $this->order->email,
            'order_phone' => $this->order->phone,
            'order_adresse' => $this->order->adresse,
            'order_zip' => $this->order->zip,
            'order_pays' => $this->order->pays,
            'order_ville' => $this->order->ville,
        ];
    }
}
