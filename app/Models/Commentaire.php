<?php

namespace App\Models;


use App\Models\Blog;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    protected $guarded = [];

    public function user()  {
        return $this->belongsTo('App\Models\User');
    }

    public function blog(){
        return $this->belongsTo('App\Models\Blog');
    }
}
