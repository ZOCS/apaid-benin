<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offre extends Model
{
    protected $guarded = [];
    
    protected $casts = [
        'expiration' => 'datetime',
        'publie' => 'datetime',
    ];

     public function users(){
        return  $this->belongsToMany('App\Models\User', 'offre_user');
    }

    public function categories(){
        return  $this->belongsToMany('App\Models\Categorie',  'categorie_offre');
    }
}
