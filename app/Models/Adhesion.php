<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adhesion extends Model
{
    protected $fillable = ['nom', 'responsable', 'email', 'phone','demande', 'domaine',  'pays', 'ville', 'objectifs', 'realisation'];
}
