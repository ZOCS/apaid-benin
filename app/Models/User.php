<?php

namespace App\Models;

use App\Models\Blog;
use App\Models\Commentaire;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Authenticatable
{
    use Notifiable; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'sexe', 'phone', 'pays', 'terms', 'cv',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function commentaires() {
        return $this->hasMany('App\Models\Commentaire');
    }

    

    public function blogs() {
        return $this->hasMany('App\Models\Blog');
    }

    public function commandes() {
        return $this->hasMany(Commande::class);
    }

     public function offres(){
        return  $this->belongsToMany('App\Models\Offre', 'offre_user');
    }

    public function isAdmin() {
        return $this->role == 'admin';
    }

    public function isUser()
    {
        return $this->role == 'user';
    }
    public function isManage()
    {
        return $this->role == 'manage';
    }
    public function isBlogger()
    {
        return $this->role == 'blogger';
    }
}
