<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = ['code', 'percent_off'];

    public function discount($subtotal)
    {
        return (floatval($subtotal) * ($this->percent_off / 100));
    }
}
