<?php

namespace App\Models;

use App\Models\Commentaire;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title', 'image', 'text', 'description', 'auteur','categorie_id',
    ];

    public function commentaires() {
        return $this->hasMany('App\Models\Commentaire');
    }

     public function categories(){
        return  $this->belongsToMany('App\Models\Categorie',  'categorie_blog');
    }
}
