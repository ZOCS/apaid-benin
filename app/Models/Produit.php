<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Categorie;

class Produit extends Model
{
    protected $fillable = [
        'code','nom', 'prix',  'image', 'slug', 'description', 'detail', 'auteur', 'status', 'stock','tags', 'categorie_id','featured','promo','new_arrival','popular','prix_promo','taux_promo', 'mesure_id'
    ];
    protected $casts = [
        'popular' => 'boolean',
        'featured' => 'boolean',
        'new_arrival' => 'boolean',
        'promo' => 'boolean',
    ];

    protected $table = 'produits';

    public function categories(){
        return  $this->belongsToMany('App\Models\Categorie',  'categorie_produit');
    }

    public function getPrice()
    {
        $price = $this->prix;
        return number_format($price, 2, ',', ' ') . ' XOF';
    }

    public function mesure()
    {
        return $this->belongsTo(Mesure::class);
    }
}
