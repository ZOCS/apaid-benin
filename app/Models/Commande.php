<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Commande extends Model
{
    use Notifiable;

    protected $guarded = [];
    public function user() {
        return $this->belongsTo(User::class);
    }
}
