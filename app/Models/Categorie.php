<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Categorie extends Model
{
   

    protected $fillable = [
        'nom', 'slug', 'parent_id',
    ];

    //fonction pour les sous categories
    public function children(){
        return $this->hasMany(Categorie::class, 'parent_id')->with('children');
    }

    public function produits(){
        return  $this->belongsToMany('App\Models\Produit', 'categorie_produit');
    }

    public function files(){
        return  $this->belongsToMany('App\Models\File', 'categorie_file');
    }

    public function prjets(){
        return  $this->belongsToMany('App\Models\Projet', 'categorie_projet');
    }

    public function blogs(){
        return  $this->belongsToMany('App\Models\Blog', 'categorie_blog');
    }

    public function offres(){
        return  $this->belongsToMany('App\Models\Offre',  'categorie_offre');
    }
}
