<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Projet extends Model
{
    protected $fillable = [
        'title', 'image', 'description', 'auteur',
    ];

    protected $table = 'projets';

     public function categories(){
        return  $this->belongsToMany('App\Models\Categorie',  'categorie_projet');
    }
}
