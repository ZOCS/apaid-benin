<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mesure extends Model
{
    protected $fillable = [
        'titre', 'slug'
    ];

    public function produits(){
        return $this->hasMany(Produit::class);
    }
}
