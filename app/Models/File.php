<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
      protected $fillable = [
        'code','nom', 'prix',  'image', 'slug', 'description', 'detail', 'auteur', 'status', 'stock', 'categorie_id','prix_promo','taux_promo'
    ];
    protected $casts = [
        'popular' => 'boolean',
        'featured' => 'boolean',
        'new_arrival' => 'boolean',
        'promo' => 'boolean',
    ];

    protected $table = 'produits';

    public function categories(){
        return  $this->belongsToMany('App\Models\Categorie',  'categorie_file');
    }

    public function getPrice()
    {
        $price = $this->prix;
        return number_format($price, 2, ',', ' ') . ' XOF';
    }

    
}
