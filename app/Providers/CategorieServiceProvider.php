<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;


class CategorieServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['layouts.navshop', 'layouts.nav', 'pages.projet.projet','pages.projet.detail', 'Boutique.accueil', 'Boutique.shop'], 'App\Http\View\Composers\NavComposer',
        );
    }
}
