<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class GlobaleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['layouts.navadmin', 'layouts.nav', 'admin.produit.insert-produit', 'admin\blog\insert-blog','admin.projet.insert-projet', 'admin.offre.ajout'], 'App\Http\View\Composers\HeaderComposer',
        );
    }
}
