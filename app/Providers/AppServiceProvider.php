<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['layouts\navadmin', 'layouts\nav', 'admin\produit\insert-produit', 'admin.blog.add','admin\projet\insert-projet', 'admin\offre\ajout', 'welcome'], 'App\Http\View\Composers\HeaderComposer',
        );

        
    }
}
